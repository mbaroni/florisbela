
import { Elm } from './Main.elm';

var app = Elm.Entries.Public.Main.init({
  node: document.getElementById('root')
});

if(module.hot){
  module.hot.accept();
}

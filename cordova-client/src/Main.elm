module Entries.Public.Main exposing (main)

-- import Entries.Public.App.Init exposing (init)
-- import Entries.Public.App.Subscriptions exposing (subscriptions)
-- import Entries.Public.App.Types exposing (Model, Msg(..))
-- import Entries.Public.App.Update exposing (update)
-- import Entries.Public.App.View exposing (view)
--

import Browser
import Browser.Navigation as Nav
import Html exposing (Html, text)
import Url exposing (Url)


type alias Model =
    { count : Int }


init : Model
init =
    { count = 0 }



--import Html.Lazy exposing (lazy, lazy2)


view : Model -> Html Msg
view model =
    text "Ok12!"


type Msg
    = NoOp


update : Msg -> Model -> Model
update msg model =
    model


main : Program () Model Msg
main =
    Browser.sandbox
        { init = init
        , view = view
        , update = update
        }

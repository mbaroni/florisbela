const path = require("path");
const webpack = require("webpack");
const WriteFilePlugin = require("write-file-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: {
    //mobile: ['./src/app.js', 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'],
    mobile: ['./src/app.js'],
  },
  context: __dirname,
  output: {
    path: path.join(__dirname, "www/js"),
    filename: '[name].js',
    publicPath: 'www/js'
  },
  devtool: 'inline-source-map',
  devServer: {
    historyApiFallback: true,
    hot: true,
    inline: false,
  },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    //new WriteFilePlugin()
  ],
  resolve: {
    modules: [path.join(__dirname, "src"), "node_modules"],
    extensions: [".js", ".elm", ".scss", ".png"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: "babel-loader" }
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      {
          test: /\.css$/,
          exclude: [/elm-stuff/, /node_modules/],
          loaders: ["style-loader", "css-loader?url=false"]
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: [
          { loader: "elm-hot-webpack-loader" },
          { loader: "elm-webpack-loader",
            options: {
              debug: true,
              forceWatch: true,
              cwd: __dirname
            }
          }
        ]
      }
    ]
  }
}

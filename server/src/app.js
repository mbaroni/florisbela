require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const cors = require('cors')
const morgan = require('morgan')
const chalk = require('chalk')
var session = require('express-session');
const Sequelize = require('sequelize');
const db = require('../models');
//var RedisStore = require('connect-redis')(session);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.js')[env];
const webpackConfig = require('../resources/web/webpack.config.js')
const Console = console;
const routes = require('./routes')

// Sequelize
const sequelize = db.sequelize;

/************************************
**            HELPERS              **
************************************/
// Redis config
var redis = require("redis");
var client = redis.createClient({
  host: config.redis.host,
  port: config.redis.port,
});

// Webpack config (development)
const configWebpack = (app) => {
  var compiler = webpack(webpackConfig);
  app.use(webpackDevMiddleware(compiler, {
    log: console.log,
    publicPath: webpackConfig.output.publicPath,
    logLevel: 'error' /* trace, debug, info, warn, error, silent */
  }));
  app.use(webpackHotMiddleware(compiler, {
    log: (str) => {console.log('webpack-hot-log: ' + str);}
    //log: (str) => {}
  }));
}

// Database Connection Test
const testDB = () => {
  sequelize
    .authenticate()
    .then(() => {
      Console.log(chalk.green('Connection has been established successfully.'));
    })
    .catch(err => {
      Console.log(chalk.red('Unable to connect to the database:', err));
    });
}

// Log server initialization
const logInit = (port, hostname) => {
  Console.log(chalk.green('Server runnig at '+ hostname + ':' + port))
}

// Latency simulation middleware
const throttle = (req, res, next) => {
  setTimeout( () => {
    next();
  }, 2000);
}


/************************************
**            CONFIGS              **
************************************/
const app = express()
// Pug template engine
app.set('view engine', 'pug')

// Log format
app.use(morgan('combined'))

// json req body parser
app.use(bodyParser.json())

// cookie parser
app.use(cookieParser())

// Cross-Origin Resource Sharing
app.use(cors())

// Config the public dtatic directory
app.use(express.static('public'))

// Route config
app.use(routes)


/************************************
**       DEVELOPMENT CONFIG        **
************************************/
// throttle
//app.use(throttle)

// Webpack config (development)
if( process.env.NODE_ENV == 'development' ){
  //configWebpack(app)
}


/************************************
**            LISTEN              **
************************************/
app.listen(config.web.port, config.web.host, () => {
  testDB();
  logInit(config.web.port, config.web.host);
});

const jwt = require('jsonwebtoken');

/* Auth middleware */
var auth_mw = ( (req, res, next) => {
  var redirecting = !req.xhr;
  var token = req.headers.authorization;
  if( token ){
    token = token.replace(/Bearer\ +/g,'');
  }else{
    token = req.cookies.jwttoken;
  }

  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if( !err ){
      req.auth = decoded;
      next();
    }else{
      if( redirecting ){
        res.redirect('/public/login');
      }else{
        res.status(400).json({
          msg: 'Usuário não reconhecido'
        });
      }
    }
  });
});

module.exports = {
  auth_mw
};

const jwt = require('jsonwebtoken');
var router = require('express').Router();

var admin_home = require('./controllers/admin/home');
var public = require('./controllers/public');
var admin = require('./controllers/admin');

router.use('/adminapp', admin_home);
router.use('/admin', admin);
router.use('/public', public);

module.exports = router;

var router = require('express').Router();
var { auth_mw } = require('../../helpers');

router.use('/profile', auth_mw, require('./profile'));
router.use('/produtos', auth_mw, require('./produtos'));
router.use('/users', auth_mw, require('./users'));
//router.use('/home', require('./home'));

module.exports = router

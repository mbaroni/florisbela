var models = require('../../../../models');
var config = require('../../../../config');
var pick = require('lodash/pick');

const multer = require('multer')
const upload = multer({dest: '/tmp' });


const uploadConfig = upload.fields([
  { name: 'new_files', maxCount: 2 },
]);


/* Salva profile */
module.exports = [uploadConfig, (req, res) => {
  var { id } = req.auth;
  var { new_files } = req.files;
  var { removed_files } = req.body;

  models.File.createFromUpload(new_files, 1, id);
  models.File.deleteById(removed_files, 1, id);

  models.User.findByPk(id)
  .then( user => {
    var us_update = pick(req.body, [
      'nome',
      'email',
      'sexo',
      'datNascimento'
    ]);

    if( user ){
      user.update(us_update)
      .then( resp => {
        if( resp ){
          res.json('Registro salvo com sucesso.');
        }else{
          res.status(400).json({
            msg: 'Erro ao salvar registro.'
          });
        }
      });
    }else{
      res.status(400).json({
        msg: 'Erro ao salvar registro.'
      });
    }
  })
}];

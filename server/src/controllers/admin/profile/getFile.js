var models = require('../../../../models');

module.exports = (req, res) => {
  var { file_id } = req.params;

  models.File.findByPk(file_id)
  .then( file => {
    file.serveDownload(res);
  });
}

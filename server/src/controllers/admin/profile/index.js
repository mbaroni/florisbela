var router = require('express').Router();

router.get('/view', require('./view'));
router.post('/save', require('./save'));
router.get('/getfile/:file_id', require('./getFile'));

module.exports = router

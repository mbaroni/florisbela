var models = require('../../../../models');
var pick = require('lodash/pick');

module.exports = (req, res) => {
  var { id } = req.auth;

  models.User.findByPk(id)
  .then( user => {
    if( !user ){
      res.status(400).json({
        msg: 'Erro!'
      });
    }else{
      var user = pick(user, ['email', 'nome', 'sexo', 'datNascimento']);
      models.File.findByReference(1, id)
      .then( files => {
        user.files = files.map( f => {
          var arch = f.toArchive();
          arch.url = `/admin/profile/getfile/${f.id}`;
          return arch;
        });
        res.json(user);
      });
    }
  });
}

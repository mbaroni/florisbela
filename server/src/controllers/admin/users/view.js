var models = require('../../../../models');

module.exports = (req, res) => {
  var { id } = req.params;
  models.User.findOne({ where: { id } })
  .then( user => {
    res.json(user);
  })
  .catch( err => {
    res.status(400).json({
      msg: 'Erro!'
    });
  });
}

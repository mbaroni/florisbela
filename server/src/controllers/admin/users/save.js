var models = require('../../../../models');
var bcrypt = require('bcrypt');

module.exports = (req, res) => {
  var { id } = req.params;
  var { password, nome, sexo } = req.body;

  models.User.findOne({ where: { id } })
  .then( user => {
    user.nome = nome;
    user.sexo = sexo;
    user.password = bcrypt.hashSync(password, 10);
    user.save().then( () => {
      res.json({
        msg: 'Salvo com sucesso.'
      });
    })
    .catch( () => {
      res.status(400).json({
        msg: 'Ocorreu um erro ao salvar o registro.'
      });
    });
  })
  .catch( err => {
    res.status(400).json({
      msg: 'Erro!'
    });
  });
}

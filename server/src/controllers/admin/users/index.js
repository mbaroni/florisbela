var router = require('express').Router();

router.get('/list', require('./list'));
router.get('/view/:id', require('./view'));
router.post('/save/:id', require('./save'));

module.exports = router

var models = require('../../../../models');

module.exports = (req, res) => {
  var query = {
    order: [ ['email', 'ASC'] ],
    matchFields: ['email']
  };
  models.User.gridResponce(req, res, query);
}

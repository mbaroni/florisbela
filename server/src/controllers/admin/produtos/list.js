var models = require('../../../../models');

module.exports = (req, res) => {
  var query = {
    order: [ ['nome', 'ASC'] ],
    nested: true
  };
  models.Produto.gridResponce(req, res, query);
}

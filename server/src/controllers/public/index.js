var express = require('express');
var router = express.Router();
var models = require('../../../models');
var bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const checkUniqueUser = (req, res, next) => {
  var { email, password, password2 } = req.body;
  models.User.findOne({
    where: { email }
  }).then(user => {
    if( user ){
      res.status(400).json({
        msg: 'Já existe usuário cadastrado com este e-mail.'
      });
    }else{
      next();
    }
  });
}

/* Registro de novo usuário */
router.post('/api/register', checkUniqueUser, (req, res) => {
  var { email, password } = req.body;
  var newUser = {email, password: bcrypt.hashSync(password, 10)};
  models.User.create(newUser)
  .then( newUser => {
    console.log('ok, criado!');
    var token = jwt.sign({ id: newUser.dataValues.id }, process.env.JWT_SECRET);
    res.json({
      msg: 'Usuário criado com sucesso.',
      jwt: token
    });
  })
  .catch( resp => {
    res.status(400).json({
      msg: 'Não foi possível criar o usuário.',
      error: JSON.stringify(resp)
    });
  })
});


/* Login de usuário */
router.post('/api/login', (req, res) => {
  var { email, password } = req.body;
  var failLogin = () => {
    res.status(400).json({
      msg: 'Não foi possível efetuar login.'
    });
  }
  models.User.findOne({
    where: { email }
  }).then(user => {
    if( user ){
      var cmp = bcrypt.compareSync(password, user.dataValues.password);
      if( cmp ){
        var token = jwt.sign({ id: user.dataValues.id }, process.env.JWT_SECRET);
        res.cookie('jwttoken', token);
        res.json({
          msg: 'Login efetuado com sucesso.',
          jwt: token
        });
      }else{
        failLogin();
      }
    }else{
      failLogin();
    }
  });
});


/* the Single Page App */
router.get('*', (req, res) => {
  res.render('public-elm', {});
});

module.exports = router;

# Florisbela Server
## Using:
 * node v10.15
 * npm 6.4


## Testing login:
`curl -d '{"email" : "marcos@gmail.com", "password": "1234"}' -H "Content-Type: application/json" -X POST http://localhost:8080/auth/login`

## Testing authentication routes:
`curl -H "Authorization: Bearer ${TOKEN}" https://{hostname}/api/myresource`

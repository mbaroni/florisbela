const path = require("path");
const webpack = require("webpack");
//const merge = require("webpack-merge");
const elmMinify = require("elm-minify");

const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: 'development',
  entry: {
    //webpack: ,
    admin: ['./src/Entries/Admin/index.js', 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'],
    public: ['./src/Entries/Public/index.js', 'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000'],
  },
  context: __dirname,
  output: {
    path: path.join(__dirname, "../../public/js"),
    filename: '[name].js',
    publicPath: '/js'
  },
  // devServer: {
  // },
  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.NamedModulesPlugin(),
    //new webpack.NoEmitOnErrorsPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    //new elmMinify.WebpackPlugin(),
  ],
  resolve: {
    modules: [path.join(__dirname, "src"), "node_modules"],
    extensions: [".js", ".elm", ".scss", ".png"]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: { loader: "babel-loader" }
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      {
          test: /\.css$/,
          exclude: [/elm-stuff/, /node_modules/],
          loaders: ["style-loader", "css-loader?url=false"]
      },
      {
        test: /\.elm$/,
        exclude: [/elm-stuff/, /node_modules/],
        use: [
          { loader: "elm-hot-webpack-loader" },
          { loader: "elm-webpack-loader",
            options: {
              debug: true,
              forceWatch: true,
              cwd: __dirname
            }
          }
        ]
      }
    ]
  }
}

const path = require("path");
const webpack = require("webpack");
const elmMinify = require("elm-minify");
const HTMLWebpackPlugin = require("html-webpack-plugin");

const entry = {
  admin: {
    main: './src/Entries/Admin/main.js',
    output: '../../public/js/app-admin.js'
  },
  public: {
    main: './src/Entries/Public/Main.elm',
    output: '../../public/js/app-public.js'
  }
}['admin'];

module.exports = {
  mode: 'development',
  entry: entry.main,
  output: {
    path: path.join(__dirname, "/"),
    filename: entry.output,
  },
  devServer: {
    writeToDisk: true
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    modules: [path.join(__dirname, "src"), "node_modules"],
    extensions: [".js", ".elm", ".scss", ".png"]
  },
  module: {
    rules: [
      { test: /\.js$/
      , exclude: /node_modules/
      , use: { loader: "babel-loader" }
      },
      { test: /\.scss$/
      , use: [
          "style-loader", // creates style nodes from JS strings
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      },
      { test: /\.css$/
      , exclude: [/elm-stuff/, /node_modules/]
      , loaders: ["style-loader", "css-loader?url=false"]
      },
      { test: /\.elm$/
      , exclude: [/elm-stuff/, /node_modules/]
      , use: [
          { loader: "elm-hot-webpack-loader" },
          { loader: "elm-webpack-loader",
            options: {
              debug: true,
              forceWatch: true,
              cwd: __dirname
            }
          }
        ]
      },
    ]
  }
}

module.exports

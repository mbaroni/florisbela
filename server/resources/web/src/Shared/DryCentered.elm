module Shared.DryCentered exposing (view)

import Entries.Public.App.Routes as R
import Html exposing (Html, a, div, text)
import Html.Attributes exposing (class, href)


viewLogo : Html msg
viewLogo =
    a
        [ class "auth-logo"
        , href (R.route2Url R.Login)
        ]
        [ text "Home" ]


view : List (Html msg) -> Html msg
view content =
    div
        [ class "main-wrapper" ]
        [ div
            [ class "auth-wrapper" ]
            (viewLogo :: content)
        ]

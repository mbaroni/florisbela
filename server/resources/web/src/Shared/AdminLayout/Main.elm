module Shared.AdminLayout.Main exposing
    ( BCrumb
    , Option(..)
    , view
    )

import FontAwesome as FA
import Helpers as H
import Html exposing (Html, a, aside, div, footer, h4, header, li, nav, ol, span, text, ul)
import Html.Attributes exposing (class, href, style)
import Html.Events exposing (onClick)
import List.Extra as LE
import Maybe exposing (withDefault)
import Maybe.Extra as ME
import Shared.AdminLayout.ListItem as ListItem


type alias BCrumb msg =
    { label : Html msg
    , href : String
    , onClick : Maybe msg
    }


type alias Config msg =
    { logo : Html msg
    , pageTitle : Maybe (Html msg)
    , breadcrumbs : List ( String, String )
    , sidebar : List (List (ListItem.Option msg))
    , topbar : List (List (ListItem.Option msg))
    , content : List (Html msg)
    , currentPath : String
    , fluid : Bool
    }


default : Config msg
default =
    { logo = text "Matrix Admin!"
    , pageTitle = Just (text "Page Title!")
    , breadcrumbs = []
    , sidebar = []
    , topbar = []
    , content = []
    , currentPath = ""
    , fluid = True
    }


type Option msg
    = Logo (Html msg)
    | PageTitle (Html msg)
    | Breadcrumbs (List ( String, String ))
    | Sidebar (List (List (ListItem.Option msg)))
    | Topbar (List (List (ListItem.Option msg)))
    | Content (List (Html msg))
    | CurrentPath String
    | Fluid Bool


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Logo logo ->
            { config | logo = logo }

        PageTitle pageTitle ->
            { config | pageTitle = Just pageTitle }

        Breadcrumbs breadcrumbs ->
            { config | breadcrumbs = breadcrumbs }

        Sidebar sidebar ->
            { config | sidebar = sidebar }

        Topbar topbar ->
            { config | topbar = topbar }

        Content content ->
            { config | content = content }

        CurrentPath currentPath ->
            { config | currentPath = currentPath }

        Fluid fluid ->
            { config | fluid = fluid }



------------------------------
--           VIEW           --
------------------------------


trimPath : String -> String
trimPath p =
    case String.uncons p of
        Just ( c, tal ) ->
            if c == '/' then
                tal

            else
                p

        Nothing ->
            p


extractPathItem : List (ListItem.Option msg) -> Maybe String
extractPathItem opts =
    let
        getItUrl opt =
            case opt of
                ListItem.Href href ->
                    Just href

                _ ->
                    Nothing
    in
    opts
        |> List.map getItUrl
        |> ME.values
        |> List.head
        |> Maybe.map trimPath


pathIsActive : String -> String -> String -> Bool
pathIsActive guessedPrefix cPath iPath =
    let
        getFirstToken s =
            trimPath s
                |> String.dropLeft (String.length (trimPath guessedPrefix))
                |> String.split "/"
                |> List.head
    in
    case getFirstToken cPath of
        Just cFstToken ->
            case getFirstToken iPath of
                Just iFstToken ->
                    cFstToken == iFstToken

                Nothing ->
                    False

        Nothing ->
            False


guessPrefix : List String -> String
guessPrefix strs =
    let
        isNPrefixed s1 s2 n =
            String.left n s1 == String.left n s2

        getMaxLengthPrefix s1 s2 =
            List.range 0 (min (String.length s1) (String.length s1))
                |> List.reverse
                |> List.filter (isNPrefixed s1 s2)
                |> List.head
                |> withDefault 0
    in
    case LE.uncons strs of
        Just ( s1, l1 ) ->
            List.map (getMaxLengthPrefix s1) l1
                |> List.foldl max 0
                |> (\n -> String.left n s1)

        Nothing ->
            ""


activateItem : String -> String -> List (ListItem.Option msg) -> List (ListItem.Option msg)
activateItem currentPath guessedPrefix itemOpts =
    let
        getItUrl opt =
            case opt of
                ListItem.Href href ->
                    Just href

                _ ->
                    Nothing

        itHref =
            itemOpts
                |> List.map getItUrl
                |> ME.values
                |> List.head
                |> withDefault "ALPcq01elUvNUCqIR9O1"

        activeOpts =
            if pathIsActive guessedPrefix currentPath itHref then
                [ ListItem.Active ]

            else
                []
    in
    itemOpts ++ activeOpts



-----------------------
--    VIEW PIECES    --
-----------------------


viewBCrumb : ( String, String ) -> Html msg
viewBCrumb ( label, h ) =
    li
        [ class "breadcrumb-item" ]
        [ if h == "" then
            text label

          else
            a [ href h ] [ text label ]
        ]


viewBCrumbs : List ( String, String ) -> Html msg
viewBCrumbs bcs =
    let
        bcSep =
            span
                [ class "breadcrumb-sep" ]
                [ text ">" ]
    in
    bcs
        |> List.map viewBCrumb
        |> List.map (\x -> [ bcSep, x ])
        |> List.concat
        |> List.drop 1
        |> ol [ class "breadcrumb-list" ]


viewHeader : Config msg -> Html msg
viewHeader config =
    let
        items =
            List.map
                ListItem.viewOnTop
                config.topbar
    in
    header
        [ class "topbar" ]
        [ nav
            [ class "navbar top-navbar navbar-expand-md navbar-dark" ]
            [ div
                [ class "navbar-header" ]
                [ config.logo ]
            , div
                [ class "navbar-tools" ]
                items
            ]
        ]


viewSidebar : Config msg -> Html msg
viewSidebar config =
    let
        guessedPrefix =
            config.sidebar
                |> List.map extractPathItem
                |> ME.values
                |> guessPrefix

        items =
            config.sidebar
                |> List.map (activateItem config.currentPath guessedPrefix)
                |> List.map ListItem.viewOnSide
    in
    aside
        [ class "left-sidebar" ]
        [ div
            [ class "scroll-sidebar" ]
            [ nav
                [ class "sidebar-nav" ]
                [ ul
                    [ class "sidebarnav-ul p-t-30" ]
                    items
                ]
            ]
        ]


viewBreadcrumbs : Config msg -> Html msg
viewBreadcrumbs config =
    let
        dscBreads =
            if List.length config.breadcrumbs > 0 then
                viewBCrumbs config.breadcrumbs

            else
                text ""
    in
    div
        [ class "page-breadcrumb" ]
        [ div
            [ class "row" ]
            [ div
                [ class "col-sm-8", class "col-xs-12" ]
                [ dscBreads
                ]
            ]
        ]


viewFooter : Html msg
viewFooter =
    footer
        [ class "footer text-center" ]
        [ text "Footer!" ]


viewContent : Config msg -> Html msg
viewContent config =
    let
        tester =
            div
                [ style "min-height" "00vh" -- "100vh"
                ]
                []

        attrs =
            if config.fluid then
                [ class "container-fluid" ]

            else
                [ class "container"
                , style "position" "absolute"
                , style "left" "10px"

                --, padding
                ]

        content =
            div
                attrs
                config.content
    in
    div
        [ class "page-wrapper" ]
        [ viewBreadcrumbs config
        , content
        , viewFooter
        , tester
        ]



-----------------------------------
--           MAIN VIEW           --
-----------------------------------


view : List (Option msg) -> Html msg
view options =
    let
        config =
            List.foldl
                applyOption
                default
                options
    in
    div
        [ class "main-wrapper" ]
        [ viewHeader config
        , viewSidebar config
        , viewContent config
        ]

module Shared.AdminLayout.ListItem exposing
    ( Option(..)
    , viewOnSide
    , viewOnTop
    )

import FontAwesome as FA
import Html exposing (Attribute, Html, a, div, li, span, text)
import Html.Attributes exposing (class, href, style)
import Html.Events exposing (onClick)
import Maybe.Extra as ME


type alias Config msg =
    { label : Html msg
    , icon : Maybe FA.Icon
    , href : String
    , onClick : Maybe msg
    , prefix : String
    , attrs : List (Attribute msg)
    , active : Bool
    }


default : Config msg
default =
    { label = text ""
    , icon = Nothing
    , href = ""
    , onClick = Nothing
    , prefix = "NCvfhxwkHeK5rdDidmn0"
    , attrs = []
    , active = False
    }


type Option msg
    = Label (Html msg)
    | Icon FA.Icon
    | Href String
    | OnClick msg
    | Prefix String
    | Attr (Attribute msg)
    | Active


applyOption : Option msg -> Config msg -> Config msg
applyOption options config =
    case options of
        Label label ->
            { config | label = label }

        Icon icon ->
            { config | icon = Just icon }

        Href hre ->
            { config | href = hre }

        OnClick onClick ->
            { config | onClick = Just onClick }

        Prefix prefix ->
            { config | prefix = prefix }

        Attr attr ->
            { config | attrs = config.attrs ++ [ attr ] }

        Active ->
            { config | active = True }



-------------------------------
--       VIEW ON SIDE        --
-------------------------------


viewOnSide : List (Option msg) -> Html msg
viewOnSide options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        sidebarItemClass =
            if config.active then
                class "sidebar-item sidebar-item-active"

            else
                class "sidebar-item"

        sideIcon =
            case config.icon of
                Nothing ->
                    text ""

                Just icon ->
                    FA.iconWithOptions
                        icon
                        FA.Solid
                        []
                        [ class "sidebar-icon" ]

        itemHref =
            if config.href /= "" then
                href config.href

            else
                class ""

        clickAttr =
            case config.onClick of
                Nothing ->
                    class ""

                Just onc ->
                    onClick onc

        attrs =
            [ sidebarItemClass
            , clickAttr
            ]
                ++ config.attrs

        node =
            if config.href /= "" then
                a

            else
                div

        children =
            [ node
                [ class "sidebar-link waves-effect"
                , itemHref
                ]
                [ sideIcon
                , span
                    [ class "hide-menu"
                    , style "padding-left" "5px"
                    ]
                    [ config.label ]
                ]
            ]
    in
    li
        attrs
        children



------------------------------
--       VIEW ON TOP        --
------------------------------


viewOnTop : List (Option msg) -> Html msg
viewOnTop options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        clickAttr =
            case config.onClick of
                Nothing ->
                    class ""

                Just onClk ->
                    onClick onClk

        href_ =
            if config.href == "" then
                class ""

            else
                href config.href

        cursorStyle =
            if config.href /= "" || ME.isJust config.onClick then
                style "cursor" "pointer"

            else
                class ""

        renderIcon =
            case config.icon of
                Nothing ->
                    text ""

                Just icon ->
                    FA.iconWithOptions
                        icon
                        FA.Solid
                        []
                        []

        attrs =
            [ class "navbar-tools-util" ]
                ++ config.attrs

        node =
            if config.href /= "" then
                a

            else
                div

        children =
            [ node
                [ class "navbar-tool-item"
                , clickAttr
                , cursorStyle
                , href_
                ]
                [ renderIcon
                , config.label
                ]
            ]
    in
    div
        attrs
        children

module Session exposing
    ( JWT
    , Model(..)
    , Msg(..)
    , User
    , init
    , subscriptions
    , toJson
    , update
    )

import Helpers exposing (log)
import Json.Encode as JE


type alias JWT =
    String


type alias User =
    { nome : String
    , email : String
    , jwt : JWT
    }


type Msg
    = CommitLogin User
    | NoOp


type Model
    = Guest
    | Logged User


toValue : Model -> JE.Value
toValue model =
    case model of
        Guest ->
            JE.null

        Logged user ->
            JE.object
                [ ( "nome", JE.string user.nome )
                , ( "email", JE.string user.email )
                , ( "jwt", JE.string user.jwt )
                ]


toJson : Model -> String
toJson model =
    toValue model
        |> JE.encode 1


update : Model -> Msg -> ( Model, Cmd Msg )
update model msg =
    case msg of
        CommitLogin user ->
            ( Logged user, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


init : Maybe String -> Model
init mJwt =
    case mJwt of
        Just jwt ->
            Logged (User "" "" (log "jwt" jwt))

        Nothing ->
            Guest

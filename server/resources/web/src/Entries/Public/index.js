//import '../../../../../public/css/pages/login.css';
import '../../../../../resources/scss/components/style.scss';
import '../../../../../resources/scss/public/public.scss';

import { Elm } from './Main.elm';
import registerServiceWorker from '../../registerServiceWorker';

var app = Elm.Entries.Public.Main.init({
  node: document.getElementById('root')
});

app.ports.saveJwt.subscribe(function(jwt) {
 localStorage.setItem('jwt', jwt);
 //console.log('saveJwt called:', jwt);
 app.ports.gotJwt.send(jwt);
});
app.ports.getJwt.subscribe(function() {
  var jwt = localStorage.getItem('jwt') || "";
  //console.log('gotJwt called:', jwt);
  app.ports.gotJwt.send(jwt);
});

if(module.hot){
  module.hot.accept();
}

registerServiceWorker();

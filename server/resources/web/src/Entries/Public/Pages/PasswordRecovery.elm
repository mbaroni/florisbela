module Entries.Public.Pages.PasswordRecovery exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Components.Button as Btn
import Components.Field.Text as Txt
import Components.Modal as Modal
import Components.Types exposing (..)
import Entries.Public.App.Routes as Route
import FontAwesome as FA
import Helpers as H exposing (isValidEmail)
import Html exposing (Html, a, br, div, h4, h5, input, span, text)
import Html.Attributes exposing (class, href, style)
import Shared.DryCentered as DryCenter


type alias Model =
    { email : String
    , submitting : Bool
    , showErrors : Bool
    , showModal : Bool
    }


type Msg
    = UpdateEmail String
    | Submit
    | ToggleModal
    | NoOp


init : ( Model, Cmd Msg )
init =
    let
        model =
            { email = ""
            , submitting = False
            , showErrors = False
            , showModal = False
            }
    in
    ( model, Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateEmail newEmail ->
            ( { model | email = newEmail }, Cmd.none )

        Submit ->
            ( { model | showErrors = True }, Cmd.none )

        ToggleModal ->
            ( { model | showModal = not model.showModal }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


viewTitle : Html Msg
viewTitle =
    div
        [ class "auth-title" ]
        [ text "Password Recovery" ]


viewRecoveryForm : Model -> Html Msg
viewRecoveryForm model =
    let
        emailInfoOpt =
            if isValidEmail model.email || not model.showErrors then
                Txt.NoOpt

            else
                Txt.Error (Just "E-mail inválido.")
    in
    div
        [ class "login-form" ]
        [ Txt.view
            [ Txt.Placeholder "E-mail"
            , Txt.OnInput UpdateEmail
            , Txt.Value model.email
            , Txt.Type Txt.Email
            , emailInfoOpt
            , Txt.PreAppend
                [ Txt.AppTyp Success
                , Txt.AppIcon FA.user
                ]
            ]
        ]


viewSubmitBtns : Model -> Html Msg
viewSubmitBtns model =
    let
        submitEnabled =
            not model.showErrors || isValidEmail model.email
    in
    div
        [ class "login-btnbar" ]
        [ Btn.view
            [ Btn.Label (text "Enviar")
            , Btn.Typ Success
            , Btn.Icon FA.check
            , Btn.Enabled submitEnabled
            , Btn.OnClick Submit
            ]
        ]


viewModal model =
    Modal.view
        [ Modal.Opened model.showModal
        , Modal.OnClose ToggleModal
        , div
            []
            [ text "Bodyyy" ]
            |> Modal.Body
        , div
            []
            [ Btn.view
                [ Btn.Label (text "Enviar")
                , Btn.Icon FA.check
                , Btn.Typ Success
                ]
            , Btn.view
                [ Btn.Label (text "Cancelar")
                , Btn.Icon FA.times
                , Btn.Typ Danger
                ]
            ]
            |> Modal.Footer
        ]


viewAlert model =
    Modal.alert
        [ Modal.Opened model.showModal
        , Modal.OnOk ToggleModal
        , Modal.Body (text "Tudo ok!")
        , Modal.Header (h5 [] [ text "Tudo ok!" ])
        ]


viewConfirm model =
    Modal.confirm
        [ Modal.Opened model.showModal
        , Modal.OnOk ToggleModal
        , Modal.OnCancel ToggleModal
        , Modal.Body (text "Confirma?")
        , Modal.Header (h5 [] [ text "Confirmação..." ])
        ]


view : Model -> Html Msg
view model =
    DryCenter.view
        [ div
            [ class "auth-box" ]
            [ viewTitle
            , viewRecoveryForm model
            , viewSubmitBtns model
            , viewConfirm model
            ]
        ]

module Entries.Public.Pages.NotFound exposing (view)

import Entries.Public.App.Routes as Route
import Html exposing (Html, a, br, div, text)
import Html.Attributes exposing (href, style)


view : Html msg
view =
    div
        [ style "color" "#fff"
        , style "font-weight" "bold"
        , style "font-size" "20px"
        , style "text-align" "center"
        ]
        [ text "Not Found"
        , br [] []
        , a
            [ href (Route.route2Url Route.Login) ]
            [ text "login page" ]
        ]

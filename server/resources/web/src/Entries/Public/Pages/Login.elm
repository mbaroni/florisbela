module Entries.Public.Pages.Login exposing
    ( Model
    , Msg(..)
    , init
    , update
    , view
    )

import Api
import Browser.Navigation as Nav
import Components.Button as Btn
import Components.Field.Text as Txt
import Components.Modal as Modal
import Components.Types exposing (..)
import Entries.Public.App.Routes as Routes
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, style)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Shared.DryCentered as DryCenter



----------------------------------------
--               MODEL               --
----------------------------------------


type alias LoginRes =
    { msg : String
    , jwt : String
    }


type alias Model =
    { email : String
    , password : String
    , submitting : Bool
    , showErrors : Bool
    , alertMsg : Maybe String
    }


init : ( Model, Cmd Msg )
init =
    ( { email = ""
      , password = ""
      , submitting = False
      , showErrors = False
      , alertMsg = Nothing
      }
    , Cmd.none
    )



-------------------------------------
--               MSG               --
-------------------------------------


type Msg
    = UpdateEmail String
    | UpdatePassword String
    | Submit
    | SubmitGot (Result (Maybe String) LoginRes)
    | DismissAlert



----------------------------------------
--               UPDATE               --
----------------------------------------


encodeLoginReq : Model -> JE.Value
encodeLoginReq model =
    JE.object
        [ ( "email", JE.string model.email )
        , ( "password", JE.string model.password )
        ]


decodeLoginRes : JD.Decoder LoginRes
decodeLoginRes =
    JD.map2
        LoginRes
        (JD.field "msg" JD.string)
        (JD.field "jwt" JD.string)


loginRequest : Model -> Cmd Msg
loginRequest model =
    let
        postReq =
            Api.nonAuthedResource.post
                [ "public", "api", "login" ]
                []
                (model |> encodeLoginReq |> Http.jsonBody)
    in
    Api.myExecRequest
        SubmitGot
        decodeLoginRes
        postReq


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateEmail newEmail ->
            ( { model | email = newEmail }, Cmd.none )

        UpdatePassword newPassword ->
            ( { model | password = newPassword }, Cmd.none )

        Submit ->
            let
                cmdPost =
                    loginRequest model
            in
            ( { model | submitting = True }, cmdPost )

        SubmitGot result ->
            let
                newAlerMsg =
                    case result of
                        Ok logRes ->
                            Nothing

                        Err err ->
                            err
            in
            ( { model
                | submitting = False
                , alertMsg = newAlerMsg
              }
            , Cmd.none
            )

        DismissAlert ->
            ( { model | alertMsg = Nothing }, Cmd.none )



--------------------------------------
--               VIEW               --
--------------------------------------


viewTitle : Html Msg
viewTitle =
    div
        [ class "auth-title" ]
        [ text "Login" ]


viewLoginForm : Model -> Html Msg
viewLoginForm model =
    div
        [ class "login-form" ]
        [ Txt.view
            [ Txt.Placeholder "E-mail"
            , Txt.OnInput UpdateEmail
            , Txt.Value model.email
            , Txt.Type Txt.Email
            , Txt.PreAppend
                [ Txt.AppTyp Success
                , Txt.AppIcon FA.user
                ]
            ]
        , Txt.view
            [ Txt.Placeholder "Senha"
            , Txt.OnInput UpdatePassword
            , Txt.Value model.password
            , Txt.Type Txt.Password
            , Txt.PreAppend
                [ Txt.AppTyp Warning
                , Txt.AppIcon FA.key
                ]
            ]
        ]


viewLoginBtns : Model -> Html Msg
viewLoginBtns model =
    let
        submitEnabled =
            H.isValidEmail model.email && String.length model.password >= 8
    in
    div
        [ class "login-btnbar" ]
        [ Btn.view
            [ Btn.Label (text "Login")
            , Btn.Typ Success
            , Btn.OnClick Submit
            , Btn.Icon FA.check
            , Btn.Enabled submitEnabled
            , Btn.Loading model.submitting
            ]
        , Btn.view
            [ Btn.Label (text "Lost password")
            , Btn.Typ Info
            , Btn.Icon FA.lock
            , Btn.Enabled True
            , Btn.Href (Routes.route2Url Routes.PasswordRecovery)
            ]
        ]


viewLoginBtns2 : Model -> Html Msg
viewLoginBtns2 model =
    div
        [ class "login-btnbar" ]
        [ div [] []
        , Btn.view
            [ Btn.Label (text "Register")
            , Btn.Typ Primary
            , Btn.Icon FA.user
            , Btn.Enabled True
            , Btn.Href (Routes.route2Url Routes.Register)
            ]
        ]


viewAlert : Model -> Html Msg
viewAlert model =
    case model.alertMsg of
        Nothing ->
            text ""

        Just msg ->
            Modal.alert
                [ Modal.Body (text msg)
                , Modal.Opened True
                , Modal.OnClose DismissAlert
                ]


view : Model -> Html Msg
view model =
    DryCenter.view
        [ div
            [ class "auth-box" ]
            [ viewTitle
            , viewLoginForm model
            , viewLoginBtns model
            , viewLoginBtns2 model
            , viewAlert model
            ]
        ]

module Entries.Public.Pages.Register exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Api
import Browser.Navigation as Nav
import Components.Button as Btn
import Components.Field.Text as Txt
import Components.FieldValidators as FVal
import Components.Form as F
import Components.Form.Types as FT
import Components.Modal as Modal
import Components.Types exposing (..)
import Dict exposing (Dict)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Maybe.Extra as ME
import Shared.DryCentered as DryCenter



------------------------------------
--              MODEL              --
------------------------------------


type alias PostResponse =
    { msg : String
    , jwt : String
    }


type alias Register =
    { email : String
    , password : String
    , password2 : String
    }


type alias Model =
    { register : Register
    , form : FT.Model
    , submitting : Bool
    , showErrors : Bool
    , lastReqRes : Maybe (Result String String)
    }



------------------------------------
--              INIT              --
------------------------------------


emptyRegister : Register
emptyRegister =
    { email = ""
    , password = ""
    , password2 = ""
    }


init : ( Model, Cmd Msg )
init =
    let
        newModel =
            { register = emptyRegister
            , form = F.initial
            , submitting = False
            , showErrors = False
            , lastReqRes = Nothing
            }

        cmd =
            Cmd.none
    in
    ( newModel, cmd )



-----------------------------------
--              MSG              --
-----------------------------------


type FormRegisterMsg
    = UpdateEmail String
    | UpdatePassword String
    | UpdatePassword2 String


type Msg
    = NoOp
    | FormRegMsg FormRegisterMsg
    | Submit
    | SubmitGot (Result (Maybe String) PostResponse)
    | DismissAlert



--------------------------------------
--              UPDATE              --
--------------------------------------


encodeRegister : Register -> JE.Value
encodeRegister register =
    JE.object
        [ ( "email", JE.string register.email )
        , ( "password", JE.string register.password )
        ]


decodePostResponse : JD.Decoder PostResponse
decodePostResponse =
    JD.map2
        PostResponse
        (JD.field "msg" JD.string)
        (JD.field "jwt" JD.string)


updateFormRegister : FormRegisterMsg -> Register -> ( Register, Cmd FormRegisterMsg )
updateFormRegister msg register =
    case msg of
        UpdateEmail email ->
            ( { register | email = email }, Cmd.none )

        UpdatePassword password ->
            ( { register | password = password }, Cmd.none )

        UpdatePassword2 password2 ->
            ( { register | password2 = password2 }, Cmd.none )


registerRequest : Register -> Cmd Msg
registerRequest register =
    let
        postReq =
            Api.nonAuthedResource.post
                [ "public", "api", "register" ]
                []
                (register |> encodeRegister |> Http.jsonBody)
    in
    Api.myExecRequest
        SubmitGot
        decodePostResponse
        postReq


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        FormRegMsg subMsg ->
            let
                ( register, cmdRegister ) =
                    updateFormRegister
                        subMsg
                        model.register
            in
            ( { model | register = register }
            , Cmd.map FormRegMsg cmdRegister
            )

        Submit ->
            let
                formIsValid =
                    model.register
                        |> registerErrors True
                        |> Dict.isEmpty

                cmd =
                    if formIsValid then
                        registerRequest
                            model.register

                    else
                        Cmd.none
            in
            ( { model | showErrors = True }, cmd )

        SubmitGot res ->
            let
                lastReqRes =
                    case res of
                        Ok resp ->
                            Ok resp.msg

                        Err err ->
                            Maybe.withDefault
                                "Não foi possível criar usuário."
                                err
                                |> Err
            in
            let
                cmd =
                    case res of
                        Ok resp ->
                            Api.saveJwt resp.jwt

                        Err _ ->
                            Cmd.none
            in
            ( { model | lastReqRes = Just lastReqRes }, cmd )

        DismissAlert ->
            let
                cmd =
                    case model.lastReqRes of
                        Just res ->
                            case res of
                                Ok _ ->
                                    Cmd.none

                                --Nav.load "/admin/home/home"
                                Err _ ->
                                    Cmd.none

                        Nothing ->
                            Cmd.none
            in
            ( { model | lastReqRes = Nothing }, cmd )

        NoOp ->
            ( model, Cmd.none )


registerErrors : Bool -> Register -> Dict String (List String)
registerErrors showErrors reg =
    let
        passwordMatches =
            FVal.assertOrError
                (reg.password == reg.password2)
                "A confirmação de senha não confere."

        minPWLength =
            FVal.assertOrError
                (String.length reg.password >= 8)
                "A senha deve ter no mínimo 8 caracteres."
    in
    if showErrors then
        FVal.buildErrorDict
            [ ( "email", [ FVal.isValidEmail reg.email, FVal.isStrEmpty reg.email ] )
            , ( "password", [ minPWLength, FVal.isStrEmpty reg.password ] )
            , ( "password2", [ passwordMatches, FVal.isStrEmpty reg.password2 ] )
            ]

    else
        Dict.empty



------------------------------------
--              VIEW              --
------------------------------------


viewForm : Model -> Html Msg
viewForm model =
    let
        reg =
            model.register

        formErrors =
            registerErrors
                model.showErrors
                model.register
    in
    div
        []
        [ Txt.view
            [ Txt.Label (text "E-mail")
            , Txt.Name "email"
            , Txt.Value reg.email
            , Txt.OnInput UpdateEmail
            , Txt.FormErrors formErrors
            , Txt.Type Txt.Email
            , Txt.Required True
            ]
        , Txt.view
            [ Txt.Label (text "Senha")
            , Txt.Name "password"
            , Txt.Value reg.password
            , Txt.OnInput UpdatePassword
            , Txt.FormErrors formErrors
            , Txt.Type Txt.Password
            , Txt.Required True
            ]
        , Txt.view
            [ Txt.Label (text "Confirmação")
            , Txt.Name "password2"
            , Txt.Value reg.password2
            , Txt.OnInput UpdatePassword2
            , Txt.FormErrors formErrors
            , Txt.Type Txt.Password
            , Txt.Required True
            ]
        ]
        |> Html.map FormRegMsg


viewTitle : Html Msg
viewTitle =
    div
        [ class "auth-title" ]
        [ text "Account Register" ]


viewSubmitBtns : Model -> Html Msg
viewSubmitBtns model =
    let
        submitEnabled =
            model.register
                |> registerErrors model.showErrors
                |> Dict.isEmpty
    in
    div
        [ class "login-btnbar" ]
        [ Btn.view
            [ Btn.Label (text "Registrar")
            , Btn.Typ Success
            , Btn.Icon FA.check
            , Btn.Enabled submitEnabled
            , Btn.OnClick Submit
            ]
        ]


viewAlert : Model -> Html Msg
viewAlert model =
    case model.lastReqRes of
        Nothing ->
            text ""

        Just resp ->
            let
                msg =
                    case resp of
                        Ok m ->
                            m

                        Err m ->
                            m
            in
            Modal.alert
                [ Modal.Opened True
                , Modal.OnClose DismissAlert
                , Modal.Body (text msg)
                ]


view : Model -> Html Msg
view model =
    DryCenter.view
        [ div
            [ class "auth-box" ]
            [ viewTitle
            , viewForm model
            , viewSubmitBtns model
            , viewAlert model
            ]
        ]

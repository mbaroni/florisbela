module Entries.Public.Main exposing (main)

import Browser
import Entries.Public.App.Init exposing (init)
import Entries.Public.App.Subscriptions exposing (subscriptions)
import Entries.Public.App.Types exposing (Model, Msg(..))
import Entries.Public.App.Update exposing (update)
import Entries.Public.App.View exposing (view)



--import Html.Lazy exposing (lazy, lazy2)


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }

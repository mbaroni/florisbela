module Entries.Public.App.Init exposing (init)

import Api
import Browser.Navigation as Nav
import Entries.Public.App.Routes as R
import Entries.Public.App.Types exposing (..)
import Entries.Public.Pages.Login as Login
import Entries.Public.Pages.PasswordRecovery as PWRecovery
import Entries.Public.Pages.Register as Register
import Helpers exposing (send)
import Url exposing (Url)


initPages : ( Pages, Cmd Msg )
initPages =
    let
        ( modelLogin, cmdLogin ) =
            Login.init

        ( modelPWRevocery, cmdPWRecovery ) =
            PWRecovery.init

        ( modelRegister, cmdRegister ) =
            Register.init

        pages =
            { login = modelLogin
            , pwrecovery = modelPWRevocery
            , register = modelRegister
            }

        pageCmds =
            Cmd.batch
                [ Cmd.map LoginMsg cmdLogin
                , Cmd.map PWRecoveryMsg cmdPWRecovery
                , Cmd.map RegisterMsg cmdRegister
                ]
    in
    ( pages, Cmd.map GotPageMsg pageCmds )


init : flags -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( pageModel, pageCmd ) =
            initPages

        model =
            { navKey = key
            , route = R.url2Route url
            , pages = pageModel
            }

        cmd =
            Cmd.batch [ Api.getJwt (), pageCmd ]
    in
    ( model, cmd )

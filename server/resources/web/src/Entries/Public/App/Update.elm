module Entries.Public.App.Update exposing (update)

import Api
import Browser exposing (Document, UrlRequest(..))
import Browser.Navigation as Nav
import Entries.Public.App.Routes as R
import Entries.Public.App.Types exposing (..)
import Entries.Public.Pages.Login as Login
import Entries.Public.Pages.PasswordRecovery as PWRecovery
import Entries.Public.Pages.Register as Register
import Helpers as H
import Maybe
import Url exposing (Url)


adminUrl : String
adminUrl =
    "/adminapp/home"


updatePage : PageMsg -> Pages -> ( Pages, Cmd Msg )
updatePage msg pages =
    case msg of
        LoginMsg subMsg ->
            let
                ( newSubModel, cmd ) =
                    Login.update subMsg pages.login

                jwtCmd =
                    case subMsg of
                        Login.SubmitGot result ->
                            case result of
                                Ok logRes ->
                                    Cmd.batch
                                        [ Api.saveJwt logRes.jwt ]

                                Err err ->
                                    Cmd.none

                        _ ->
                            Cmd.none
            in
            ( { pages | login = newSubModel }, Cmd.batch [ Cmd.map (LoginMsg >> GotPageMsg) cmd, jwtCmd ] )

        PWRecoveryMsg subMsg ->
            let
                ( newSubModel, cmd ) =
                    PWRecovery.update subMsg pages.pwrecovery
            in
            ( { pages | pwrecovery = newSubModel }, Cmd.map (PWRecoveryMsg >> GotPageMsg) cmd )

        RegisterMsg subMsg ->
            let
                ( newSubModel, cmd ) =
                    Register.update subMsg pages.register
            in
            ( { pages | register = newSubModel }, Cmd.map (RegisterMsg >> GotPageMsg) cmd )


initPagesWith : Pages -> Maybe (subMsg -> PageMsg) -> (Pages -> subModel -> Pages) -> ( subModel, Cmd subMsg ) -> ( Pages, Cmd Msg )
initPagesWith pages msgMapper pageSetter pageIniter =
    let
        ( newPage, cmdPage ) =
            pageIniter

        newCmd =
            msgMapper
                |> Maybe.map (\m -> Cmd.map m cmdPage)
                |> Maybe.map (Cmd.map GotPageMsg)
                |> Maybe.withDefault Cmd.none
    in
    ( pageSetter pages newPage, newCmd )


initPage : R.Route -> Pages -> ( Pages, Cmd Msg )
initPage route pages =
    case route of
        R.Login ->
            initPagesWith
                pages
                (Just LoginMsg)
                (\pgs login -> { pgs | login = login })
                Login.init

        R.PasswordRecovery ->
            initPagesWith
                pages
                (Just PWRecoveryMsg)
                (\pgs pwrecovery -> { pgs | pwrecovery = pwrecovery })
                PWRecovery.init

        R.Register ->
            initPagesWith
                pages
                (Just RegisterMsg)
                (\pgs register -> { pgs | register = register })
                Register.init

        R.NotFound ->
            ( pages, Cmd.none )


handlePageChange : Url -> Model -> ( Model, Cmd Msg )
handlePageChange url model =
    let
        newRoute =
            R.url2Route url

        oldPages =
            model.pages

        ( newPages, cmd ) =
            initPage newRoute model.pages
    in
    ( { model | route = newRoute, pages = newPages }, cmd )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotPageMsg subMsg ->
            let
                ( newPages, cmdPage ) =
                    updatePage subMsg model.pages
            in
            ( { model | pages = newPages }, cmdPage )

        OnUrlRequest urlReq ->
            case urlReq of
                External url ->
                    ( model, Nav.load url )

                Internal url ->
                    ( model, Nav.pushUrl model.navKey url.path )

        OnUrlChange url ->
            handlePageChange url model

        GotJwt jwt ->
            let
                cmd =
                    if jwt == "" then
                        Cmd.none

                    else
                        Nav.load adminUrl
            in
            ( model, cmd )

        NoOp ->
            ( model, Cmd.none )

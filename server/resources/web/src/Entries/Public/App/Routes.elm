module Entries.Public.App.Routes exposing (Route(..), prefix, route2Url, url2Route)

import Url exposing (Url)
import Url.Parser exposing ((</>), int, map, oneOf, parse, s, string)


type Route
    = Login
    | PasswordRecovery
    | Register
    | NotFound


prefix =
    "public"


route2Url : Route -> String
route2Url route =
    let
        suf =
            case route of
                Login ->
                    "login"

                PasswordRecovery ->
                    "passrecovery"

                Register ->
                    "register"

                NotFound ->
                    ""
    in
    "/" ++ prefix ++ "/" ++ suf


url2Route : Url -> Route
url2Route url =
    url
        |> parse
            (oneOf
                [ map Login (s prefix </> s "login")
                , map PasswordRecovery (s prefix </> s "passrecovery")
                , map Register (s prefix </> s "register")
                , map NotFound (s prefix </> s "home")
                ]
            )
        |> Maybe.withDefault NotFound

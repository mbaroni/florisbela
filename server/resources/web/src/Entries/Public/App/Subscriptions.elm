module Entries.Public.App.Subscriptions exposing (subscriptions)

import Api
import Entries.Public.App.Types exposing (..)


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Api.gotJwt GotJwt ]

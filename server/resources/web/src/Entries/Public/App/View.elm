module Entries.Public.App.View exposing (view)

import Browser exposing (Document)
import Entries.Public.App.Routes as Route
import Entries.Public.App.Types exposing (..)
import Entries.Public.Pages.Login as PageLogin
import Entries.Public.Pages.NotFound as PageNotFound
import Entries.Public.Pages.PasswordRecovery as PagePWRecovery
import Entries.Public.Pages.Register as Register
import Html exposing (Html)


viewPage : Model -> Html Msg
viewPage model =
    case model.route of
        Route.Login ->
            PageLogin.view model.pages.login
                |> Html.map (LoginMsg >> GotPageMsg)

        Route.PasswordRecovery ->
            PagePWRecovery.view model.pages.pwrecovery
                |> Html.map (PWRecoveryMsg >> GotPageMsg)

        Route.Register ->
            Register.view model.pages.register
                |> Html.map (RegisterMsg >> GotPageMsg)

        Route.NotFound ->
            PageNotFound.view


view : Model -> Document Msg
view model =
    let
        title =
            "Public - Front"

        body =
            [ viewPage model ]
    in
    { title = title
    , body = body
    }

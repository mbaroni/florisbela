module Entries.Public.App.Types exposing
    ( Model
    , Msg(..)
    , PageMsg(..)
    , Pages
    )

import Browser exposing (UrlRequest(..))
import Browser.Navigation as Nav
import Entries.Public.App.Routes as R
import Entries.Public.Pages.Login as Login
import Entries.Public.Pages.PasswordRecovery as PWRecovery
import Entries.Public.Pages.Register as Register
import Url exposing (Url)



---------------------------------------
--                MSG                --
---------------------------------------


type PageMsg
    = LoginMsg Login.Msg
    | PWRecoveryMsg PWRecovery.Msg
    | RegisterMsg Register.Msg


type Msg
    = OnUrlRequest UrlRequest
    | OnUrlChange Url
    | GotJwt String
    | GotPageMsg PageMsg
    | NoOp


type alias Pages =
    { login : Login.Model
    , pwrecovery : PWRecovery.Model
    , register : Register.Model
    }


type alias Model =
    { navKey : Nav.Key
    , route : R.Route
    , pages : Pages
    }

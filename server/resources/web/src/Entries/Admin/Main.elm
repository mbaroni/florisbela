module Entries.Admin.Main exposing (main)

import Browser
import Entries.Admin.App.Init exposing (init)
import Entries.Admin.App.Model exposing (Model)
import Entries.Admin.App.Msg exposing (Msg(..))
import Entries.Admin.App.Subscriptions exposing (subscriptions)
import Entries.Admin.App.Update exposing (update)
import Entries.Admin.App.View exposing (view)
import Html.Lazy exposing (lazy, lazy2)


main : Program String Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , update = update
        , subscriptions = subscriptions
        , onUrlRequest = OnUrlRequest
        , onUrlChange = OnUrlChange
        }

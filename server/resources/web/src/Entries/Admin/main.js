import { Elm } from './Main.elm';
import registerServiceWorker from '../../registerServiceWorker';

console.log(Object.keys(Elm.Entries));
var app = Elm.Entries.Admin.Main.init({
  node: document.getElementById('root'),
  flags: 'ok!'
});

if(module.hot){
  module.hot.accept();
}

registerServiceWorker();

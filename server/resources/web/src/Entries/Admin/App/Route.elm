module Entries.Admin.App.Route exposing
    ( Route(..)
    , route2Str
    , url2Route
    )

import List exposing (filter, head)
import Maybe exposing (withDefault)
import Tuple exposing (first, second)
import Url exposing (Url)
import Url.Parser exposing ((</>), int, map, oneOf, parse, s, string)


{-| Route list
-}
type Route
    = Home
    | Profile
    | ProfileEdit
    | Gallery
    | Users
    | UserEdit Int
    | UserNew
    | NotFound


prefix =
    s "adminapp"


prefixStr =
    "adminapp"


url2Route : Url -> Maybe Route
url2Route =
    oneOf
        [ map Home (prefix </> s "home")
        , map Profile (prefix </> s "profile")
        , map ProfileEdit (prefix </> s "profile" </> s "edit")
        , map Gallery (prefix </> s "gallery")
        , map Users (prefix </> s "users")
        , map UserEdit (prefix </> s "users" </> int)
        , map UserNew (prefix </> s "users" </> s "new")
        ]
        |> parse


route2Str : Route -> String
route2Str route =
    let
        suf =
            case route of
                Home ->
                    "home"

                Profile ->
                    "profile"

                ProfileEdit ->
                    "profile/edit"

                Gallery ->
                    "gallery"

                Users ->
                    "users"

                UserEdit id ->
                    "users/" ++ String.fromInt id

                UserNew ->
                    "users/new"

                NotFound ->
                    "---"
    in
    "/" ++ prefixStr ++ "/" ++ suf

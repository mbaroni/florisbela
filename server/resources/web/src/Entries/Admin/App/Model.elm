module Entries.Admin.App.Model exposing (Model, Pages)

import Browser.Navigation as Nav
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Gallery.Main as Gallery
import Entries.Admin.Pages.Home.Main as Home
import Entries.Admin.Pages.Profile.Model as Profile
import Entries.Admin.Pages.Users.Edit.Model as UserEdit
import Entries.Admin.Pages.Users.Main as Users
import Session as Session
import Url exposing (Url)


{-| Pages
-}
type alias Pages =
    { home : Home.Model
    , profile : Profile.Model
    , gallery : Gallery.Model
    , users : Users.Model
    , userEdit : UserEdit.Model
    }


{-| Model
-}
type alias Model =
    { navKey : Nav.Key
    , route : Route.Route
    , session : Session.Model
    , loadingSession : Bool
    , currentUrl : Url
    , pages : Pages
    , showLogoutModal : Bool
    }

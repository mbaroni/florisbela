port module Entries.Admin.App.Init exposing (getJwt, init)

import Browser.Navigation as Nav
import Entries.Admin.App.Model exposing (..)
import Entries.Admin.App.Msg exposing (..)
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Gallery.Main as Gallery
import Entries.Admin.Pages.Home.Main as Home
import Entries.Admin.Pages.Profile.Model as Profile
import Entries.Admin.Pages.Users.Edit.Update as UserEdit
import Entries.Admin.Pages.Users.Main as Users
import Session as Session
import Url exposing (Url)


port getJwt : () -> Cmd msg


init : String -> Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url navKey =
    let
        ( newHome, cmdHome ) =
            Home.init Nothing

        ( newGallery, cmdGallery ) =
            Gallery.init

        ( newUsers, cmdUsers ) =
            Users.init

        ( newUserEdit, cmdEditUser ) =
            UserEdit.init Nothing
    in
    ( { navKey = navKey
      , route = Route.Home
      , session = Session.init (Just flags)
      , loadingSession = False
      , currentUrl = url
      , showLogoutModal = False
      , pages =
            { home = newHome
            , profile = Profile.blankModel
            , gallery = newGallery
            , users = newUsers
            , userEdit = newUserEdit
            }
      }
    , Cmd.batch
        [ Nav.pushUrl navKey url.path
        , getJwt ()
        , Cmd.map (Home >> Page) cmdHome
        , Cmd.map (Gallery >> Page) cmdGallery
        , Cmd.map (Users >> Page) cmdUsers
        , Cmd.map (UserEdit >> Page) cmdEditUser
        ]
    )

port module Entries.Admin.App.Update exposing (update)

import Api
import Browser exposing (Document, UrlRequest(..))
import Browser.Navigation as Nav
import Components.Modal as Modal
import Either exposing (Either(..))
import Entries.Admin.App.Init exposing (getJwt)
import Entries.Admin.App.Model exposing (..)
import Entries.Admin.App.Msg exposing (..)
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Gallery.Main as Gallery
import Entries.Admin.Pages.Home.Main as Home
import Entries.Admin.Pages.Profile.Edit as ProfileEdit
import Entries.Admin.Pages.Profile.Main as Profile
import Entries.Admin.Pages.Statics.NotFound as NotFound
import Entries.Admin.Pages.Users.Edit.Update as UserEdit
import Entries.Admin.Pages.Users.Main as Users
import FontAwesome as FA
import Helpers exposing (log, send)
import Html as H exposing (Html, a, button, div, h1, h3, img, span, text)
import Html.Attributes exposing (class, style, title)
import Html.Events exposing (onClick)
import Platform.Cmd as PCmd
import Session as Session
import Shared.AdminLayout.ListItem as AdmListItem
import Shared.AdminLayout.Main as AdmLayout
import Url exposing (Url)


port saveJwt : String -> Cmd msg


loginUrl : String
loginUrl =
    "/public/login"


{-| Router
-}
routePage : Route.Route -> Model -> ( Model, Cmd Msg )
routePage route model =
    let
        resource =
            Api.authedResource model.session

        routedModel =
            { model | route = route }

        oldPages =
            model.pages

        upWith mp ( m, c ) setter =
            ( setter oldPages m, Cmd.map mp c )

        ( newPages, cmdPages ) =
            case route of
                Route.Home ->
                    upWith
                        Home
                        (Home.init (Just resource))
                        (\m h -> { m | home = h })

                Route.Profile ->
                    upWith
                        Profile
                        (Profile.reset resource model.pages.profile)
                        (\m p -> { m | profile = p })

                Route.ProfileEdit ->
                    upWith
                        ProfileEdit
                        (ProfileEdit.reset resource model.pages.profile)
                        (\m p -> { m | profile = p })

                Route.Gallery ->
                    upWith
                        Gallery
                        Gallery.init
                        (\m g -> { m | gallery = g })

                Route.Users ->
                    upWith
                        Users
                        Users.init
                        (\m u -> { m | users = u })

                Route.UserEdit id ->
                    upWith
                        UserEdit
                        (UserEdit.init (Just id))
                        (\m u -> { m | userEdit = u })

                Route.UserNew ->
                    upWith
                        UserEdit
                        (UserEdit.init Nothing)
                        (\m u -> { m | userEdit = u })

                Route.NotFound ->
                    ( oldPages, Cmd.none )
    in
    ( { routedModel | pages = newPages }, Cmd.map Page cmdPages )


routeUrl : Url -> Model -> ( Model, Cmd Msg )
routeUrl url model =
    case Route.url2Route url of
        Nothing ->
            ( model, Cmd.none )

        Just newRoute ->
            if model.route == newRoute then
                ( model, Cmd.none )

            else
                routePage newRoute model


updateUrlChange : Url -> Model -> ( Model, Cmd Msg )
updateUrlChange url model =
    let
        newModel =
            { model | currentUrl = url }
    in
    if model.loadingSession then
        ( newModel, getJwt () )

    else
        routeUrl url newModel


updateWithJwt : String -> Model -> ( Model, Cmd Msg )
updateWithJwt jwt model =
    if jwt == "" then
        ( { model
            | session = Session.Guest
            , loadingSession = False
            , pages = model.pages
          }
        , Cmd.batch
            [ saveJwt ""
            , Nav.load loginUrl
            ]
        )

    else
        let
            ( newModel, newCmd ) =
                routeUrl model.currentUrl model

            currentRoute =
                Route.url2Route model.currentUrl

            shouldRedirHome =
                currentRoute == Nothing || currentRoute == Just Route.Home

            destPath =
                if shouldRedirHome then
                    Route.route2Str Route.Home

                else
                    model.currentUrl.path
        in
        ( { newModel
            | session =
                Session.Logged
                    { nome = ""
                    , email = ""
                    , jwt = jwt
                    }
            , loadingSession = False
          }
        , Cmd.batch [ newCmd, Nav.pushUrl model.navKey destPath ]
        )



--updateWith : (msg -> PageMsg) -> (Pages -> p -> Pages) -> Model ( p, Cmd msg )


updateWith subMsg pageSetter model ( newPage, cmd ) =
    let
        newPages =
            pageSetter model.pages newPage
    in
    ( { model | pages = newPages }, PCmd.map (subMsg >> Page) cmd )


updatePage : Api.Resource -> PageMsg -> Model -> ( Model, Cmd Msg )
updatePage resources pmsg model =
    case pmsg of
        Home msg ->
            Home.update resources msg model.pages.home
                |> updateWith Home (\p sp -> { p | home = sp }) model

        Profile msg ->
            Profile.update resources msg model.pages.profile
                |> updateWith Profile (\p sp -> { p | profile = sp }) model

        ProfileEdit msg ->
            ProfileEdit.update resources msg model.pages.profile
                |> updateWith ProfileEdit (\p sp -> { p | profile = sp }) model

        Gallery msg ->
            Gallery.update resources msg model.pages.gallery
                |> updateWith Gallery (\p sp -> { p | gallery = sp }) model

        Users msg ->
            Users.update resources msg model.pages.users
                |> updateWith Users (\p us -> { p | users = us }) model

        UserEdit msg ->
            UserEdit.update resources msg model.pages.userEdit
                |> updateWith UserEdit (\p eus -> { p | userEdit = eus }) model


{-| Update
-}
update : Msg -> Model -> ( Model, Cmd Msg )
update msg_ model =
    let
        resource =
            Api.authedResource model.session
    in
    case msg_ of
        Session msg ->
            let
                ( session, cmd ) =
                    Session.update model.session msg
            in
            ( { model | session = session }, Cmd.map Session cmd )

        OnUrlRequest urlReq ->
            case urlReq of
                External str ->
                    ( model, Nav.load str )

                Internal url ->
                    ( model, Nav.pushUrl model.navKey url.path )

        OnUrlChange url ->
            updateUrlChange url model

        Page pmsg ->
            updatePage resource pmsg model

        GotJwt jwt ->
            updateWithJwt jwt model

        ToggleLogout ->
            ( { model | showLogoutModal = not model.showLogoutModal }, Cmd.none )

        Logout ->
            ( { model | session = Session.Guest }
            , Cmd.batch
                [ saveJwt ""
                , Nav.load loginUrl
                ]
            )

        NoOp ->
            ( model, Cmd.none )

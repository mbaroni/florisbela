module Entries.Admin.App.Msg exposing (Msg(..), PageMsg(..))

import Browser exposing (UrlRequest(..))
import Entries.Admin.Pages.Gallery.Main as Gallery
import Entries.Admin.Pages.Home.Main as Home
import Entries.Admin.Pages.Profile.Edit as ProfileEdit
import Entries.Admin.Pages.Profile.Main as Profile
import Entries.Admin.Pages.Users.Edit.Model as UserEdit
import Entries.Admin.Pages.Users.Main as Users
import Session as Session
import Url exposing (Url)


type Msg
    = Session Session.Msg
    | OnUrlRequest UrlRequest
    | OnUrlChange Url
    | Page PageMsg
    | GotJwt String
    | ToggleLogout
    | Logout
    | NoOp


type PageMsg
    = Home Home.Msg
    | Profile Profile.Msg
    | ProfileEdit ProfileEdit.Msg
    | Gallery Gallery.Msg
    | Users Users.Msg
    | UserEdit UserEdit.Msg

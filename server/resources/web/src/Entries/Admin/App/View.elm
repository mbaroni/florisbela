module Entries.Admin.App.View exposing (view)

import Api
import Browser exposing (Document, UrlRequest(..))
import Components.Modal as Modal
import Entries.Admin.App.Model exposing (Model)
import Entries.Admin.App.Msg exposing (Msg(..), PageMsg(..))
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Gallery.Main as Gallery
import Entries.Admin.Pages.Home.Main as Home
import Entries.Admin.Pages.Profile.Edit as ProfileEdit
import Entries.Admin.Pages.Profile.Main as Profile
import Entries.Admin.Pages.Statics.NotFound as NotFound
import Entries.Admin.Pages.Users.Edit.View as UserEdit
import Entries.Admin.Pages.Users.Main as Users
import FontAwesome as FA
import Helpers exposing (log)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, style, title)
import Session as Session
import Shared.AdminLayout.ListItem as AdmListItem
import Shared.AdminLayout.Main as AdmLayout


sideMenu =
    [ [ AdmListItem.Label (text "Home")
      , AdmListItem.Icon FA.home
      , AdmListItem.Href (Route.route2Str Route.Home)
      ]
    , [ AdmListItem.Label (text "Profile")
      , AdmListItem.Icon FA.userCircle
      , AdmListItem.Href (Route.route2Str Route.Profile)
      ]
    , [ AdmListItem.Label (text "Gallery")
      , AdmListItem.Icon FA.th
      , AdmListItem.Href (Route.route2Str Route.Gallery)
      ]
    , [ AdmListItem.Label (text "Usuários")
      , AdmListItem.Icon FA.users
      , AdmListItem.Href (Route.route2Str Route.Users)
      ]
    , [ AdmListItem.Label (text "Logout")
      , AdmListItem.Icon FA.signOutAlt
      , AdmListItem.OnClick ToggleLogout
      ]
    ]


topMenu =
    [ [ AdmListItem.Icon FA.th ]
    , [ AdmListItem.Icon FA.signOutAlt
      , AdmListItem.OnClick ToggleLogout
      , AdmListItem.Attr (title "Logout")
      ]
    ]


viewPage : Model -> Html Msg
viewPage model =
    let
        mountMainApp pgCtnt bcs =
            AdmLayout.view
                [ AdmLayout.Content pgCtnt
                , AdmLayout.Breadcrumbs bcs
                , AdmLayout.CurrentPath model.currentUrl.path
                , AdmLayout.Sidebar sideMenu
                , AdmLayout.Topbar topMenu
                , AdmLayout.Fluid False
                ]

        authedReqConf =
            Api.authedResource model.session

        subMount rend m =
            let
                ( bs, c ) =
                    rend
            in
            mountMainApp
                [ c |> Html.map (m >> Page) ]
                bs
    in
    case model.route of
        Route.Home ->
            subMount
                (Home.view model.pages.home)
                Home

        Route.Profile ->
            subMount
                (Profile.view model.pages.profile)
                Profile

        Route.ProfileEdit ->
            subMount
                (ProfileEdit.view model.pages.profile)
                ProfileEdit

        Route.Gallery ->
            subMount
                (Gallery.view authedReqConf model.pages.gallery)
                Gallery

        Route.Users ->
            subMount
                (Users.view authedReqConf model.pages.users)
                Users

        Route.UserEdit id ->
            subMount
                (UserEdit.view authedReqConf model.pages.userEdit)
                UserEdit

        Route.UserNew ->
            subMount
                (UserEdit.view authedReqConf model.pages.userEdit)
                UserEdit

        Route.NotFound ->
            NotFound.view


loadingDiv : Model -> Html msg
loadingDiv model =
    div
        [ class "d-flex"
        , class "h-100"
        , class "justify-content-center"
        , class "align-items-center"
        , class "text-white"
        ]
        [ div
            [ class "font-weight-bold"
            , style "font-size" "2rem"
            ]
            [ text "Loading..."
            , text (Session.toJson model.session)
            ]
        ]


viewContent : Model -> Html Msg
viewContent model =
    if model.loadingSession then
        loadingDiv model

    else
        case model.session of
            Session.Guest ->
                text "Guest"

            Session.Logged user ->
                viewPage model


viewConfirmLogout : Model -> Html Msg
viewConfirmLogout model =
    Modal.confirm
        [ Modal.Opened model.showLogoutModal
        , Modal.Body (text "Deseja fazer logout?")
        , Modal.OnClose ToggleLogout
        , Modal.OnOk Logout
        ]


view : Model -> Document Msg
view model =
    let
        body =
            [ viewContent model
            , viewConfirmLogout model
            ]

        title =
            "Page"
    in
    { title = title
    , body = body
    }

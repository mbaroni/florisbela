port module Entries.Admin.App.Subscriptions exposing (subscriptions)

import Entries.Admin.App.Model exposing (Model)
import Entries.Admin.App.Msg exposing (Msg(..))
import Session as Session


port gotJwt : (String -> msg) -> Sub msg


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map Session (Session.subscriptions model.session)
        , gotJwt GotJwt
        ]

module Entries.Admin.Pages.Statics.NotFound exposing (view)

import Html exposing (Attribute, Html, a, aside, br, div, footer, h4, header, li, nav, span, text, ul)


view : Html msg
view =
    div
        []
        [ span
            []
            [ text "Not found" ]
        ]

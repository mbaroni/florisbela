module Entries.Admin.Pages.Home.Main exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

--import Components.Form as F
--import Components.Form.Text as T

import Api
import Components.Card as C
import Dict as D
import Entries.Admin.App.Route exposing (Route(..), route2Str)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, href, src)



-----------------------------------------
--                MODEL                --
-----------------------------------------


type alias Model =
    {}


type Msg
    = NoOp



----------------------------------------
--                INIT                --
----------------------------------------


init : Maybe Api.Resource -> ( Model, Cmd Msg )
init mResource =
    ( {}, Cmd.none )



----------------------------------------
--               UPDATE               --
----------------------------------------


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )



----------------------------------------
--                VIEW                --
----------------------------------------


view : Model -> ( List ( String, String ), Html Msg )
view model =
    C.view
        [ C.Title (text "Example - User Form")
        , C.Separator
        , C.Body [ text "Home..." ]
        ]
        |> Tuple.pair [ ( "Home", "" ) ]

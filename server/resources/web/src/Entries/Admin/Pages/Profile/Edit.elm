module Entries.Admin.Pages.Profile.Edit exposing
    ( Msg
    , reset
    , update
    , view
    )

import Api
import Components.Button as Btn
import Components.Card as Card
import Components.Field.DatePicker as Dat
import Components.Field.File as Fil
import Components.Field.InputWrapper as IWrp
import Components.Field.Radio as Rad
import Components.Field.Text as Txt
import Components.Modal as Modal
import Components.Types exposing (..)
import Date exposing (Date)
import Dict exposing (Dict)
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Profile.Model exposing (..)
import FontAwesome as FA
import Helpers exposing (buildErrorDict, isValidEmail, log)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, href, src)
import Http
import Json.Decode as JD
import Maybe.Extra as ME



----------------------------------------
--                MSG                 --
----------------------------------------


type FormMsg
    = ChangeNome String
    | ChangeSexo String
    | ChangeDatNasc ( Maybe Date, Dat.Model )
    | ChangeFotos Fil.FValue


type Msg
    = GotUser (Result Http.Error User)
    | Form FormMsg
    | Submit
    | GotSave (Result (Maybe String) String)
    | DismissAlert
    | NoOp



----------------------------------------
--               RESET                --
----------------------------------------


reset : Api.Resource -> Model -> ( Model, Cmd Msg )
reset resource model =
    let
        ( newUserForm, cmd ) =
            case model.userState of
                Loaded user ->
                    ( Just user, Cmd.none )

                _ ->
                    ( Nothing, fetchUser resource GotUser )
    in
    ( { model | userForm = newUserForm }, cmd )



----------------------------------------
--               UPDATE               --
----------------------------------------


updateForm : FormMsg -> User -> ( Maybe User, Cmd FormMsg )
updateForm msg user =
    case msg of
        ChangeNome nome ->
            ( Just { user | nome = Just nome }, Cmd.none )

        ChangeSexo sx ->
            ( Just { user | sexo = Just (str2Sexo sx) }, Cmd.none )

        ChangeDatNasc ( datVal, datModel ) ->
            ( Just { user | datNascimento = datVal, datNascimentoModel = datModel }, Cmd.none )

        ChangeFotos fotos ->
            ( Just { user | fotos = log "fotos" fotos }, Cmd.none )


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        GotUser resp ->
            case resp of
                Ok user ->
                    ( { model | userForm = Just user }, Cmd.none )

                Err _ ->
                    ( model, Cmd.none )

        Form fmsg ->
            let
                ( userForm, fcmd ) =
                    model.userForm
                        |> Maybe.map (updateForm fmsg)
                        |> Maybe.withDefault ( model.userForm, Cmd.none )
            in
            ( { model | userForm = userForm }, Cmd.map Form fcmd )

        Submit ->
            let
                url =
                    [ "admin", "profile", "save" ]

                cmd =
                    model.userForm
                        |> Maybe.map user2MultiPart
                        |> Maybe.map (resource.post url [])
                        |> Maybe.map (Api.myExecRequest GotSave JD.string)
            in
            ( { model | submitting = ME.isJust cmd }, Maybe.withDefault Cmd.none cmd )

        GotSave res ->
            let
                alertStatus =
                    case res of
                        Ok _ ->
                            AlertOk

                        Err _ ->
                            AlertError
            in
            ( { model | submitting = False, alertStatus = alertStatus }, Cmd.none )

        DismissAlert ->
            ( { model | alertStatus = AlertClosed }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



----------------------------------------
--                VIEW                --
----------------------------------------


getUserErrors : User -> Dict String (List String)
getUserErrors user =
    buildErrorDict
        [ ( "email", [ ( isValidEmail user.email, "E-mail inválido" ) ] ) ]


viewUserForm : Bool -> User -> Html Msg
viewUserForm submitting user =
    let
        submitBar =
            div
                []
                [ Btn.view
                    [ Btn.Label (text "Salvar")
                    , Btn.Typ Success
                    , Btn.Icon FA.check
                    , Btn.Enabled (getUserErrors user |> Dict.isEmpty)
                    , Btn.Loading submitting
                    , Btn.OnClick Submit
                    ]
                , Btn.view
                    [ Btn.Label (text "Cancelar")
                    , Btn.Typ Default
                    , Btn.Icon FA.times
                    , Btn.Enabled (not submitting)
                    , Btn.Href (Route.route2Str Route.Profile)
                    ]
                ]

        sexoVal =
            user.sexo
                |> Maybe.map sexo2str
                |> Maybe.withDefault "-"
    in
    div
        []
        [ Txt.view
            [ Txt.Label (text "E-mail")
            , Txt.Name "email"
            , Txt.Value user.email
            , Txt.Size Large
            , Txt.ReadOnly True
            ]
        , Txt.view
            [ Txt.Label (text "Nome")
            , Txt.Name "nome"
            , Txt.Value (Maybe.withDefault "" user.nome)
            , Txt.Size Large
            , Txt.OnInput (ChangeNome >> Form)
            ]
        , Rad.view
            [ Rad.Label (text "Sexo")
            , Rad.Name "sexo"
            , Rad.Value sexoVal
            , Rad.OnInput (ChangeSexo >> Form)
            , Rad.Options
                [ ( "F", text "Fem.", True )
                , ( "M", text "Masc.", True )
                ]
            ]
        , Dat.view
            [ Dat.Label (text "Nascimento")
            , Dat.Name "datnasc"
            , Dat.Value user.datNascimento
            , Dat.Updater (ChangeDatNasc >> Form)
            , Dat.Model user.datNascimentoModel
            ]
        , Fil.view
            [ Fil.Label (text "Fotos")
            , Fil.Name "fotos"
            , Fil.Value user.fotos
            , Fil.OnUpdate (ChangeFotos >> Form)
            ]
        , IWrp.view
            [ IWrp.Label (text "")
            , IWrp.Input submitBar
            ]
        ]


viewUserNotLoaded : Html Msg
viewUserNotLoaded =
    text "user not loaded"


viewAlert : Model -> Html Msg
viewAlert model =
    let
        ( opened, title, body ) =
            case model.alertStatus of
                AlertClosed ->
                    ( False, text "", text "" )

                AlertError ->
                    ( True, text "Erro", text "Erro ao tentar salvar registro." )

                AlertOk ->
                    ( True, text "Sucesso", text "Registro salvo com sucesso." )
    in
    Modal.alert
        [ Modal.Opened opened
        , Modal.Body body
        , Modal.OnOk DismissAlert
        ]


view : Model -> ( List ( String, String ), Html Msg )
view model =
    let
        bcrumbs =
            [ ( "Profile", Route.route2Str Route.Profile )
            , ( "Edição", "" )
            ]

        userContent =
            model.userForm
                |> Maybe.map (viewUserForm model.submitting)
                |> Maybe.withDefault viewUserNotLoaded

        content =
            Card.view
                [ Card.Title (text "Profile")
                , Card.Separator
                , Card.Body
                    [ userContent
                    , viewAlert model
                    ]
                ]
    in
    ( bcrumbs, content )

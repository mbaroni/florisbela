module Entries.Admin.Pages.Profile.Model exposing
    ( AlertStatus(..)
    , FetchStatus(..)
    , Model
    , Sexo(..)
    , User
    , blankModel
    , decodeSexo
    , decodeUser
    , encodeUser
    , fetchUser
    , sexo2str
    , str2Sexo
    , user2MultiPart
    )

import Api
import Components.Field.DatePicker as DP
import Components.Field.File as Fil
import Date exposing (Date, toIsoString)
import Extra.Http as EH
import Helpers exposing (dateDecoder, dateEncoder, log, maybeNull, nuller, send)
import Http
import Json.Decode as JD
import Json.Encode as JE



--------------------------
--         SEXO         --
--------------------------


type Sexo
    = Masc
    | Fem


sexo2str : Sexo -> String
sexo2str sx =
    case sx of
        Masc ->
            "M"

        Fem ->
            "F"


str2Sexo : String -> Sexo
str2Sexo str =
    if str == "M" then
        Masc

    else
        Fem



--------------------------
--         USER         --
--------------------------


type alias User =
    { datNascimentoModel : DP.Model
    , fotos : Fil.FValue
    , nome : Maybe String
    , email : String
    , sexo : Maybe Sexo
    , datNascimento : Maybe Date
    , files : List Fil.Archive
    }


decodeSexo : JD.Decoder Sexo
decodeSexo =
    JD.string
        |> JD.andThen
            (\str ->
                case String.toUpper str of
                    "M" ->
                        JD.succeed Masc

                    "F" ->
                        JD.succeed Fem

                    _ ->
                        JD.succeed Masc
            )


fetchUser : Api.Resource -> (Result Http.Error User -> msg) -> Cmd msg
fetchUser resource msg =
    let
        expect =
            Http.expectJson
                msg
                decodeUser
    in
    Api.execRequest
        expect
        (resource.get
            [ "admin", "profile", "view" ]
            []
        )


fils : List Fil.Archive
fils =
    [ { id = 1
      , mimetype = "application/pdf"
      , size = 1024
      , filename = "documento.pdf"
      , url = "/download/documento.pdf"
      }
    ]


decodeUser : JD.Decoder User
decodeUser =
    JD.map6
        (User DP.initial)
        (JD.field "files" (JD.map Fil.init (JD.list Fil.decodeArchive)))
        (JD.field "nome" (maybeNull JD.string))
        (JD.field "email" JD.string)
        (JD.field "sexo" (JD.maybe decodeSexo))
        (JD.field "datNascimento" (maybeNull dateDecoder))
        (JD.field "files" (JD.list Fil.decodeArchive))


encodeUser : User -> JE.Value
encodeUser user =
    JE.object
        [ ( "nome", nuller JE.string user.nome )
        , ( "email", JE.string user.email )
        , ( "sexo", nuller (\x -> JE.string (sexo2str x)) user.sexo )
        , ( "datNascimento", nuller dateEncoder user.datNascimento )
        ]


user2MultiPart : User -> Http.Body
user2MultiPart user =
    [ EH.mStringPart "nome" user.nome
    , Http.stringPart "email" user.email
    , Http.stringPart "sexo" (user.sexo |> Maybe.map sexo2str |> Maybe.withDefault "")
    , EH.mDatePart "datNascimento" user.datNascimento
    ]
        ++ Fil.attchFValueParts "new_files" "removed_files" user.fotos
        |> Http.multipartBody



---------------------------------------
--         USER FETCH STATUS         --
---------------------------------------


type FetchStatus a
    = NotLoaded
    | Fetching
    | Loaded a
    | Error



---------------------------------------
--           ALERT STATUS            --
---------------------------------------


type AlertStatus
    = AlertClosed
    | AlertError
    | AlertOk



---------------------------------------
--               MODEL               --
---------------------------------------


type alias Model =
    { userState : FetchStatus User
    , userForm : Maybe User
    , submitting : Bool
    , alertStatus : AlertStatus
    }


blankModel : Model
blankModel =
    { userState = NotLoaded
    , userForm = Nothing
    , submitting = False
    , alertStatus = AlertClosed
    }

module Entries.Admin.Pages.Profile.Main exposing
    ( Msg(..)
    , init
    , reset
    , update
    , view
    )

import Api
import Components.Button as Btn
import Components.Card as C
import Components.Field.File as Fil
import Components.ListView as LView
import Components.ListView.Types as LViewT
import Components.Profiler as Prof
import Components.Synopsis as Syn
import Components.Types exposing (..)
import Date exposing (Date)
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Profile.Model exposing (..)
import FontAwesome as FA
import Html exposing (Attribute, Html, a, aside, br, div, footer, h4, header, li, nav, span, text, ul)
import Html.Attributes exposing (style)
import Http



---------------------------------------
--                MSG                --
---------------------------------------


type Msg
    = FetchUser
    | GotUser (Result Http.Error User)
    | NoOp



----------------------------------------
--                INIT                --
----------------------------------------


init : Maybe Api.Resource -> ( Model, Cmd Msg )
init mResource =
    let
        cmd =
            mResource
                |> Maybe.map (\r -> fetchUser r GotUser)
                |> Maybe.withDefault Cmd.none
    in
    ( blankModel, cmd )



----------------------------------------
--               UPDATE               --
----------------------------------------


reset : Api.Resource -> Model -> ( Model, Cmd Msg )
reset resource model =
    ( model, fetchUser resource GotUser )


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        FetchUser ->
            ( model, fetchUser resource GotUser )

        GotUser result ->
            case result of
                Ok user ->
                    ( { model | userState = Loaded user }, Cmd.none )

                Err err ->
                    ( { model | userState = Error }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



----------------------------------------
--                VIEW                --
----------------------------------------


viewUserProfile : FetchStatus User -> Html Msg
viewUserProfile userState =
    let
        m2txt =
            Maybe.withDefault "--"
    in
    case userState of
        Loaded user ->
            let
                txtSexo =
                    user.sexo
                        |> Maybe.map sexo2str
                        |> Maybe.withDefault "--"

                dscArquivos =
                    List.map
                        Fil.viewArchLink
                        user.files
                        |> div
                            [ style "display" "flex"
                            , style "flex-direction" "column"
                            ]

                dscDatNasc =
                    user.datNascimento
                        |> Maybe.map (Date.format "d/M/y")
                        |> Maybe.withDefault "--"
            in
            Prof.view
                [ ( text "", viewEditBtn )
                , ( text "Nome", text (m2txt user.nome) )
                , ( text "Idade", text user.email )
                , ( text "Sexo", text txtSexo )
                , ( text "Nascimento", text dscDatNasc )
                , ( text "Arquivos", dscArquivos )
                ]

        NotLoaded ->
            text "NotLoaded"

        Fetching ->
            text "loading..."

        Error ->
            text "error..."


viewEditBtn : Html Msg
viewEditBtn =
    Btn.view
        [ Btn.Icon FA.edit
        , Btn.Typ Success
        , Btn.Size Mini
        , Btn.Label (text "Editar")
        , Btn.Href (Route.route2Str Route.ProfileEdit)
        ]


view : Model -> ( List ( String, String ), Html Msg )
view model =
    C.view
        [ C.Title (text "Profile")
        , C.Separator
        , C.Body
            [ viewUserProfile model.userState
            ]
        ]
        |> Tuple.pair [ ( "Profile", "" ) ]

module Entries.Admin.Pages.Users.Edit.Update exposing (init, update)

import Api
import Entries.Admin.Pages.Users.Edit.Model exposing (..)
import Helpers exposing (send)
import Html exposing (Attribute, Html, div, h4, text)
import Http
import Maybe.Extra as ME



--------------------------------------
--               INIT               --
--------------------------------------


init : Maybe Int -> ( Model, Cmd Msg )
init mId =
    let
        cmd =
            Maybe.map (\id -> FetchUsuario id |> send) mId
                |> Maybe.withDefault Cmd.none

        usState =
            case mId of
                Just _ ->
                    Fetching

                Nothing ->
                    Got usBlankForm

        model =
            { usState = usState
            , touched = False
            , submitting = False
            , showPostErrorAlert = False
            }
    in
    ( model, cmd )



--------------------------------------
--              UPDATE              --
--------------------------------------


fetchUsuario : Api.Resource -> Int -> Cmd Msg
fetchUsuario resource id =
    let
        expect =
            Http.expectJson
                GotUsuario
                decodeUsuario
    in
    resource.get
        [ "admin", "users", "view", String.fromInt id ]
        []
        |> Api.execRequest expect


requestPostForm : Api.Resource -> UsuarioForm -> Cmd Msg
requestPostForm resource usf =
    let
        url =
            case usf.id of
                Just id ->
                    [ "admin", "users", "save", String.fromInt id ]

                Nothing ->
                    [ "admin", "users", "new" ]
    in
    Http.jsonBody (encodeUsForm usf)
        |> resource.post url []
        |> Api.myExecRequest SubmitGot decodeSaveResp


postForm : Api.Resource -> Model -> Maybe (Cmd Msg)
postForm resource model =
    case model.usState of
        Got usf ->
            if usFormIsValid usf then
                Just (requestPostForm resource usf)

            else
                Nothing

        _ ->
            Nothing


updateForm : FormMsg -> UsState -> ( UsState, Cmd FormMsg )
updateForm msg usState =
    case usState of
        Got usForm ->
            let
                ( newUsForm, usFormCmd ) =
                    case msg of
                        ChangePassword password ->
                            ( { usForm | password = password }, Cmd.none )

                        ChangePassword2 password2 ->
                            ( { usForm | password2 = password2 }, Cmd.none )

                        ChangeNome nome ->
                            ( { usForm | nome = nome }, Cmd.none )

                        ChangeEmail email ->
                            ( { usForm | email = email }, Cmd.none )

                        ChangeSexo sexo ->
                            ( { usForm | sexo = Just sexo }, Cmd.none )
            in
            ( Got newUsForm, usFormCmd )

        _ ->
            ( usState, Cmd.none )


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        FetchUsuario id ->
            ( { model | usState = Fetching }, fetchUsuario resource id )

        GotUsuario res ->
            case res of
                Ok usResp ->
                    ( { model | usState = Got (usFormFromResp usResp), touched = False }, Cmd.none )

                Err _ ->
                    ( { model | usState = NotFound }, Cmd.none )

        UpdateForm fmsg ->
            let
                ( usState, fcmd ) =
                    updateForm fmsg model.usState
            in
            ( { model | usState = usState, touched = True }, Cmd.map UpdateForm fcmd )

        Submit ->
            let
                mCmd =
                    postForm resource model

                newModel =
                    if ME.isJust mCmd then
                        { model | submitting = True }

                    else
                        model
            in
            ( newModel, Maybe.withDefault Cmd.none mCmd )

        SubmitGot result ->
            let
                newModel =
                    case result of
                        Ok resp ->
                            { model | submitting = False, touched = False }

                        Err _ ->
                            { model | submitting = False, showPostErrorAlert = True }
            in
            ( newModel, Cmd.none )

        DismissAlert ->
            ( { model | showPostErrorAlert = False }, Cmd.none )

module Entries.Admin.Pages.Users.Edit.View exposing (..)

import Api
import Components.Button as Btn
import Components.Card as Card
import Components.Field.Radio as Rad
import Components.Field.Static as Static
import Components.Field.Text as Txt
import Components.Misc exposing (loadingWrapper)
import Components.Modal as Modal
import Components.Types exposing (..)
import Entries.Admin.App.Route as Route
import Entries.Admin.Pages.Users.Edit.Model exposing (..)
import FontAwesome as FA
import Html exposing (Attribute, Html, div, h4, text)
import Html.Attributes exposing (style)
import Maybe.Extra as ME


viewNotFound : Html Msg
viewNotFound =
    div
        [ style "width" "100%"
        , style "padding" "30px"
        , style "font-align" "center"
        ]
        [ h4 []
            [ text "Not Found"
            ]
        ]


getTitle : Model -> String
getTitle model =
    case model.usState of
        Got usForm ->
            case usForm.id of
                Just _ ->
                    "Edição de usuário"

                Nothing ->
                    "Cadastro de novo usuário"

        _ ->
            ""


viewUsuarioForm : Model -> Html Msg
viewUsuarioForm model =
    case model.usState of
        Got usf ->
            let
                errDict =
                    formErrors usf
            in
            div
                []
                [ Txt.view
                    [ Txt.Label (text "E-mail")
                    , Txt.Name "email"
                    , Txt.Value usf.email
                    , Txt.Size XLarge
                    , Txt.OnInput (ChangeEmail >> UpdateForm)
                    , Txt.ReadOnly (ME.isJust usf.id)
                    , Txt.Required True
                    , Txt.FormErrors errDict
                    , Txt.PreAppend
                        [ Txt.AppIcon FA.envelope ]
                    ]
                , Txt.view
                    [ Txt.Label (text "Nome")
                    , Txt.Name "nome"
                    , Txt.Value usf.nome
                    , Txt.OnInput (ChangeNome >> UpdateForm)
                    , Txt.Size XLarge
                    , Txt.PreAppend
                        [ Txt.AppIcon FA.user ]
                    ]
                , Txt.view
                    [ Txt.Label (text "Senha")
                    , Txt.Name "password"
                    , Txt.Type Txt.Password
                    , Txt.FormErrors errDict
                    , Txt.Value usf.password
                    , Txt.OnInput (ChangePassword >> UpdateForm)
                    , Txt.Size Medium
                    , Txt.PreAppend
                        [ Txt.AppIcon FA.lock ]
                    ]
                , Txt.view
                    [ Txt.Label (text "Confirmação")
                    , Txt.Name "password2"
                    , Txt.Type Txt.Password
                    , Txt.FormErrors errDict
                    , Txt.Value usf.password2
                    , Txt.OnInput (ChangePassword2 >> UpdateForm)
                    , Txt.Size Medium
                    , Txt.PreAppend
                        [ Txt.AppIcon FA.lock ]
                    ]
                , Rad.view
                    [ Rad.Label (text "Sexo")
                    , Rad.Value (Maybe.withDefault "" usf.sexo)
                    , Rad.Name "sexo"
                    , Rad.OnInput (ChangeSexo >> UpdateForm)
                    , Rad.Options
                        [ ( "M", text "Masculino", True )
                        , ( "F", text "Feminino", True )
                        ]
                    ]
                , Static.view
                    (text "")
                    [ Btn.view
                        [ Btn.Label (text "Salvar")
                        , Btn.Icon FA.check
                        , Btn.Typ Success
                        , Btn.OnClick Submit
                        , Btn.Enabled (formIsValid model && model.touched)
                        , Btn.Loading model.submitting
                        ]
                    , Btn.view
                        [ Btn.Label (text "Cancelar")
                        , Btn.Icon FA.reply
                        , Btn.Enabled (not model.submitting)
                        , Btn.Href (Route.route2Str Route.Users)
                        ]
                    ]
                ]

        _ ->
            text ""


viewContent : Model -> Html Msg
viewContent model =
    case model.usState of
        Fetching ->
            loadingWrapper [] True

        NotFound ->
            viewNotFound

        Got usuario ->
            div
                []
                [ viewUsuarioForm model
                ]


viewAlert : Model -> Html Msg
viewAlert model =
    Modal.alert
        [ Modal.Opened model.showPostErrorAlert
        , Modal.OnOk DismissAlert
        , Modal.Body (text "Ocorreu um erro...")
        ]


view : Api.Resource -> Model -> ( List ( String, String ), Html Msg )
view resource model =
    Card.view
        [ Card.Title (text (getTitle model))
        , Card.Separator
        , Card.Body
            [ viewContent model
            , viewAlert model
            ]
        ]
        |> Tuple.pair
            [ ( "Usuários", Route.route2Str Route.Users )
            , ( "Usuário", "" )
            ]

module Entries.Admin.Pages.Users.Main exposing (Model, Msg, init, update, view)

import Api
import Components.Button as Btn
import Components.Card as C
import Components.Synopsis as Syn
import Components.Table as Table
import Components.Types exposing (..)
import Date exposing (Date)
import Entries.Admin.App.Route as Route
import FontAwesome as FA
import Helpers as H exposing (posixDayFormat, posixDecode, posixTimeFormat)
import Html exposing (Attribute, Html, div, text)
import Html.Attributes exposing (class)
import Json.Decode as JD
import Time as Time exposing (Posix)


type alias Usuario =
    { id : Int
    , email : String
    , createdAt : Posix
    }


usuarioDecoder : JD.Decoder Usuario
usuarioDecoder =
    JD.map3
        Usuario
        (JD.field "id" JD.int)
        (JD.field "email" JD.string)
        (JD.field "createdAt" posixDecode)


type alias Model =
    { usTable : Table.Model Usuario }


type Msg
    = UsTableMsg (Table.Msg Usuario)


init : ( Model, Cmd Msg )
init =
    let
        ( usTable, cmdUsTable ) =
            Table.initial True
    in
    ( { usTable = usTable }, Cmd.map UsTableMsg cmdUsTable )


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        UsTableMsg subMsg ->
            let
                ( usTable, cmd ) =
                    Table.update
                        subMsg
                        (usersTableOpts resource model)
            in
            ( { model | usTable = usTable }, Cmd.map UsTableMsg cmd )


usRowRenderer : List (Table.CellRenderer Usuario Msg)
usRowRenderer =
    [ { label = text "ID"
      , content = \u -> text (String.fromInt u.id)
      , aligment = Table.Left
      }
    , { label = text "E-mail"
      , content = \u -> text u.email
      , aligment = Table.Left
      }
    , { label = text "Criado em"
      , content = \u -> text (posixTimeFormat u.createdAt)
      , aligment = Table.Center
      }
    ]


usSynRenderer : Usuario -> List (Syn.Option Msg)
usSynRenderer u =
    [ Syn.Title (text u.email)
    , Syn.Corner (text (posixTimeFormat u.createdAt))
    , Syn.Action
        [ Btn.Label (text "Editar")
        , Btn.Icon FA.edit
        , Btn.Typ Success
        , Btn.Href (Route.route2Str (Route.UserEdit u.id))
        ]
    , Syn.Action
        [ Btn.Label (text "Excluir")
        , Btn.Icon FA.trash
        , Btn.Typ Danger
        ]
    ]


usersRequester : Api.Resource -> List ( String, String ) -> Api.ReqConfig
usersRequester resource params =
    resource.get
        [ "admin", "users", "list" ]
        params


viewInsertBtn : Html Msg
viewInsertBtn =
    div
        [ class "row"
        , class "mx-2"
        , class "mt-3"
        ]
        [ div
            [ class "col-sm-12" ]
            [ Btn.view
                [ Btn.Label (text "Novo")
                , Btn.Typ Success
                , Btn.Icon FA.plus
                , Btn.Href (Route.route2Str Route.UserNew)
                ]
            ]
        ]


usersTableOpts : Api.Resource -> Model -> List (Table.Option Usuario Msg)
usersTableOpts resource model =
    [ Table.TableModel model.usTable
    , Table.RowRenderer usRowRenderer
    , Table.SynRenderer usSynRenderer
    , Table.Decoder usuarioDecoder
    , Table.MsgMapper UsTableMsg
    , Table.Requester (usersRequester resource)
    ]


view : Api.Resource -> Model -> ( List ( String, String ), Html Msg )
view resource model =
    C.view
        [ C.Title (text "Usuários")
        , C.Content
            [ viewInsertBtn
            , Table.view (usersTableOpts resource model)
            ]
        ]
        |> Tuple.pair [ ( "Usuários", "" ) ]

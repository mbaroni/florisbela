module Entries.Admin.Pages.Users.Edit.Model exposing (..)

{-| Usuario Response
-}

import Dict exposing (Dict)
import Helpers exposing (buildErrorDict, isValidEmail, maybeNull, nuller)
import Http
import Json.Decode as JD
import Json.Encode as JE


type alias UsuarioResp =
    { id : Int
    , email : String
    , sexo : Maybe String
    , nome : Maybe String
    }


decodeUsuario : JD.Decoder UsuarioResp
decodeUsuario =
    JD.map4
        UsuarioResp
        (JD.field "id" JD.int)
        (JD.field "email" JD.string)
        (JD.field "sexo" (maybeNull parseSexo))
        (JD.field "nome" (maybeNull JD.string))


{-| Usuario Post Response
-}
type alias SaveResp =
    { msg : String }


decodeSaveResp : JD.Decoder SaveResp
decodeSaveResp =
    JD.succeed { msg = "Ok!" }


parseSexo : JD.Decoder String
parseSexo =
    JD.string
        |> JD.andThen
            (\s ->
                case String.toUpper s of
                    "M" ->
                        JD.succeed "M"

                    "F" ->
                        JD.succeed "F"

                    _ ->
                        JD.fail "Erro "
            )


{-| Usuario Form
-}
type alias UsuarioForm =
    { id : Maybe Int
    , email : String
    , nome : String
    , password : String
    , password2 : String
    , sexo : Maybe String
    }


usBlankForm : UsuarioForm
usBlankForm =
    { id = Nothing
    , email = ""
    , nome = ""
    , password = ""
    , password2 = ""
    , sexo = Nothing
    }


formErrors : UsuarioForm -> Dict String (List String)
formErrors usForm =
    let
        pwHasDigit =
            String.toList usForm.password
                |> List.any Char.isDigit

        pwIsLong =
            String.length usForm.password > 7

        pwRules =
            [ ( pwHasDigit, "Senha possuir digito." )
            , ( pwIsLong, "Senha deve ter mais de 7 caracteres." )
            ]

        pw2Rules =
            [ ( usForm.password == usForm.password2, "As senhas devem ser iguais." ) ]

        emailRules =
            [ ( isValidEmail usForm.email, "E-mail inválido" ) ]
    in
    buildErrorDict
        [ ( "password", pwRules )
        , ( "password2", pw2Rules )
        , ( "email", emailRules )
        ]


usFormFromResp : UsuarioResp -> UsuarioForm
usFormFromResp usResp =
    { id = Just usResp.id
    , email = usResp.email
    , nome = Maybe.withDefault "" usResp.nome
    , password = ""
    , password2 = ""
    , sexo = usResp.sexo
    }


encodeUsForm : UsuarioForm -> JD.Value
encodeUsForm usForm =
    JE.object
        [ ( "email", JE.string usForm.email )
        , ( "nome", JE.string usForm.nome )
        , ( "password", JE.string usForm.password )
        , ( "sexo", nuller JE.string usForm.sexo )
        ]


{-| State
-}
type UsState
    = Fetching
    | NotFound
    | Got UsuarioForm


type alias Model =
    { usState : UsState
    , touched : Bool
    , submitting : Bool
    , showPostErrorAlert : Bool
    }


type FormMsg
    = ChangePassword String
    | ChangePassword2 String
    | ChangeNome String
    | ChangeEmail String
    | ChangeSexo String


type Msg
    = FetchUsuario Int
    | GotUsuario (Result Http.Error UsuarioResp)
    | UpdateForm FormMsg
    | Submit
    | SubmitGot (Result (Maybe String) SaveResp)
    | DismissAlert


usFormIsValid : UsuarioForm -> Bool
usFormIsValid usForm =
    usForm
        |> formErrors
        |> Dict.isEmpty


formIsValid : Model -> Bool
formIsValid model =
    case model.usState of
        Got usf ->
            usFormIsValid usf

        _ ->
            False

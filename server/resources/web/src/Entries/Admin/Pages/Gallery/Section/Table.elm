module Entries.Admin.Pages.Gallery.Section.Table exposing
    ( Model
    , Msg(..)
    , initial
    , update
    , view
    )

import Api
import Components.Card as Card
import Components.Table as Tbl
import Helpers as H
import Html exposing (Html, text)
import Json.Decode as JD


type alias Model =
    { table : Tbl.Model Produto
    , person : Maybe Produto
    }


initial : ( Model, Cmd Msg )
initial =
    let
        ( tableModel, tableCmd ) =
            Tbl.initial True

        cmds =
            Cmd.map TableMsg tableCmd
    in
    ( { table = tableModel
      , person = Nothing
      }
    , cmds
    )


type Msg
    = Reset
    | SelectPerson Produto
    | TableMsg (Tbl.Msg Produto)



------------------------------------
--             UPDATE             --
------------------------------------


produtoRequester : Api.Resource -> List ( String, String ) -> Api.ReqConfig
produtoRequester resource ps =
    resource.get
        [ "admin", "produtos", "list" ]
        ps


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        Reset ->
            initial

        SelectPerson person ->
            ( { model | person = Just person }, Cmd.none )

        TableMsg subMsg ->
            let
                ( table, cmd ) =
                    Tbl.update subMsg (produtosTableOpts resource model)
            in
            ( { model | table = table }, Cmd.map TableMsg cmd )



------------------------------------
--              VIEW              --
------------------------------------


viewTableRaw : Html Msg
viewTableRaw =
    Tbl.raw
        { head =
            [ text "Nome"
            , text "E-mail"
            , text "Idade"
            ]
        , body =
            [ [ text "Marcos", text "marcos@email.com", text "32" ]
            , [ text "Paula", text "paula@email.com", text "33" ]
            ]
        }


type alias Produto =
    { id : Int
    , nome : String
    , valor : Float
    }


personDecoder : JD.Decoder Produto
personDecoder =
    JD.map3
        Produto
        (JD.field "id" JD.int)
        (JD.field "nome" JD.string)
        (JD.field "valor" JD.float)



-- mockList : List Produto
-- mockList =
--     [ { nome = "Marcos", snome = "Baroni", idade = 30 }
--     , { nome = "Demetris", snome = "Stamm", idade = 31 }
--     , { nome = "Khalid", snome = "Gutmann", idade = 27 }
--     , { nome = "Ariane", snome = "Champlin", idade = 24 }
--     , { nome = "Cole", snome = "Heller", idade = 15 }
--     , { nome = "Vince", snome = "Stracke", idade = 24 }
--     , { nome = "Jamie", snome = "McCullough", idade = 19 }
--     , { nome = "Kenny", snome = "Becker", idade = 28 }
--     , { nome = "Justyn", snome = "Erdman", idade = 41 }
--     , { nome = "Camille", snome = "Sauer", idade = 34 }
--     , { nome = "Pearlie", snome = "Kemmer", idade = 21 }
--     , { nome = "Reed", snome = "Rath", idade = 25 }
--     , { nome = "Judah", snome = "Hyatt", idade = 14 }
--     , { nome = "Donato", snome = "Spencer", idade = 25 }
--     ]


viewSelectedPerson : Model -> Html Msg
viewSelectedPerson model =
    case model.person of
        Just p ->
            text p.nome

        Nothing ->
            text "--"


produtosTableOpts : Api.Resource -> Model -> List (Tbl.Option Produto Msg)
produtosTableOpts resource model =
    let
        renderer =
            [ { label = text "ID"
              , content = \p -> text (String.fromInt p.id)
              , aligment = Tbl.Left
              }
            , { label = text "Nome"
              , content = \p -> text p.nome
              , aligment = Tbl.Left
              }
            , { label = text "Valor"
              , content = \p -> text ("R$ " ++ String.fromFloat p.valor)
              , aligment = Tbl.Left
              }
            ]
    in
    [ Tbl.TableModel model.table
    , Tbl.RowRenderer renderer
    , Tbl.MsgMapper TableMsg
    , Tbl.Decoder personDecoder
    , Tbl.Requester (produtoRequester resource)
    ]


view : Api.Resource -> Model -> Html Msg
view resource model =
    Card.view
        [ Card.Title (text "Tabela")
        , Card.Content [ Tbl.view (produtosTableOpts resource model) ]
        ]

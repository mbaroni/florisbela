module Entries.Admin.Pages.Gallery.Section.Inputs exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Api
import Components.Card as C
import Components.Field.Checkbox as Check
import Components.Field.DatePicker as DP
import Components.Field.File as Fil
import Components.Field.Radio as Radio
import Components.Field.Select as Sel
import Components.Field.Text as FText
import Components.Field.Typeahead as TAh
import Components.Form.Validator as V
import Components.Types as CT
import Date as DT exposing (Date)
import File exposing (File)
import FontAwesome as FA
import Helpers as H exposing (log)
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (style)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Maybe exposing (withDefault)
import Result



----------------------
--   TEXT FIELD
-- 1. Radio
-- 2. Check
-- 3. Select
-- 4. DatePicker
-- 5. "Lookup"/"Typeahead"
-- 6. Textarea
-- 7. File(s)
-- 8. Static
----------------------


type alias Produto =
    { id : Int
    , nome : String
    , valor : Float
    }


type alias Pessoa =
    { id : Int
    , nome : String
    , telefone : String
    }


type alias Usuario =
    { id : Int
    , nome : String
    , email : String
    }


type alias Model =
    { name : String
    , tipo : String
    , cebola : Bool
    , ovo : Bool
    , hamburger : String
    , dia : Maybe Date
    , diaModel : DP.Model
    , prodModel : TAh.Model Produto
    , produto : Maybe Produto
    , peModel : TAh.Model Pessoa
    , pessoa : Maybe Pessoa
    , fotos : Fil.FValue
    , newFotos : List File
    }


init : ( Model, Cmd Msg )
init =
    let
        ( newProdModel, cmdProdModel ) =
            TAh.initial

        ( newPeModel, cmdPeModel ) =
            TAh.initial
    in
    ( { name = ""
      , tipo = "1"
      , cebola = True
      , ovo = False
      , hamburger = "caseiro"
      , dia = Nothing
      , diaModel = DP.initial
      , prodModel = newProdModel
      , produto = Nothing
      , peModel = newPeModel
      , pessoa = Nothing
      , fotos = Fil.init []
      , newFotos = []
      }
    , Cmd.batch
        [ Cmd.map UpdateProduto cmdProdModel
        , Cmd.map UpdatePessoa cmdPeModel
        ]
    )


modelEncoder : Model -> String
modelEncoder model =
    JE.object
        [ ( "name", JE.string model.name )
        , ( "tipo", JE.string model.tipo )
        , ( "cebola", JE.bool model.cebola )
        , ( "ovo", JE.bool model.ovo )
        , ( "hamburger", JE.string model.hamburger )
        , ( "dia", H.nuller H.dateEncoder model.dia )
        , ( "produto", H.nuller produtoEncoder model.produto )
        , ( "pessoa", H.nuller pessoaEncoder model.pessoa )
        ]
        |> JE.encode 1


type Msg
    = UpdateName String
    | UpdateTipo String
    | UpdateCebola Bool
    | UpdateOvo Bool
    | UpdateHamburger String
    | UpdateDia ( Maybe Date, DP.Model )
    | UpdateProduto (TAh.Msg Produto)
    | UpdatePessoa (TAh.Msg Pessoa)
    | UpdateFotos Fil.FValue
    | NoOp


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resources msg model =
    let
        usGetterReqConfiger params =
            resources.get
                [ "api", "empresas", "list" ]
                params

        prodGetterReqConfiger params =
            resources.get
                [ "api", "produtos", "list" ]
                params
    in
    case msg of
        UpdateName newName ->
            ( { model | name = newName }, Cmd.none )

        UpdateTipo newTipo ->
            ( { model | tipo = newTipo }, Cmd.none )

        UpdateCebola cebola ->
            ( { model | cebola = cebola }, Cmd.none )

        UpdateOvo ovo ->
            ( { model | ovo = ovo }, Cmd.none )

        UpdateHamburger newHamburger ->
            ( { model | hamburger = newHamburger }, Cmd.none )

        UpdateDia ( dia, diaModel ) ->
            ( { model | dia = dia, diaModel = diaModel }, Cmd.none )

        UpdateProduto subMsg ->
            let
                ( newProdModel, mNewProduto, cmd ) =
                    TAh.update
                        { model = model.prodModel
                        , decoder = decodeProduto
                        , preFetch = False
                        , resGetter = prodGetterReqConfiger
                        }
                        subMsg
            in
            ( { model
                | prodModel = newProdModel
                , produto = Maybe.withDefault model.produto mNewProduto
              }
            , Cmd.map UpdateProduto cmd
            )

        UpdatePessoa subMsg ->
            let
                ( newUPeModel, mNewPessoa, cmd ) =
                    TAh.update
                        { model = model.peModel
                        , decoder = decodePessoa
                        , preFetch = False
                        , resGetter = usGetterReqConfiger
                        }
                        subMsg
            in
            ( { model
                | peModel = newUPeModel
                , pessoa = Maybe.withDefault model.pessoa mNewPessoa
              }
            , Cmd.map UpdatePessoa cmd
            )

        UpdateFotos fotos ->
            ( { model | fotos = fotos }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



--------------------------------------------------------------------------------
--       TEXT
--------------------------------------------------------------------------------


nameInput : Model -> Html Msg
nameInput model =
    FText.view
        [ FText.Label (text "Nome")
        , FText.Value model.name
        , FText.OnInput UpdateName
        , FText.Feedback "Exemplo de explicação"
        , FText.Placeholder "Nome..."
        , FText.Size CT.Large
        , FText.ContextType CT.Default
        , FText.PreAppend
            [ FText.AppIcon FA.infoCircle
            ]
        , FText.Append
            [ FText.AppContent (text "Append") ]
        ]



--------------------------------------------------------------------------------
--       SELECT
--------------------------------------------------------------------------------


typeSelect : Model -> Html Msg
typeSelect model =
    Sel.view
        [ Sel.Label (text "Tipo")
        , Sel.Value model.tipo
        , Sel.OnInput UpdateTipo
        , Sel.Options
            [ ( "1", "Tipo1" )
            , ( "2", "Tipo2" )
            , ( "3", "Tipo3" )
            ]
        ]



--------------------------------------------------------------------------------
--       CHECK
--------------------------------------------------------------------------------


checkInput : Model -> Html Msg
checkInput model =
    Check.view
        [ Check.Label (text "Adicionais")
        , Check.Options
            [ [ Check.Name "cebola"
              , Check.OpLabel (text "Cebola")
              , Check.Value model.cebola
              , Check.OnCheck UpdateCebola
              ]
            , [ Check.Name "ovo"
              , Check.OpLabel (text "Ovo")
              , Check.Value model.ovo
              , Check.OnCheck UpdateOvo
              ]
            , [ Check.Name "banana"
              , Check.OpLabel (text "Banana")
              , Check.Enabled False
              ]
            ]
        ]



--------------------------------------------------------------------------------
--       RADIO
--------------------------------------------------------------------------------


radioInput : Model -> Html Msg
radioInput model =
    Radio.view
        [ Radio.Label (text "Hamburger")
        , Radio.Name "hamburger"
        , Radio.Value model.hamburger
        , Radio.OnInput UpdateHamburger
        , Radio.Options
            [ ( "caseiro", text "Caseiro", True )
            , ( "picanha", text "Picanha", True )
            , ( "vegetal", text "Vegeral", False )
            ]
        ]



--------------------------------------------------------------------------------
--       DATE
--------------------------------------------------------------------------------


dateInput : Model -> Html Msg
dateInput model =
    DP.view
        [ DP.Label (text "Dia")
        , DP.Name "datdia"
        , DP.Value model.dia
        , DP.Model model.diaModel
        , DP.Updater UpdateDia
        , DP.Validators
            [ V.isUsefulDay
            ]
        ]



--------------------------------------------------------------------------------
--       TYPEAHEAD PRODUTO
--------------------------------------------------------------------------------


produtoEncoder : Produto -> JE.Value
produtoEncoder produto =
    JE.object
        [ ( "id", JE.int produto.id )
        , ( "nome", JE.string produto.nome )
        , ( "valor", JE.float produto.valor )
        ]


decodeProduto : JD.Decoder Produto
decodeProduto =
    JD.map3
        Produto
        (JD.field "id" JD.int)
        (JD.field "nome" JD.string)
        (JD.field "valor" JD.float)


viewProduto : Produto -> TAh.ItemViewer msg
viewProduto prod =
    ( prod.nome, Just (String.fromFloat prod.valor), FA.icon FA.user |> Just )


produtoTAOpts : Model -> List (TAh.Option Produto Msg)
produtoTAOpts model =
    [ TAh.Label (text "Produto")
    , TAh.Decoder decodeProduto
    , TAh.Viewer viewProduto
    , TAh.Value model.produto
    , TAh.PreFetch True
    ]


produtoInput : Model -> Html Msg
produtoInput model =
    TAh.view
        model.prodModel
        UpdateProduto
        (produtoTAOpts model)



--------------------------------------------------------------------------------
--       TYPEAHEAD PESSOA
--------------------------------------------------------------------------------


pessoaEncoder : Pessoa -> JE.Value
pessoaEncoder pessoa =
    JE.object
        [ ( "id", JE.int pessoa.id )
        , ( "nome", JE.string pessoa.nome )
        , ( "telefone", JE.string pessoa.telefone )
        ]


decodePessoa : JD.Decoder Pessoa
decodePessoa =
    JD.map3
        Pessoa
        (JD.field "id" JD.int)
        (JD.field "nome" JD.string)
        (JD.field "telefone" JD.string)


viewPessoa : Pessoa -> TAh.ItemViewer msg
viewPessoa pe =
    ( pe.nome, Just pe.telefone, Nothing )


pessoaTAOpts : Model -> List (TAh.Option Pessoa Msg)
pessoaTAOpts model =
    [ TAh.Label (text "Pessoa")
    , TAh.Decoder decodePessoa
    , TAh.Viewer viewPessoa
    , TAh.Value model.pessoa
    , TAh.PreFetch True
    ]


pessoaInput : Model -> Html Msg
pessoaInput model =
    TAh.view
        model.peModel
        UpdatePessoa
        (pessoaTAOpts model)



--------------------------------------------------------------------------------
--       FILE UPLOAD
--------------------------------------------------------------------------------


fotosInput : Model -> Html Msg
fotosInput model =
    Fil.view
        [ Fil.Label (text "Fotos")
        , Fil.Name "foto"
        , Fil.Value model.fotos
        , Fil.OnUpdate UpdateFotos
        ]



--------------------------------------------------------------------------------
--       VIEW
--------------------------------------------------------------------------------


view : Model -> Html Msg
view model =
    C.view
        [ C.Body
            [ nameInput model
            , typeSelect model
            , checkInput model
            , radioInput model
            , dateInput model
            , produtoInput model

            --, pessoaInput model
            , fotosInput model
            , modelEncoder model |> text
            ]
        ]

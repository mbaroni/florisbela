module Entries.Admin.Pages.Gallery.Section.Buttons exposing (Model, Msg, init, update, view)

import Components.Button as Btn
import Components.Card as C
import Components.Types as CT
import FontAwesome as FA
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)


type Msg
    = NoOp


type alias Model =
    {}


init : Model
init =
    {}


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


typeTuples : List ( String, CT.ContextType )
typeTuples =
    [ ( "Default", CT.Default )
    , ( "Primary", CT.Primary )
    , ( "Secundary", CT.Secundary )
    , ( "Success", CT.Success )
    , ( "Danger", CT.Danger )
    , ( "Warning", CT.Warning )
    , ( "Info", CT.Info )
    ]


sizeTuples : List ( String, CT.Size )
sizeTuples =
    [ ( "Mini", CT.Mini )
    , ( "Small", CT.Small )
    , ( "Medium", CT.Medium )
    , ( "Large", CT.Large )
    ]


renderBtn : ( String, CT.Size ) -> ( String, CT.ContextType ) -> Html Msg
renderBtn ( strSize, sizeType ) ( strType, ctxType ) =
    Btn.view
        [ Btn.Label (text strType)
        , Btn.Typ ctxType
        , Btn.Size sizeType
        , Btn.OnClick NoOp
        ]


renderBtnTypes : ( String, CT.Size ) -> List (Html Msg)
renderBtnTypes ( strSize, ctxSizee ) =
    List.map
        (renderBtn ( strSize, ctxSizee ))
        typeTuples


renderBtnIcons : C.Option Msg
renderBtnIcons =
    C.Body
        [ Btn.view
            [ Btn.Label (text "Star")
            , Btn.Icon FA.star
            ]
        , Btn.view
            [ Btn.Label (text "Loading")
            , Btn.Loading True
            ]
        , Btn.view
            [ Btn.Label (text "Disabled")
            , Btn.Enabled False
            ]
        , Btn.view
            [ Btn.Label (text "Light")
            , Btn.Icon FA.bolt
            , Btn.IconRight
            , Btn.Light True
            ]
        , Btn.view
            [ Btn.Label (text "Light")
            , Btn.Icon FA.square
            , Btn.Outline True
            ]
        ]


view : Model -> Html Msg
view model =
    let
        renderBtnSection szTuple =
            [ C.Title (text (Tuple.first szTuple ++ " Buttons"))
            , C.Body (renderBtnTypes szTuple)
            , C.Separator
            ]

        sizeSecs =
            List.concatMap
                renderBtnSection
                sizeTuples
    in
    C.view
        (sizeSecs
            ++ [ C.Title (text "Misc Buttons")
               , renderBtnIcons
               ]
        )

module Entries.Admin.Pages.Gallery.Section.Form exposing
    ( Model
    , Msg(..)
    , Produto
    , blankProd
    , initial
    , update
    , view
    )

import Api
import Components.Button as Btn
import Components.Card as C
import Components.Field.Checkbox as Chk
import Components.Form as F
import Components.Form.Types as FT
import Components.Form.Validator as Val
import Components.Misc exposing (loadingWrapper)
import Components.Types exposing (..)
import Date exposing (Date)
import Dict
import File exposing (File)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, br, div, input, li, span, text, ul)
import Html.Attributes exposing (style)
import Json.Decode as JD
import Json.Encode as JE
import Process
import Task



------------------------------------------------------------------------
--       CLIENTE
------------------------------------------------------------------------


type alias Cliente =
    { id : Int
    , nome : String
    , email : String
    , telefone : String
    }


clienteEncoder : Cliente -> JE.Value
clienteEncoder cliente =
    JE.object
        [ ( "id", JE.int cliente.id )
        , ( "nome", JE.string cliente.nome )
        , ( "email", JE.string cliente.email )
        , ( "telefone", JE.string cliente.telefone )
        ]


clienteDecoder : JD.Decoder Cliente
clienteDecoder =
    JD.map4
        Cliente
        (JD.field "id" JD.int)
        (JD.field "nome" JD.string)
        (JD.field "email" JD.string)
        (JD.field "telefone" JD.string)



--clienteView : Cliente -> TAh.ItemViewer msg
--clienteView pe =
--    ( pe.nome, Just pe.telefone, Nothing )
------------------------------------------------------------------------
--       PRODUTO
------------------------------------------------------------------------


type alias Produto =
    { nome : String
    , modelo : String
    , arcond : Bool
    , qportas : Bool
    , retirada : String
    , aniversario : Maybe Date
    , cliente : Maybe Cliente
    , fotos : List File
    }


produtoEncoder2 : Produto -> JE.Value
produtoEncoder2 produto =
    JE.object
        [ ( "nome", JE.string produto.nome )
        , ( "modelo", JE.string produto.modelo )
        , ( "arcond", JE.bool produto.arcond )
        , ( "qportas", JE.bool produto.qportas )
        , ( "retirada", JE.string produto.retirada )
        , ( "aniversario", H.mDateEncoder produto.aniversario )
        , ( "cliente", H.nuller clienteEncoder produto.cliente )
        ]


produtoEncoder resource =
    F.getJson (prodFields resource)


blankProd : Produto
blankProd =
    { nome = ""
    , modelo = ""
    , arcond = False
    , qportas = False
    , retirada = ""
    , aniversario = Nothing
    , cliente = Nothing
    , fotos = []
    }


type alias Model =
    { produto : Produto
    , formModel : FT.Model
    , showErrors : Bool
    , submitting : Bool
    , loading : Bool
    }


modelEncoder : Api.Resource -> Model -> JE.Value
modelEncoder resource model =
    JE.object
        [ ( "produto", produtoEncoder resource model.produto )
        , ( "show_errors", JE.bool model.showErrors )
        ]


initial : Model
initial =
    { produto = blankProd
    , formModel = F.initial
    , showErrors = False
    , submitting = False
    , loading = False
    }


type Msg
    = Reset
    | StopLoading
    | ProdUpdate FT.Model Produto
    | FormMsg FT.Msg
    | SetShowErrors Bool
    | SubmitProduto
    | StopSubmit
    | NoOp


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resource msg model =
    case msg of
        Reset ->
            let
                newModel =
                    { model
                        | produto = blankProd
                        , showErrors = False
                        , submitting = False
                        , loading = True
                    }

                dispatchStopLoading =
                    Process.sleep 500
                        |> Task.perform (always StopLoading)
            in
            ( newModel, dispatchStopLoading )

        StopLoading ->
            ( { model | loading = False }, Cmd.none )

        ProdUpdate newFormModel newProd ->
            ( { model | produto = newProd, formModel = newFormModel }, Cmd.none )

        FormMsg formMsg ->
            let
                fConfig =
                    { fields = prodFields resource
                    , model = model.formModel
                    , value = model.produto
                    , showErrors = False
                    }

                ( newFormModel, mNewProduto, cmd ) =
                    F.update fConfig formMsg

                newProduto =
                    Maybe.withDefault
                        model.produto
                        mNewProduto
            in
            ( { model
                | produto = newProduto
                , formModel = newFormModel
              }
            , Cmd.map FormMsg cmd
            )

        SetShowErrors newShowErrors ->
            ( { model | showErrors = newShowErrors }, Cmd.none )

        SubmitProduto ->
            let
                formIsValid =
                    model.produto
                        |> F.getErrors (prodFields resource)
                        |> Dict.isEmpty

                cmdDelayStop =
                    if formIsValid then
                        Process.sleep 1000
                            |> Task.perform (always StopSubmit)

                    else
                        Cmd.none
            in
            ( { model | submitting = formIsValid, showErrors = True }, cmdDelayStop )

        StopSubmit ->
            ( { model | submitting = False }, Cmd.none )

        NoOp ->
            ( model, Cmd.none )


prodFields : Api.Resource -> List (FT.Field Produto)
prodFields resource =
    [ FT.Text
        [ FT.Label (text "Nome")
        , FT.Name "nome"
        , FT.TxtGetter (\p -> p.nome)
        , FT.TxtSetter (\p s -> { p | nome = s })
        , FT.Size Medium
        , FT.Validators
            [ Val.minLength 5
            , Val.notEmpty
            ]
        ]
    , FT.Select
        [ FT.Label (text "Modelo")
        , FT.Name "modelo"
        , FT.SelGetter (\p -> p.modelo)
        , FT.SelSetter (\p s -> { p | modelo = s })
        , FT.SelNoEmpty
        , FT.SelOptions
            [ ( "vw", "Volkswagen" )
            , ( "ford", "Ford" )
            , ( "fiat", "Fiat" )
            , ( "chevrolet", "Chevrolet" )
            ]
        , FT.Validators
            [ Val.notEmpty
            ]
        ]
    , FT.Checkbox
        [ FT.Label (text "Adicionais")
        , FT.ChkOptions
            [ [ FT.ChkName "ar-condicionado"
              , FT.ChkLabel (text "Ar condicionado")
              , FT.ChkGetter (\p -> p.arcond)
              , FT.ChkSetter (\p ac -> { p | arcond = ac })
              ]
            , [ FT.ChkName "4-portas"
              , FT.ChkLabel (text "4 portas")
              , FT.ChkGetter (\p -> p.qportas)
              , FT.ChkSetter (\p qp -> { p | qportas = qp })
              , FT.ChkEnabled False
              ]
            ]
        ]
    , FT.Radio
        [ FT.Label (text "Local de retirada")
        , FT.Name "retirada"
        , FT.RadGetter (\p -> p.retirada)
        , FT.RadSetter (\p ret -> { p | retirada = ret })
        , FT.RadOptions
            [ ( "sp", text "São Paulo", True )
            , ( "rj", text "Rio de Janeiro", False )
            , ( "mg", text "Minas Gerais", True )
            ]
        , FT.Validators
            [ Val.notEmpty ]
        ]
    , FT.DatePicker
        [ FT.Label (text "Aniversário")
        , FT.Name "aniversario"
        , FT.DatGetter (\p -> p.aniversario)
        , FT.DatSetter (\p dat -> { p | aniversario = dat })
        , FT.Validators
            [ Val.isUsefulDay
            , Val.isDateRequired
            ]
        ]
    , FT.Typeahead
        [ FT.Label (text "Cliente")
        , FT.Name "cliente"
        , FT.TahGetter clienteGetter
        , FT.TahSetter clienteSetter
        , FT.TahFetcher (clienteFetcher resource)
        , FT.TahViewer clienteViewer
        ]
    , FT.File
        [ FT.Name "fotos"
        , FT.Label (text "Fotos")
        , FT.FilGetter (\p -> p.fotos)
        , FT.FilSetter (\p fs -> { p | fotos = fs })
        ]
    ]


clienteGetter : Produto -> Maybe JE.Value
clienteGetter produto =
    Maybe.andThen
        (clienteEncoder >> Just)
        produto.cliente


clienteSetter : Produto -> Maybe JE.Value -> Produto
clienteSetter p mVal =
    let
        oldCliente =
            p.cliente

        newCliente =
            case mVal of
                Just val ->
                    JD.decodeValue clienteDecoder val |> Result.toMaybe

                Nothing ->
                    Nothing
    in
    { p | cliente = newCliente }


clienteThumb : Cliente -> Html msg
clienteThumb cliente =
    let
        ( color, icon ) =
            if remainderBy 2 cliente.id == 0 then
                ( "#db5bda", FA.female )

            else
                ( "#1a8dbe", FA.male )
    in
    span
        [ style "font-size" "28px"
        , style "color" color
        ]
        [ FA.icon icon ]


clienteViewer : JE.Value -> F.TahItemViewer
clienteViewer val =
    val
        |> JD.decodeValue clienteDecoder
        |> Result.toMaybe
        |> Maybe.map (\c -> ( c.nome, Just c.email, clienteThumb c |> Just ))
        |> Maybe.withDefault F.emptyTahItemView


clienteFetcher : Api.Resource -> (List ( String, String ) -> Api.ReqConfig)
clienteFetcher resources params =
    resources.get
        [ "api", "empresas", "list" ]
        params


produtosValid : Produto -> Bool
produtosValid produto =
    String.length produto.nome > 5


viewForm : Api.Resource -> Model -> Html Msg
viewForm resource model =
    F.view
        { value = model.produto
        , model = model.formModel
        , fields = prodFields resource
        , showErrors = model.showErrors
        }
        |> Html.map FormMsg


submitBar : Api.Resource -> Model -> Html Msg
submitBar resource model =
    let
        formIsValid =
            model.produto
                |> F.getErrors (prodFields resource)
                |> Dict.isEmpty

        sendEnabled =
            (formIsValid && not model.submitting) || not model.showErrors
    in
    div []
        [ Btn.view
            [ Btn.Label (text "Enviar")
            , Btn.Typ Success
            , Btn.Icon FA.check
            , Btn.Enabled sendEnabled
            , Btn.OnClick SubmitProduto
            , Btn.Loading model.submitting
            ]
        , Btn.view
            [ Btn.Label (text "Parar")
            , Btn.Icon FA.times
            , Btn.Typ Danger
            , Btn.Enabled model.submitting
            , Btn.OnClick StopSubmit
            ]
        ]


dscValid : Api.Resource -> Model -> Html Msg
dscValid resource model =
    let
        errors =
            F.getErrors
                (prodFields resource)
                model.produto

        renderError err =
            li [] [ text err ]

        renderFieldErrors ( name, errs ) =
            errs
                |> List.map renderError
                |> (\es -> li [] [ text name, ul [] es ])

        dscErrors =
            Dict.toList errors
                |> List.map renderFieldErrors
                |> div []
    in
    if Dict.isEmpty errors then
        text "Valid!!!"

    else
        dscErrors


view : Api.Resource -> Model -> Html Msg
view resource model =
    C.view
        [ C.Body
            [ loadingWrapper
                [ viewForm resource model
                , text (modelEncoder resource model |> JE.encode 2)
                , br [] []
                , dscValid resource model
                ]
                model.loading
            ]
        , C.Separator
        , C.Body
            [ submitBar resource model
            ]
        ]

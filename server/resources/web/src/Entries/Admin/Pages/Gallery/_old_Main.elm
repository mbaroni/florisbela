module Pages.Gallery.Main exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

import Components.Form as Form
import Components.Form.Validator exposing (..)
import Components.Types exposing (..)
import Date exposing (Date)
import Dict exposing (Dict, empty, fromList)
import Html exposing (Html, div, h4, input, text)
import Html.Attributes as Att exposing (class, href, src)
import Html.Events exposing (onInput)
import Json.Encode exposing (encode)



{----------------------------------------
--               USUARIO               --
----------------------------------------}


{-| Value model
-}
type alias Usuario =
    { nome : String
    , email : String
    , isAdmin : Bool
    , isMember : Bool
    , isSubscribed : Bool
    , faixa : String
    , observacao : String
    , nascimento : Maybe Date
    }


type alias Model =
    { form : Form.FormModel Usuario
    , isLoading : Bool
    }


init : Model
init =
    let
        blankUsuario =
            { nome = ""
            , email = ""
            , isAdmin = False
            , isMember = False
            , isSubscribed = False
            , faixa = "crianca"
            , observacao = ""
            , nascimento = Nothing
            }

        myFormInit =
            { value = blankUsuario
            , isValid = False
            , formState = Form.initFormState
            }
    in
    { form = myFormInit
    , isLoading = False
    }


type Msg
    = UpdateForm (Form.FormModel Usuario)
    | SetIsLoading Bool


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        UpdateForm newForm ->
            ( { model | form = newForm }, Cmd.none )

        SetIsLoading newLoading ->
            ( { model | isLoading = newLoading }, Cmd.none )


usuarioFormConfig : Model -> Form.FormConfig Usuario Msg
usuarioFormConfig model =
    { validators = fromList [ ( "nome", [ maxLength 20 ] ) ]
    , fields =
        [ Form.Text
            { label = text "Nome"
            , name = "nome"
            , getter = \u -> u.nome
            , setter = \s u -> { u | nome = s }
            , placeholder = Just "Nome"
            , size_ = Nothing
            }
        , Form.Text
            { label = text "E-mail"
            , name = "email"
            , getter = \u -> u.email
            , setter = \s u -> { u | email = s }
            , placeholder = Just "E-mail"
            , size_ = Nothing
            }
        , Form.Checkbox
            { label = text "Acesso"
            , options =
                [ { name = "is_admin"
                  , label = text "Admin"
                  , getter = \u -> u.isAdmin
                  , setter = \b u -> { u | isAdmin = b }
                  , enabled = False
                  }
                , { name = "is_member"
                  , label = text "Membro"
                  , getter = \u -> u.isMember
                  , setter = \b u -> { u | isMember = b }
                  , enabled = False
                  }
                , { name = "is_subscribed"
                  , label = text "Inscrito"
                  , getter = \u -> u.isSubscribed
                  , setter = \b u -> { u | isSubscribed = b }
                  , enabled = True
                  }
                ]
            }
        , Form.Radio
            { label = text "Faixa-etária"
            , name = "faixa"
            , getter = \u -> Just u.faixa
            , setter = \s u -> { u | faixa = s }
            , options =
                [ { label = text "Criança"
                  , value = "crianca"
                  , enabled = True
                  }
                , { label = text "Jovem"
                  , value = "jovem"
                  , enabled = True
                  }
                , { label = text "Adulto"
                  , value = "adulto"
                  , enabled = False
                  }
                ]
            , enabled = True
            }
        , Form.Textarea
            { label = text "Observação"
            , name = "observacao"
            , getter = \u -> u.observacao
            , setter = \s u -> { u | observacao = s }
            , placeholder = Nothing
            , size_ = Just XLarge
            }
        , Form.DatePicker
            { label = text "Nascimento"
            , name = "nascimento"
            , getter = \u -> u.nascimento
            , setter = \d u -> { u | nascimento = d }
            , placeholder = Nothing
            }
        ]
    , formModel = model.form
    , onChange = UpdateForm
    , showErrors = False
    }


view : Model -> Html Msg
view model =
    let
        usFormConfig =
            usuarioFormConfig model
    in
    div
        [ class "card" ]
        [ div
            [ class "card-body" ]
            [ div
                [ class "card-title" ]
                [ h4
                    []
                    [ text "FormGallery page!!!" ]
                ]
            , Form.view usFormConfig
            , text
                (encode 1
                    (Form.toJSON usFormConfig.fields model.form.value)
                )
            ]
        ]

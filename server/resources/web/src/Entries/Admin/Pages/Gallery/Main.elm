module Entries.Admin.Pages.Gallery.Main exposing
    ( Model
    , Msg
    , init
    , update
    , view
    )

--import Entries.Admin.Pages.Gallery.Section.Form as Form

import Api
import Components.Badge as Badge
import Components.Button as Btn
import Components.Card as C
import Components.ListView as LView
import Components.ListView.Types as LViewT
import Components.Synopsis as Syn
import Components.Tabs as Tabs
import Components.Types as CT
import Entries.Admin.Pages.Gallery.Section.Buttons as GBtn
import Entries.Admin.Pages.Gallery.Section.Inputs as Inp
import Entries.Admin.Pages.Gallery.Section.Table as Tbl
import FontAwesome as FA
import Helpers exposing (log, send)
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as JD


type alias Model =
    { activeTabKey : String
    , btns : GBtn.Model
    , input : Inp.Model
    , tabl : Tbl.Model
    }


init : ( Model, Cmd Msg )
init =
    let
        ( newInput, cmdInput ) =
            Inp.init

        ( newTable, cmdTable ) =
            Tbl.initial
    in
    ( { activeTabKey = "botoes"
      , btns = GBtn.init
      , input = newInput
      , tabl = newTable
      }
    , Cmd.batch
        [ Cmd.map InpMsg cmdInput
        , Cmd.map TblMsg cmdTable
        ]
    )


type Msg
    = ChangeTab String
    | BtnMsg GBtn.Msg
    | InpMsg Inp.Msg
    | TblMsg Tbl.Msg
    | NoOp


update : Api.Resource -> Msg -> Model -> ( Model, Cmd Msg )
update resources msg model =
    case msg of
        ChangeTab key ->
            ( { model | activeTabKey = key }, Cmd.none )

        InpMsg inpMsg ->
            Inp.update resources inpMsg model.input
                |> (\( i, c ) -> ( { model | input = i }, Cmd.map InpMsg c ))

        TblMsg tblMsg ->
            Tbl.update resources tblMsg model.tabl
                |> (\( f, c ) -> ( { model | tabl = f }, Cmd.map TblMsg c ))

        BtnMsg btnMsg ->
            ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )



----------------------
--   BOTÕES
----------------------


tabBtn : Model -> Tabs.Option Msg
tabBtn model =
    Tabs.Tab "botoes"
        [ Tabs.Label "Botões"
        , Tabs.Icon FA.caretSquareRight
        , GBtn.view model.btns
            |> Html.map BtnMsg
            |> Tabs.Content
        ]



----------------------
--   INPUTS
----------------------


tabInputs : Model -> Tabs.Option Msg
tabInputs model =
    Tabs.Tab "inputs"
        [ Tabs.Label "Inputs"
        , Tabs.Icon FA.wpForms
        , Tabs.Content
            (Inp.view model.input |> Html.map InpMsg)
        ]



----------------------
--   FORMS
----------------------
-- tabForm : Api.Resource -> Model -> Tabs.Option Msg
-- tabForm resources model =
--     Tabs.Tab "form"
--         [ Tabs.Label "Form"
--         , Tabs.Icon FA.wpForms
--         , Tabs.OnSelect (FormMsg Form.Reset)
--         , Tabs.Content
--             (Form.view resources model.form |> Html.map FormMsg)
--         ]


tabTable : Api.Resource -> Model -> Tabs.Option Msg
tabTable resources model =
    Tabs.Tab "table"
        [ Tabs.Label "Table"
        , Tabs.Icon FA.table
        , Tabs.OnSelect (TblMsg Tbl.Reset)
        , Tabs.Content (Tbl.view resources model.tabl |> Html.map TblMsg)
        ]



------------------
-----  MAIN  -----
------------------


view : Api.Resource -> Model -> ( List ( String, String ), Html Msg )
view resources model =
    let
        cntn =
            C.view
                [ C.Title
                    (span []
                        [ text "Galeria "
                        , Badge.view CT.Success "Exemplos" True
                        ]
                    )
                , Tabs.view
                    model.activeTabKey
                    ChangeTab
                    [ tabBtn model
                    , tabInputs model

                    --, tabForm resources model
                    , tabTable resources model
                    ]
                    |> List.singleton
                    |> C.Content
                ]
    in
    div
        [ class "row" ]
        [ div
            [ class "col-md-12" ]
            [ cntn ]
        ]
        |> Tuple.pair [ ( "Gallery", "" ) ]

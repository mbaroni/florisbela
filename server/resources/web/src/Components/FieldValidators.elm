module Components.FieldValidators exposing
    ( assertOrError
    , buildErrorDict
    , isStrEmpty
    , isValidEmail
    )

import Dict exposing (Dict)
import Helpers as H
import Maybe.Extra as ME
import Tuple


buildErrorDict : List ( String, List (Maybe String) ) -> Dict String (List String)
buildErrorDict tuples =
    tuples
        |> List.map (Tuple.mapSecond ME.values)
        |> List.filter (\( x, l ) -> List.isEmpty l |> not)
        |> Dict.fromList


assertOrError : Bool -> String -> Maybe String
assertOrError right error =
    if right then
        Nothing

    else
        Just error


isValidEmail : String -> Maybe String
isValidEmail email =
    assertOrError
        (H.isValidEmail email)
        "E-mail inválido."


isStrEmpty : String -> Maybe String
isStrEmpty str =
    assertOrError
        (String.length str > 0)
        "Campo obrigatório."

module Components.Misc exposing
    ( loadingIcon
    , loadingWrapper
    )

import FontAwesome as FA
import Html exposing (Html, a, button, div, text)
import Html.Attributes exposing (class, disabled, href, style)


loadingIcon : Html msg
loadingIcon =
    FA.iconWithOptions
        FA.spinner
        FA.Solid
        [ FA.Animation FA.Pulse ]
        []


loadingWrapper : List (Html msg) -> Bool -> Html msg
loadingWrapper content loading =
    if loading then
        div
            [ style "display" "flex"
            , style "justify-content" "center"
            , style "align-items" "center"
            , style "font-size" "42px"
            , style "color" "#bebebe"
            , style "padding" "3rem"
            ]
            [ loadingIcon ]

    else
        div
            []
            content

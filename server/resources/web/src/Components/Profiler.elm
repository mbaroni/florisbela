module Components.Profiler exposing (view)

import Components.Types as T
import Html exposing (Html, table, tbody, td, tr)
import Html.Attributes exposing (class, style)


view : List ( Html msg, Html msg ) -> Html msg
view fields =
    let
        row ( lbl, info ) =
            tr
                [ class "profiler-row" ]
                [ td [ class "profiler-label" ] [ lbl ]
                , td [ class "profiler-info" ] [ info ]
                ]

        rows =
            List.map row fields
    in
    table
        []
        [ tbody
            []
            rows
        ]

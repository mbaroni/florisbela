module Components.Card exposing
    ( Option(..)
    , view
    )

import Html exposing (Html, div, h2, h3, h4, h5, text)
import Html.Attributes exposing (class, style)
import List


type Option msg
    = Title (Html msg)
    | Content (List (Html msg))
    | Body (List (Html msg))
    | Separator


renderOption : Option msg -> Html msg
renderOption option =
    case option of
        Title title ->
            div
                [ class "card-body" ]
                [ h4 [ class "card-title" ] [ title ] ]

        Content cntn ->
            div [] cntn

        Body body ->
            div [ class "card-body" ] body

        Separator ->
            div
                [ style "border-top" "1px solid #e9ecef" ]
                []


view : List (Option msg) -> Html msg
view options =
    div
        [ class "card" ]
        (List.map renderOption options)

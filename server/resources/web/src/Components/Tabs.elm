module Components.Tabs exposing
    ( Option(..)
    , TabOption(..)
    , view
    )

import FontAwesome as FA
import Helpers exposing (log, send, strHash)
import Html exposing (Html, a, div, h4, li, span, text, ul)
import Html.Attributes exposing (class, hidden, href, style)
import Html.Events exposing (onClick)
import List


type alias Model =
    { keyActived : String }


initial =
    { keyActived = "" }


type Msg msg
    = NoOp
    | SelectTab (Maybe msg) String



----------------------
--   TABS CONFIG
----------------------


type alias TabConfig msg =
    { key : String
    , content : Html msg
    , label : String
    , onSelect : Maybe msg
    , enabled : Bool
    , padded : Bool
    , icon : Maybe FA.Icon
    }


defaultTabConfig : String -> TabConfig msg
defaultTabConfig key =
    { key = key
    , content = text ""
    , label = ""
    , onSelect = Nothing
    , enabled = True
    , padded = False
    , icon = Nothing
    }


type TabOption msg
    = Content (Html msg)
    | Label String
    | OnSelect msg
    | Enabled Bool
    | Padded
    | Icon FA.Icon


applyTabOption : TabOption msg -> TabConfig msg -> TabConfig msg
applyTabOption option tabConf =
    case option of
        Content cntn ->
            { tabConf | content = cntn }

        Label lbl ->
            { tabConf | label = lbl }

        OnSelect m ->
            { tabConf | onSelect = Just m }

        Enabled enabled ->
            { tabConf | enabled = enabled }

        Padded ->
            { tabConf | padded = True }

        Icon ic ->
            { tabConf | icon = Just ic }



----------------------
--   CONFIG
----------------------


type Option msg
    = Tab String (List (TabOption msg))


type alias Config msg =
    { tabs : List (TabConfig msg) }


defaultConfig : Config msg
defaultConfig =
    { tabs = [] }


applyOption : Option msg -> Config msg -> Config msg
applyOption option conf =
    case option of
        Tab key tOpts ->
            { conf
                | tabs =
                    List.foldl
                        applyTabOption
                        (defaultTabConfig key)
                        tOpts
                        :: conf.tabs
            }



---------------
-- VIEW
---------------


tabIsActive : Config msg -> String -> TabConfig msg -> Bool
tabIsActive config keyActived tab =
    let
        subIsActive t =
            t.key == keyActived

        hasInside =
            List.any
                subIsActive
                config.tabs

        enabledLists =
            List.filter
                (\t -> t.enabled)
                config.tabs
    in
    if hasInside then
        tab.key == keyActived

    else
        case List.head enabledLists of
            Just t ->
                t.label == tab.label

            Nothing ->
                False


viewLabel : Config msg -> (String -> msg) -> String -> TabConfig msg -> Html msg
viewLabel config changePage key tabConfig =
    let
        isActive =
            tabIsActive config key tabConfig

        activeLinkClass =
            if isActive then
                class "tab-link__active"

            else
                class ""

        disabledLinkClass =
            if tabConfig.enabled then
                class ""

            else
                class "tab-link__disabled"

        linkNode =
            if tabConfig.enabled then
                a

            else
                span

        onClk =
            if tabConfig.enabled && not isActive then
                onClick (changePage tabConfig.key)

            else
                class ""

        txtIcon =
            case tabConfig.icon of
                Just ic ->
                    FA.icon ic

                Nothing ->
                    text ""

        linkText =
            span
                []
                [ txtIcon
                , text " "
                , text tabConfig.label
                ]
    in
    li
        [ class "tab-item"
        ]
        [ linkNode
            [ class "tab-link"
            , activeLinkClass
            , disabledLinkClass
            , href "#"
            , onClk
            ]
            [ linkText ]
        ]


viewLabels : Config msg -> String -> (String -> msg) -> Html msg
viewLabels conf key changePage =
    let
        content =
            List.map
                (viewLabel conf changePage key)
                conf.tabs
    in
    ul
        [ class "tab-nav" ]
        content


viewContent : Config msg -> String -> Html msg
viewContent config key =
    let
        paddedStyle t =
            if t.padded then
                style "padding" "1.25rem"

            else
                style "" ""

        renderTabContent tab =
            div
                [ hidden (tabIsActive config key tab |> not)
                , paddedStyle tab
                ]
                [ tab.content ]
    in
    div
        [ class "tab-content" ]
        (List.map renderTabContent config.tabs)


view : String -> (String -> msg) -> List (Option msg) -> Html msg
view key changePage options =
    let
        conf =
            List.foldr
                applyOption
                defaultConfig
                options
    in
    div
        []
        [ viewLabels conf key changePage
        , viewContent conf key
        ]

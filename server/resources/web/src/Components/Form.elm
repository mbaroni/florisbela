module Components.Form exposing
    ( TahItemViewer
    , emptyTahItemView
    , getErrors
    , getJson
    , initial
    , update
    , view
    )

--import Components.Form.File as FFil

import Api
import Components.Field.Typeahead as Tah
import Components.Form.Checkbox as FChk
import Components.Form.DatePicker as FDat
import Components.Form.Helpers exposing (..)
import Components.Form.Radio as FRad
import Components.Form.Select as FSel
import Components.Form.Text as FTxt
import Components.Form.Typeahead as FTah
import Components.Form.Types as T
import Components.Form.Validator as V
import Components.Types exposing (..)
import Date as D exposing (Date)
import Dict exposing (Dict)
import Helpers as H
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Maybe.Extra as ME
import Tuple



-----------------------------------------------
---           INITIAL                       ---
-----------------------------------------------


initial : T.Model
initial =
    { datModels = Dict.empty
    , tahModels = Dict.empty
    }


type alias TahItemViewer =
    Tah.ItemViewer T.Msg


emptyTahItemView =
    Tah.emptyItemView



----------------------------------------------------
---             JSON ENCODER                     ---
----------------------------------------------------


getJson : List (T.Field a) -> a -> JE.Value
getJson fields value =
    let
        appendJsonValue field fieldList =
            case field of
                T.Select opts ->
                    FSel.appendJsonValue value opts fieldList

                T.Text opts ->
                    FTxt.appendJsonValue value opts fieldList

                T.Radio opts ->
                    FRad.appendJsonValue value opts fieldList

                T.DatePicker opts ->
                    FDat.appendJsonValue value opts fieldList

                T.Checkbox opts ->
                    FChk.appendJsonValue value opts fieldList

                T.Typeahead opts ->
                    FTah.appendJsonValue value opts fieldList
    in
    fields
        |> List.foldr appendJsonValue []
        |> JE.object



----------------------------------------------------
---             PART GETTERS                     ---
----------------------------------------------------


getMultiPart : List (T.Field a) -> a -> List Http.Part
getMultiPart fields value =
    let
        appendPart field parts =
            case field of
                T.Select opts ->
                    FSel.appendPart value opts parts

                T.Text opts ->
                    FTxt.appendPart value opts parts

                T.Radio opts ->
                    FRad.appendPart value opts parts

                T.DatePicker opts ->
                    FDat.appendPart value opts parts

                T.Checkbox opts ->
                    FChk.appendPart value opts parts

                T.Typeahead opts ->
                    parts
    in
    fields
        |> List.foldr appendPart []



-------------------------------------------------
---           FORM ERRORS                     ---
-------------------------------------------------


getErrors : List (T.Field a) -> a -> Dict String (List String)
getErrors fields value =
    let
        getNameAndErrors field =
            Maybe.andThen
                (\n -> Just ( n, getFieldErrors value field ))
                (getFieldName field)
    in
    fields
        |> List.map getNameAndErrors
        |> ME.values
        |> List.filter (\( n, es ) -> List.isEmpty es |> not)
        |> Dict.fromList


update : T.Config a -> T.Msg -> ( T.Model, Maybe a, Cmd T.Msg )
update config msg =
    case msg of
        T.TxtUpdate name txtValue ->
            let
                newValue =
                    FTxt.update config name txtValue
            in
            ( config.model, Just newValue, Cmd.none )

        T.SelUpdate name selValue ->
            let
                newValue =
                    FSel.update config name selValue
            in
            ( config.model, Just newValue, Cmd.none )

        T.ChkUpdate name selValue ->
            let
                newValue =
                    FChk.update config name selValue
            in
            ( config.model, Just newValue, Cmd.none )

        T.RadUpdate name radValue ->
            let
                newValue =
                    FRad.update config name radValue
            in
            ( config.model, Just newValue, Cmd.none )

        T.DatUpdate name ( newMDat, newDatModel ) ->
            let
                oldModel =
                    config.model

                newDatModels =
                    Dict.insert
                        name
                        newDatModel
                        oldModel.datModels

                newModel =
                    { oldModel | datModels = newDatModels }

                newValue =
                    FDat.update config name newMDat
            in
            ( newModel, Just newValue, Cmd.none )

        T.TahUpdate name subMsg ->
            let
                oldModel =
                    config.model

                ( newTahModel, newValue, cmd ) =
                    FTah.update
                        config
                        name
                        subMsg

                newModel =
                    { oldModel
                        | tahModels =
                            Dict.insert
                                name
                                newTahModel
                                oldModel.tahModels
                    }
            in
            ( newModel, Just newValue, Cmd.map (T.TahUpdate name) cmd )

        T.NoOp ->
            ( config.model, Nothing, Cmd.none )



--------------------------------------------
---           FORM BUILDING              ---
--------------------------------------------


renderfield : T.Config a -> Dict String (List V.Validator) -> T.Field a -> Html T.Msg
renderfield config valDict field =
    let
        validators =
            getFieldName field
                |> Maybe.andThen (\name -> Dict.get name valDict)
                |> Maybe.withDefault []
    in
    case field of
        T.Text opts ->
            FTxt.view config validators opts

        T.Select opts ->
            FSel.view config validators opts

        T.Checkbox opts ->
            FChk.view config opts

        T.Radio opts ->
            FRad.view config validators opts

        T.DatePicker opts ->
            FDat.view config validators opts

        T.Typeahead opts ->
            FTah.view config validators opts


view : T.Config a -> Html T.Msg
view config =
    let
        subRenderfield =
            config.fields
                |> buildValidatorDict
                |> renderfield config
    in
    config.fields
        |> List.map subRenderfield
        |> div []

module Components.Types exposing
    ( ContextType(..)
    , Size(..)
    , ctxType2Str
    , size2Str
    )


type ContextType
    = Default
    | Primary
    | Secundary
    | Info
    | Success
    | Warning
    | Danger


type Size
    = Mini
    | Small
    | Medium
    | Large
    | XLarge


size2Str : Size -> String
size2Str sz =
    case sz of
        Mini ->
            "mini"

        Small ->
            "small"

        Medium ->
            "medium"

        Large ->
            "large"

        XLarge ->
            "xlarge"


ctxType2Str : ContextType -> String
ctxType2Str typ =
    case typ of
        Default ->
            "default"

        Primary ->
            "primary"

        Secundary ->
            "secundary"

        Info ->
            "info"

        Success ->
            "success"

        Warning ->
            "warning"

        Danger ->
            "danger"

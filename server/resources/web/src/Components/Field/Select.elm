module Components.Field.Select exposing
    ( Option(..)
    , view
    )

import Components.Field.InputWrapper as IW
import Components.Form.Validator as V
import Helpers as Help
import Html exposing (Html, option, select, text)
import Html.Attributes exposing (class, placeholder, selected, style, type_, value)
import Html.Events exposing (onInput)


emptyValToken : String
emptyValToken =
    "motWtIXNOUNG1myJOcNn"


type alias Config msg =
    { label : Maybe (Html msg)
    , name : String
    , value : String
    , onInput : Maybe (String -> msg)
    , feedback : String
    , noEmpty : Bool
    , options : List ( String, String )
    , validators : List V.Validator
    , required : Bool
    }


default : Config msg
default =
    { label = Nothing
    , name = ""
    , value = ""
    , onInput = Nothing
    , feedback = ""
    , noEmpty = False
    , options = []
    , validators = []
    , required = False
    }


type Option msg
    = Label (Html msg)
    | Name String
    | Value String
    | OnInput (String -> msg)
    | Feedback String
    | Options (List ( String, String ))
    | Validators (List V.Validator)
    | Required Bool
    | NoEmpty


applyOption : Option msg -> Config msg -> Config msg
applyOption opt config =
    case opt of
        Label lbl ->
            { config | label = Just lbl }

        Name name ->
            { config | name = name }

        Value val ->
            { config | value = val }

        OnInput onInput ->
            { config | onInput = Just onInput }

        Feedback feedback ->
            { config | feedback = feedback }

        Options options ->
            { config | options = config.options ++ options }

        Validators valids ->
            { config | validators = config.validators ++ valids }

        Required required ->
            { config | required = required }

        NoEmpty ->
            { config | noEmpty = True }


view : List (Option msg) -> Html msg
view opts =
    let
        config =
            List.foldl
                applyOption
                default
                opts

        selectedAttr val =
            if config.value == "" && config.noEmpty then
                class ""

            else
                selected (config.value == val)

        stdfyOnInput val =
            if val == emptyValToken then
                ""

            else
                val

        onInputAttr =
            case config.onInput of
                Just onInp ->
                    [ onInput (stdfyOnInput >> onInp) ]

                Nothing ->
                    []

        attrs =
            [ style "margin" "3px 0"
            , style "padding" "3px"
            ]
                ++ onInputAttr

        emptyOpt =
            if config.noEmpty && config.value /= "" then
                []

            else
                [ option
                    [ value emptyValToken
                    , selectedAttr ""
                    ]
                    [ text "Escolher..." ]
                ]

        renderOptions ( val, lbl ) =
            option
                [ value val
                , selectedAttr val
                ]
                [ text lbl ]

        children =
            List.map
                renderOptions
                config.options
                |> (++) emptyOpt

        errors =
            V.getStrErrors config.validators config.value

        input =
            select
                attrs
                children

        iWOpts =
            [ IW.Input input
            , IW.Required config.required
            ]
                |> Help.appIfJust (Maybe.map IW.Label config.label)
                |> Help.appIfJust (Maybe.map IW.Error (List.head errors))
    in
    IW.view iWOpts

module Components.Field.Checkbox exposing
    ( CheckOption(..)
    , Option(..)
    , view
    )

import Components.Field.InputWrapper as IW
import Helpers as Help
import Html exposing (Html, div, input, label, option, select, span, text)
import Html.Attributes exposing (checked, class, disabled, for, id, name, placeholder, style, type_, value)
import Html.Events exposing (onClick)
import Maybe.Extra as ME



----------------------
--   CHECK OPTION
----------------------


type alias CheckConfig msg =
    { name : String
    , label : Html msg
    , value : Bool
    , onCheck : Maybe (Bool -> msg)
    , enabled : Bool
    }


defaultCheck : CheckConfig msg
defaultCheck =
    { name = ""
    , label = text ""
    , value = False
    , onCheck = Nothing
    , enabled = True
    }


type CheckOption msg
    = Name String
    | OpLabel (Html msg)
    | Value Bool
    | OnCheck (Bool -> msg)
    | Enabled Bool


type Option msg
    = Label (Html msg)
    | Options (List (List (CheckOption msg)))


applyCheckOption : CheckOption msg -> CheckConfig msg -> CheckConfig msg
applyCheckOption opt config =
    case opt of
        Name name ->
            { config | name = name }

        OpLabel lbl ->
            { config | label = lbl }

        Value val ->
            { config | value = val }

        OnCheck onCheck ->
            { config | onCheck = Just onCheck }

        Enabled enabled ->
            { config | enabled = enabled }


renderOption : List (CheckOption msg) -> Html msg
renderOption opts =
    let
        config =
            List.foldl
                applyCheckOption
                defaultCheck
                opts

        classDisabled =
            if config.enabled && ME.isJust config.onCheck then
                class ""

            else
                class "disabled"

        onClick_ =
            case config.onCheck of
                Just onChk ->
                    onClick (onChk (not config.value))

                Nothing ->
                    class ""
    in
    div
        [ class "custom-check" ]
        [ label
            [ class "custom-check-label"
            , classDisabled
            , for config.name
            ]
            [ config.label
            , input
                [ type_ "checkbox"
                , class "custom-control-input"
                , name config.name
                , id config.name
                , disabled (not config.enabled)
                , checked config.value
                , onClick_
                ]
                []
            , span
                [ class "custom-checkmark"
                , classDisabled
                ]
                []
            ]
        ]



----------------------
--   FIELD
----------------------


type alias Config msg =
    { label : Maybe (Html msg)
    , options : List (List (CheckOption msg))
    }


default : Config msg
default =
    { label = Nothing
    , options = []
    }


applyOption : Option msg -> Config msg -> Config msg
applyOption opt config =
    case opt of
        Label lbl ->
            { config | label = Just lbl }

        Options opts ->
            { config | options = config.options ++ opts }


view : List (Option msg) -> Html msg
view opts =
    let
        config =
            List.foldl
                applyOption
                default
                opts

        input =
            div
                [ class "custom-check-box" ]
                (List.map renderOption config.options)
    in
    IW.view
        ([ IW.Input input ]
            |> Help.appIfJust
                (Maybe.map
                    IW.Label
                    config.label
                )
        )

module Components.Field.File exposing
    ( Archive
    , FValue
    , Option(..)
    , attchFValueParts
    , decodeArchive
    , init
    , view
    , viewArchLink
    )

import Components.Button as Btn
import Components.Field.InputWrapper as IW
import Components.Modal as Modal
import Components.Types exposing (..)
import Dict exposing (Dict)
import File exposing (File)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, a, div, input, label, span, text)
import Html.Attributes exposing (checked, class, disabled, for, href, id, multiple, name, placeholder, style, target, type_, value)
import Html.Events exposing (on)
import Http
import Json.Decode as JD
import Maybe.Extra as ME



-------------------------------------------
--                 MODEL                 --
-------------------------------------------


type alias Archive =
    { id : Int
    , mimetype : String
    , size : Int
    , filename : String
    , url : String
    }


file2Archive : File -> Archive
file2Archive file =
    { id = 0
    , mimetype = File.mime file
    , size = File.size file
    , filename = File.name file
    , url = ""
    }


decodeArchive : JD.Decoder Archive
decodeArchive =
    JD.map5
        Archive
        (JD.field "id" JD.int)
        (JD.field "mimetype" JD.string)
        (JD.field "size" JD.int)
        (JD.field "filename" JD.string)
        (JD.field "url" JD.string)


type ToRemove
    = RmArchive Archive
    | RmFile File
    | RmNone


type alias FValue =
    { current : List Archive
    , removed : List Archive
    , new : List File
    , toRemove : ToRemove
    }


init : List Archive -> FValue
init current =
    { current = current
    , removed = []
    , new = []
    , toRemove = RmNone
    }


isOpened : FValue -> Bool
isOpened fvalue =
    case fvalue.toRemove of
        RmNone ->
            False

        _ ->
            True


getRmFilename : FValue -> String
getRmFilename fvalue =
    case fvalue.toRemove of
        RmNone ->
            ""

        RmArchive archive ->
            archive.filename

        RmFile file ->
            File.name file


type Option msg
    = Label (Html msg)
    | Name String
    | Placeholder String
    | Feedback String
    | Required Bool
    | OnUpdate (FValue -> msg)
    | Value FValue
    | NoOpt


type alias Config msg =
    { label : Maybe (Html msg)
    , placeholder : String
    , name : String
    , feedback : String
    , required : Bool
    , onUpdate : Maybe (FValue -> msg)
    , value : FValue
    }


default : Config msg
default =
    { label = Nothing
    , placeholder = ""
    , name = ""
    , feedback = ""
    , required = False
    , onUpdate = Nothing
    , value = init []
    }


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Label label ->
            { config | label = Just label }

        Placeholder placeholder ->
            { config | placeholder = placeholder }

        Name name ->
            { config | name = name }

        Feedback feedback ->
            { config | feedback = feedback }

        Required required ->
            { config | required = required }

        OnUpdate onUpdate ->
            { config | onUpdate = Just onUpdate }

        Value value ->
            { config | value = value }

        NoOpt ->
            config


filesDecoder : JD.Decoder (List File)
filesDecoder =
    JD.at
        [ "target", "files" ]
        (JD.list File.decoder)


attchPreAppend : Html msg
attchPreAppend =
    div
        [ class "custom-file-preappend" ]
        [ FA.icon FA.paperclip ]


iconDict : Dict String FA.Icon
iconDict =
    Dict.fromList
        [ ( "image/gif", FA.fileImage )
        , ( "image/jpeg", FA.fileImage )
        , ( "image/png", FA.fileImage )
        , ( "audio/mpeg", FA.fileAudio )
        , ( "application/pdf", FA.filePdf )
        , ( "application/zip", FA.fileArchive )
        ]


getMimeIcon : String -> FA.Icon
getMimeIcon mimetype =
    mimetype
        |> (\f -> Dict.get f iconDict)
        |> Maybe.withDefault FA.paperclip


attchFValueParts : String -> String -> FValue -> List Http.Part
attchFValueParts newfield removedfield fvalue =
    let
        newFiles =
            if removedfield == "" then
                []

            else if List.length fvalue.new == 0 then
                []

            else
                List.map (Http.filePart newfield) fvalue.new

        newRemoved =
            if removedfield == "" then
                []

            else if List.length fvalue.removed == 0 then
                []

            else
                fvalue.removed
                    |> List.map .id
                    |> List.map String.fromInt
                    |> List.map (Http.stringPart (removedfield ++ "[]"))
    in
    newFiles ++ newRemoved



-------------------------------------------
--            MODEL MODIFIERS            --
-------------------------------------------


removeFile : FValue -> File -> FValue
removeFile fvalue file =
    let
        newNew =
            List.filter
                (\f -> File.name file /= File.name f && File.size file /= File.size f)
                fvalue.new
    in
    { fvalue | new = newNew }


addFile : FValue -> File -> FValue
addFile fvalue file =
    { fvalue | new = fvalue.new ++ [ file ] }


removeArchive : FValue -> Archive -> FValue
removeArchive fvalue archive =
    let
        newRemoved =
            List.filter
                (\a -> archive.id /= a.id)
                fvalue.removed
    in
    { fvalue | removed = newRemoved }


applyRemove : FValue -> FValue
applyRemove fvalue =
    case fvalue.toRemove of
        RmArchive archive ->
            { fvalue
                | removed = fvalue.removed ++ [ archive ]
                , toRemove = RmNone
            }

        RmFile file ->
            { fvalue
                | new =
                    List.filter
                        (\f -> File.name file /= File.name f && File.size file /= File.size f)
                        fvalue.new
                , toRemove = RmNone
            }

        RmNone ->
            fvalue


requestRemoveArchive : FValue -> Archive -> FValue
requestRemoveArchive fvalue archive =
    { fvalue | toRemove = RmArchive archive }


requestRemoveFile : FValue -> File -> FValue
requestRemoveFile fvalue file =
    { fvalue | toRemove = RmFile file }



-------------------------------------------
--                RENDERS                --
-------------------------------------------


getNiceFileDsc : Archive -> String
getNiceFileDsc archive =
    H.abrevFileName
        30
        archive.filename
        ++ " ("
        ++ H.humanizeSize archive.size
        ++ ")"


renderFile : Archive -> Maybe msg -> Html msg
renderFile archive mOnRemove =
    let
        dscFile =
            if archive.url == "" then
                text (getNiceFileDsc archive)

            else
                a
                    [ href archive.url, target "_blank" ]
                    [ text (getNiceFileDsc archive) ]

        btnRm =
            case mOnRemove of
                Just onRemove ->
                    Btn.view
                        [ Btn.Icon FA.times
                        , Btn.Class "btn-input-append"
                        , Btn.Typ Danger
                        , Btn.OnClick onRemove
                        ]

                Nothing ->
                    text ""
    in
    div
        [ class "custom-file-loaded"
        ]
        [ div
            [ class "custom-file-preappend" ]
            [ FA.icon (getMimeIcon archive.mimetype) ]
        , div
            [ class "form-control"
            , style "background-color" "#f7f7f7"
            , style "cursor" "default"
            ]
            [ dscFile ]
        , btnRm
        ]


renderArchive : Config msg -> Archive -> Html msg
renderArchive config archive =
    config.onUpdate
        |> ME.andMap (requestRemoveArchive config.value archive |> Just)
        |> renderFile archive


renderNew : Config msg -> File -> Html msg
renderNew config file =
    config.onUpdate
        |> ME.andMap (requestRemoveFile config.value file |> Just)
        |> renderFile (file2Archive file)


renderRmConfirm : Config msg -> Html msg
renderRmConfirm config =
    case config.onUpdate of
        Nothing ->
            text ""

        Just onUpdt ->
            let
                fvalue =
                    config.value

                body =
                    span
                        []
                        [ text "Deseja remover o arquivo \""
                        , span
                            [ style "font-weight" "bold" ]
                            [ text (getRmFilename fvalue) ]
                        , text "\"?"
                        ]
            in
            Modal.confirm
                [ Modal.Opened (isOpened fvalue)
                , Modal.OnOk (onUpdt (applyRemove fvalue))
                , Modal.OnClose (onUpdt { fvalue | toRemove = RmNone })
                , Modal.Body body
                ]


viewArchLink : Archive -> Html msg
viewArchLink archive =
    a
        [ href archive.url
        , target "_blank"
        ]
        [ FA.icon (getMimeIcon archive.mimetype)
        , text " "
        , text (getNiceFileDsc archive)
        ]



-------------------------------------------
--              MAIN RENDER              --
-------------------------------------------


view : List (Option msg) -> Html msg
view options =
    let
        config =
            List.foldr
                applyOption
                default
                options

        onChangeOpt =
            case config.onUpdate of
                Just onUp ->
                    let
                        oldFValue =
                            config.value

                        onSelect files =
                            onUp { oldFValue | new = oldFValue.new ++ files }
                    in
                    on "change" (JD.map onSelect filesDecoder)

                Nothing ->
                    class ""

        filteredCurrent =
            config.value.current
                |> List.filter
                    (\a -> List.any (\b -> b.id == a.id) config.value.removed |> not)

        renderCurrent =
            List.map
                (renderArchive config)
                filteredCurrent

        renderNews =
            List.map
                (renderNew config)
                config.value.new

        renderInsert =
            div
                [ class "custom-file"
                , style "width" "100%"
                ]
                [ input
                    [ type_ "file"
                    , id ("file-" ++ config.name)
                    , class "custom-file-input"
                    , multiple True
                    , onChangeOpt
                    , placeholder config.placeholder
                    ]
                    []
                , label
                    [ class "custom-file-label"
                    , style "position" "absolute"
                    , for ("file-" ++ config.name)
                    ]
                    [ text "Arquivo..." ]
                ]

        inp =
            div
                [ style "display" "flex"
                , style "flex-direction" "column"
                ]
                (renderCurrent
                    ++ renderNews
                    ++ [ renderInsert ]
                    ++ [ renderRmConfirm config ]
                )

        iWOpts =
            [ IW.Input inp
            ]
                |> H.appIfJust (Maybe.map IW.Label config.label)
    in
    IW.view iWOpts

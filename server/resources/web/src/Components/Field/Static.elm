module Components.Field.Static exposing (view)

import Html exposing (Html, div, input, label, span, text)
import Html.Attributes exposing (checked, class, disabled, for, id, name, style, type_, value)


view : Html msg -> List (Html msg) -> Html msg
view label content =
    div
        [ class "form-group row" ]
        [ div
            [ class "col-sm-3"
            , class "col-xs-12"
            ]
            [ label ]
        , div
            [ class "col-sm-9"
            , class "col-xs-12"
            , style "margin-left" "-2px"
            ]
            content
        ]

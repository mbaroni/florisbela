module Components.Field.Typeahead exposing
    ( ItemViewer
    , Model
    , Msg
    , Option(..)
    , ResGetter
    , UpdateConfig
    , emptyItemView
    , initial
    , update
    , view
    )

import Api
import Browser.Dom as BDom
import Components.Button as Btn
import Components.Field.InputWrapper as IW
import Components.Types exposing (..)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, div, input, option, select, span, text)
import Html.Attributes exposing (class, id, placeholder, selected, style, type_, value)
import Html.Events exposing (onBlur, onClick, onFocus, onInput, onMouseDown, onMouseOut, onMouseOver, onMouseUp, stopPropagationOn)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Maybe
import Maybe.Extra as ME
import Process
import Random
import Random.Char as RC
import Random.String as RS
import Regex
import Result exposing (..)
import String.Normalize exposing (removeDiacritics)
import Task
import Time


defaultRefetchMiliseconds : Float
defaultRefetchMiliseconds =
    1000


perPage : Int
perPage =
    10


type alias ResGetter =
    List ( String, String )
    -> Api.ReqConfig


type alias ItemViewer msg =
    ( String, Maybe String, Maybe (Html msg) )


emptyItemView : ItemViewer msg
emptyItemView =
    ( "--", Nothing, Nothing )


fst3 : ( a, b, c ) -> a
fst3 ( x, y, z ) =
    x


snd3 : ( a, b, c ) -> b
snd3 ( x, y, z ) =
    y


canonStr : String -> String
canonStr str =
    String.toLower str |> removeDiacritics


canonContains : String -> String -> Bool
canonContains token str =
    String.contains (canonStr token) (canonStr str)



--------------------------------------------------------------------------------
--                     MODEL
--------------------------------------------------------------------------------


type alias Model a =
    { token : String
    , data : Maybe (List a)
    , ftoken : Maybe String
    , fetching : Bool
    , fetchLocked : Bool
    , focused : Bool
    , over : Bool
    , inputId : String
    }


initial : ( Model a, Cmd (Msg a) )
initial =
    let
        cmdSetId =
            Random.generate
                SetId
                (RS.string 20 RC.latin)
    in
    ( { token = ""
      , data = Nothing
      , ftoken = Nothing
      , fetching = False
      , fetchLocked = False
      , focused = False
      , over = False
      , inputId = ""
      }
    , cmdSetId
    )


type Msg a
    = SetId String
    | Focus
    | Blur
    | MouseOver
    | MouseOut
    | Clear
    | Text String
    | Fetched String (Result Http.Error (List a))
    | UnlockFetch
    | Select a
    | NoOp


type alias UpdateConfig a =
    { model : Model a
    , decoder : JD.Decoder a
    , preFetch : Bool
    , resGetter : ResGetter
    }



------------------------------------
--             UPDATE             --
------------------------------------


update : UpdateConfig a -> Msg a -> ( Model a, Maybe (Maybe a), Cmd (Msg a) )
update config msg =
    let
        model =
            config.model

        cmdUnlock =
            H.sendAfter
                defaultRefetchMiliseconds
                UnlockFetch

        cmdFetch text =
            let
                expect =
                    Http.expectJson
                        (Fetched text)
                        (JD.field "rows" (JD.list config.decoder))
            in
            config.resGetter
                [ ( "search", text )
                , ( "page", String.fromInt 1 )
                , ( "perPage", String.fromInt perPage )
                ]
                |> Api.execRequest expect
    in
    case msg of
        SetId newId ->
            ( { model
                | inputId = newId
              }
            , Nothing
            , Cmd.none
            )

        Focus ->
            let
                ( fetching, fetchLocked, cmd ) =
                    if config.preFetch && not model.fetching && model.fetchLocked then
                        ( True, True, Cmd.batch [ cmdFetch model.token, cmdUnlock ] )

                    else
                        ( model.fetching, model.fetchLocked, Cmd.none )
            in
            ( { model
                | focused = True
                , token = model.token
                , fetching = fetching
                , fetchLocked = fetchLocked
              }
            , Nothing
            , cmd
            )

        Blur ->
            ( { model
                | focused = model.over
              }
            , Nothing
            , Cmd.none
            )

        MouseOver ->
            ( { model
                | over = True
              }
            , Nothing
            , Cmd.none
            )

        MouseOut ->
            ( { model
                | over = False
              }
            , Nothing
            , Cmd.none
            )

        Clear ->
            let
                cmdFocusInput =
                    Task.attempt
                        (\_ -> NoOp)
                        (BDom.focus model.inputId)
            in
            ( model
            , Just Nothing
            , Cmd.none
            )

        Text text ->
            let
                ( fetching, fetchLocked, cmd ) =
                    if not model.fetching && not model.fetchLocked then
                        ( True, True, Cmd.batch [ cmdFetch text, cmdUnlock ] )

                    else
                        ( model.fetching, model.fetchLocked, Cmd.none )
            in
            ( { model
                | token = text
                , fetching = fetching
                , fetchLocked = fetchLocked
              }
            , Nothing
            , cmd
            )

        Fetched ftoken res ->
            let
                newData =
                    case res of
                        Ok vals ->
                            Just vals

                        Err err ->
                            model.data

                ( fetching, fetchLocked, cmd ) =
                    if model.focused && model.token /= ftoken && not model.fetchLocked then
                        ( True, True, Cmd.batch [ cmdFetch model.token, cmdUnlock ] )

                    else
                        ( False, model.fetchLocked, Cmd.none )
            in
            ( { model
                | ftoken = Just ftoken
                , data = newData
                , fetching = fetching
                , fetchLocked = fetchLocked
              }
            , Nothing
            , cmd
            )

        UnlockFetch ->
            let
                ( fetching, fetchLocked, cmd ) =
                    case model.ftoken of
                        Just ftoken ->
                            if model.focused && model.token /= ftoken && not model.fetching then
                                ( True, True, Cmd.batch [ cmdFetch model.token, cmdUnlock ] )

                            else
                                ( model.fetching, False, Cmd.none )

                        Nothing ->
                            ( model.fetching, False, Cmd.none )
            in
            ( { model
                | fetching = fetching
                , fetchLocked = fetchLocked
              }
            , Nothing
            , cmd
            )

        Select a ->
            ( { model
                | focused = False
                , token = ""
                , ftoken = Nothing
              }
            , Just (Just a)
            , Cmd.none
            )

        NoOp ->
            ( model
            , Nothing
            , Cmd.none
            )



--------------------------------------------------------------------------------
--                     OPTION
--------------------------------------------------------------------------------


type alias Config a msg =
    { label : Maybe (Html msg)
    , name : String
    , value : Maybe a
    , viewer : a -> ItemViewer msg
    , decoder : Maybe (JD.Decoder a)
    , feedback : String
    , required : Bool
    , preFetch : Bool
    }


default : Config a msg
default =
    { label = Nothing
    , name = ""
    , value = Nothing
    , viewer = always emptyItemView
    , decoder = Nothing
    , feedback = ""
    , required = False
    , preFetch = False
    }


type Option a msg
    = Label (Html msg)
    | Name String
    | Value (Maybe a)
    | Viewer (a -> ItemViewer msg)
    | Decoder (JD.Decoder a)
    | Feedback String
    | Required Bool
    | PreFetch Bool


applyOption : Option a msg -> Config a msg -> Config a msg
applyOption option config =
    case option of
        Label label ->
            { config | label = Just label }

        Name name ->
            { config | name = name }

        Value value ->
            { config | value = value }

        Viewer viewer ->
            { config | viewer = viewer }

        Decoder dec ->
            { config | decoder = Just dec }

        Feedback feedback ->
            { config | feedback = feedback }

        Required required ->
            { config | required = required }

        PreFetch preFetch ->
            { config | preFetch = preFetch }



--------------------------------------------------------------------------------
--                     VIEW
--------------------------------------------------------------------------------


colorifySearch : String -> String -> Html msg
colorifySearch search title =
    let
        specClr s =
            span [ style "color" "color" ] s

        tokenizes str =
            String.words str
                |> List.map (\s -> ( s, removeDiacritics s |> String.toLower ))

        titles =
            tokenizes title

        searches =
            tokenizes search

        reg tk =
            Regex.fromString tk
                |> Maybe.withDefault Regex.never

        matches tk s =
            Regex.find (reg tk) s |> List.head

        drop n str =
            String.right (String.length str - n) str

        colorWord token wd =
            case matches token wd of
                Just { index } ->
                    span
                        []
                        [ text (String.left index wd)
                        , drop index wd
                            |> String.left (String.length token)
                            |> (\s -> span [ style "color" "#2054b4" ] [ text s ])
                        , drop (index + String.length token) wd
                            |> text
                        ]

                Nothing ->
                    text wd
    in
    text ""


filteredItens : String -> (a -> ItemViewer msg) -> List a -> List a
filteredItens token viewer itens =
    let
        acceptIt item =
            let
                ( title, mSubtitle, thumb ) =
                    viewer item
            in
            canonContains token title
                || (mSubtitle
                        |> Maybe.map (\s -> canonContains token s)
                        |> Maybe.withDefault False
                   )
    in
    List.filter acceptIt itens


viewDropItem : (Msg a -> msg) -> (a -> ItemViewer msg) -> a -> Html msg
viewDropItem msgMapper viewer item =
    let
        ( title, subTitle, thumbImg ) =
            viewer item

        renderThumbImg =
            case thumbImg of
                Just thumb ->
                    div
                        [ class "typeahead-drop-thumb" ]
                        [ div
                            [ class "typeahead-drop-thumb-inner" ]
                            [ thumb ]
                        ]

                Nothing ->
                    text ""

        renderSubTitle =
            case subTitle of
                Just stxt ->
                    span
                        [ class "typeahead-drop-subtitle" ]
                        [ text stxt ]

                Nothing ->
                    text ""

        renderTitle =
            span
                [ class "typeahead-drop-title" ]
                [ text title ]
    in
    div
        [ class "typeahead-drop-item"
        , onClick (msgMapper (Select item))
        ]
        [ renderThumbImg
        , div
            [ class "typeahead-drop-desc" ]
            [ renderTitle
            , renderSubTitle
            ]
        ]


view : Model a -> (Msg a -> msg) -> List (Option a msg) -> Html msg
view model msgMapper options =
    let
        config =
            List.foldr
                applyOption
                default
                options

        value_ =
            if model.focused then
                model.token

            else
                case config.value of
                    Just val ->
                        config.viewer val |> fst3

                    Nothing ->
                        ""

        handleFocus =
            if ME.isNothing config.value then
                Focus

            else
                NoOp

        interestData =
            model.data
                |> Maybe.map
                    (filteredItens model.token config.viewer)

        renderItems =
            case interestData of
                Just ds ->
                    List.map
                        (viewDropItem msgMapper config.viewer)
                        ds

                Nothing ->
                    []

        hasData =
            case interestData of
                Just ds ->
                    List.length ds > 0

                Nothing ->
                    False

        drop =
            if model.focused && hasData then
                div
                    [ class "typeahead-drop-wrapper"
                    , onMouseUp (msgMapper MouseOut)
                    , onMouseDown (msgMapper MouseOver)
                    ]
                    renderItems

            else
                text ""

        emptyDrop =
            if model.focused && not hasData && ME.isJust model.ftoken then
                div
                    [ class "typeahead-drop-wrapper" ]
                    [ div
                        [ class "typeahead-drop-empty" ]
                        [ text "Nenhum encontrado"
                        ]
                    ]

            else
                text ""

        spinner =
            let
                className =
                    if model.fetching then
                        "typeahead-input-spinner-loading"

                    else
                        ""
            in
            span
                [ class "typeahead-input-spinner"
                , class className
                ]
                [ FA.iconWithOptions
                    FA.spinner
                    FA.Solid
                    [ FA.Animation FA.Pulse ]
                    []
                ]

        textInput =
            if ME.isJust config.value && not model.focused then
                div
                    []
                    [ span
                        [ class "form-control"
                        , class "input-large"
                        , class "typeahead-text"
                        , style "background-color" "#f7f7f7"
                        , style "width" "210px"
                        ]
                        [ text value_ ]
                    ]

            else
                div
                    []
                    [ input
                        [ class "form-control"
                        , style "width" "245px"
                        , class "typeahead-text"
                        , type_ "text"
                        , value value_
                        , id model.inputId
                        , placeholder "Escolher..."
                        , onInput (msgMapper << Text)
                        , onBlur (msgMapper Blur)
                        , onClick (msgMapper handleFocus)
                        , onFocus (msgMapper handleFocus)
                        ]
                        []
                    , drop
                    , emptyDrop
                    , spinner
                    ]

        renderClearBtn =
            Btn.view
                [ Btn.Icon FA.times
                , Btn.Class "btn-input-append"
                , Btn.Typ Danger
                , Btn.OnClick (msgMapper Clear)
                ]

        appendOpts =
            if ME.isJust config.value && not model.focused then
                [ IW.Append renderClearBtn ]

            else
                []

        opts =
            [ IW.Input textInput
            , IW.Feedback config.feedback
            , IW.Required config.required
            ]
                ++ appendOpts
                |> H.appIfJust (Maybe.map IW.Label config.label)
    in
    IW.view opts

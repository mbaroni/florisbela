module Components.Field.Radio exposing
    ( Option(..)
    , view
    )

import Components.Field.InputWrapper as IW
import Components.Form.Validator as V
import Dict exposing (Dict)
import Helpers as Help
import Html exposing (Html, div, input, label, span, text)
import Html.Attributes exposing (checked, class, disabled, for, id, name, style, type_, value)
import Html.Events exposing (onInput)
import Maybe.Extra as ME


type alias Config msg =
    { label : Maybe (Html msg)
    , name : String
    , value : String
    , onInput : Maybe (String -> msg)
    , feedback : String
    , options : List ( String, Html msg, Bool )
    , validators : List V.Validator
    , required : Bool
    , errorsDict : Dict String (List String)
    }


default : Config msg
default =
    { label = Nothing
    , name = ""
    , value = ""
    , onInput = Nothing
    , feedback = ""
    , options = []
    , validators = []
    , required = False
    , errorsDict = Dict.empty
    }


type Option msg
    = Label (Html msg)
    | Name String
    | Value String
    | OnInput (String -> msg)
    | Feedback String
    | Options (List ( String, Html msg, Bool ))
    | Validators (List V.Validator)
    | ErrorsDict (Dict String (List String))
    | Required Bool


applyOption : Option msg -> Config msg -> Config msg
applyOption opt config =
    case opt of
        Label lbl ->
            { config | label = Just lbl }

        Name name ->
            { config | name = name }

        Value val ->
            { config | value = val }

        OnInput onInput ->
            { config | onInput = Just onInput }

        Feedback feedback ->
            { config | feedback = feedback }

        Options options ->
            { config | options = config.options ++ options }

        Validators valids ->
            { config | validators = config.validators ++ valids }

        Required required ->
            { config | required = required }

        ErrorsDict errorsDict ->
            { config | errorsDict = errorsDict }


renderInput : Config msg -> ( String, Html msg, Bool ) -> Html msg
renderInput config ( val, lbl, enabled ) =
    let
        isChecked =
            config.value == val

        classDisabled =
            if enabled then
                class ""

            else
                class "disabled"
    in
    div
        [ class "custom-radio" ]
        [ label
            [ class "custom-radio-label"
            , classDisabled
            , for val
            ]
            [ lbl
            , input
                ([ type_ "radio"
                 , class "custom-control-input"
                 , name val
                 , id val
                 , disabled (not enabled)
                 , value val
                 , checked isChecked
                 ]
                    |> Help.appIfJust (Maybe.map onInput config.onInput)
                )
                []
            , span
                [ class "custom-radio-check2"
                , classDisabled
                , class "checkmark"
                ]
                []
            ]
        ]


view : List (Option msg) -> Html msg
view opts =
    let
        config =
            List.foldl
                applyOption
                default
                opts

        inp =
            div
                [ class "custom-radio-box" ]
                (List.map (renderInput config) config.options)

        errors =
            V.getStrErrors config.validators config.value

        error2 =
            if config.name /= "" then
                Dict.get
                    config.name
                    config.errorsDict
                    |> Maybe.map List.head
                    |> ME.join

            else
                Nothing

        iWOpts =
            [ IW.Input inp
            , IW.Required config.required
            ]
                |> Help.appIfJust (Maybe.map IW.Label config.label)
                |> Help.appIfJust (Maybe.map IW.Error (List.head errors))
                |> Help.appIfJust (Maybe.map IW.Error error2)
    in
    IW.view iWOpts

module Components.Field.Text exposing
    ( AppOption(..)
    , Option(..)
    , TextConfig
    , TextMask
    , Type(..)
    , applyOption
    , buildConfig
    , default
    , emptyMask
    , numberMask
    , view
    )

import Components.Button as Btn
import Components.Field.InputWrapper as IW
import Components.Form.Validator as V
import Components.Types exposing (..)
import Dict exposing (Dict)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, div, input, span, text)
import Html.Attributes exposing (autocomplete, class, placeholder, readonly, type_, value)
import Html.Events exposing (onInput)
import Json.Encode as JE
import Maybe exposing (withDefault)
import Maybe.Extra as ME


pulsingIcon : Html msg
pulsingIcon =
    FA.iconWithOptions
        FA.spinner
        FA.Solid
        [ FA.Animation FA.Pulse ]
        []



---------------------------------------------------
--                 TEXT APPEND                   --
---------------------------------------------------


type alias AppConfig msg =
    { content : Html msg
    , icon : Maybe FA.Icon
    , iconStyle : FA.Style
    , typ : Maybe ContextType
    , isPre : Bool
    , loading : Bool
    , onClick : Maybe msg
    }


appDefault : AppConfig msg
appDefault =
    { content = text ""
    , icon = Nothing
    , iconStyle = FA.Solid
    , typ = Nothing
    , isPre = False
    , loading = False
    , onClick = Nothing
    }


type AppOption msg
    = AppContent (Html msg)
    | AppIcon FA.Icon
    | AppIconStyle FA.Style
    | AppTyp ContextType
    | AppIsPre Bool
    | AppLoading Bool
    | AppOnClick msg


applyAppOption : AppOption msg -> AppConfig msg -> AppConfig msg
applyAppOption option conf =
    case option of
        AppContent content ->
            { conf | content = content }

        AppIcon ic ->
            { conf | icon = Just ic }

        AppIconStyle style ->
            { conf | iconStyle = style }

        AppTyp typ ->
            { conf | typ = Just typ }

        AppIsPre isPre ->
            { conf | isPre = isPre }

        AppLoading loading ->
            { conf | loading = loading }

        AppOnClick onClick ->
            { conf | onClick = Just onClick }


viewApp : List (AppOption msg) -> Html msg
viewApp options =
    if List.isEmpty options then
        text ""

    else
        let
            conf =
                List.foldr
                    applyAppOption
                    appDefault
                    options

            outterClas =
                if conf.isPre then
                    "input-group-prepend"

                else
                    "input-group-append"

            icDsc =
                if conf.loading then
                    pulsingIcon

                else
                    case conf.icon of
                        Just ic ->
                            FA.iconWithOptions ic conf.iconStyle [] []

                        Nothing ->
                            text ""

            iconOpt =
                conf.icon
                    |> Maybe.map Btn.Icon
                    |> Maybe.withDefault (Btn.Class "")

            typeOpt =
                conf.typ
                    |> Maybe.map Btn.Typ
                    |> Maybe.withDefault (Btn.Class "")

            onClickOpt =
                conf.onClick
                    |> Maybe.map Btn.OnClick
                    |> Maybe.withDefault (Btn.Class "")

            btnClass =
                if conf.isPre then
                    "btn-input-prepend"

                else
                    "btn-input-append"

            btnVersion =
                div
                    [ class outterClas ]
                    [ Btn.view
                        [ Btn.Label conf.content
                        , Btn.Class btnClass
                        , Btn.Loading conf.loading
                        , Btn.IconStyle conf.iconStyle
                        , iconOpt
                        , typeOpt
                        , onClickOpt
                        ]
                    ]

            typClass =
                case conf.typ of
                    Just typ ->
                        class ("input-group-append-" ++ ctxType2Str typ)

                    Nothing ->
                        class ""

            spanVersion =
                div
                    [ class outterClas ]
                    [ span
                        [ class "input-group-append-text"
                        , typClass
                        ]
                        [ conf.content
                        , icDsc
                        ]
                    ]
        in
        if List.isEmpty options then
            text ""

        else if ME.isJust conf.onClick then
            btnVersion

        else
            spanVersion



---------------------------------------------------
--                    TEXT                       --
---------------------------------------------------
-- Verificar elm-diff para implementar futuras mascaras avancadas


type alias TextMask =
    ( String -> String, String -> String )


type Type
    = Text
    | Color
    | Tel
    | Time
    | Url
    | Email
    | Number
    | Password
    | Range
    | Date


typ2Str : Type -> String
typ2Str typ =
    case typ of
        Text ->
            "text"

        Color ->
            "color"

        Tel ->
            "tel"

        Time ->
            "time"

        Url ->
            "url"

        Email ->
            "email"

        Number ->
            "number"

        Password ->
            "password"

        Range ->
            "range"

        Date ->
            "date"



-- | Search
-- | DatetimeLocal
-- | File
-- | Hidden
-- | Image
-- | Month
-- | Radio
-- | Reset
-- | Submit
-- | Week
-- | Button
-- | Checkbox


emptyMask : TextMask
emptyMask =
    ( identity, identity )


numberMask : TextMask
numberMask =
    ( identity, String.filter Char.isDigit )


type alias TextConfig msg =
    { label : Maybe (Html msg)
    , name : String
    , value : String
    , onInput : Maybe (String -> msg)
    , placeholder : Maybe String
    , size_ : Maybe Size
    , ctxType : ContextType
    , feedback : String
    , error : Maybe String
    , formErrors : Dict String (List String)
    , append : List (AppOption msg)
    , preAppend : List (AppOption msg)
    , mask : TextMask
    , validators : List V.Validator
    , typ : Type
    , autoc : Bool
    , required : Bool
    , readOnly : Bool
    }


default : TextConfig msg
default =
    { label = Nothing
    , name = ""
    , value = ""
    , onInput = Nothing
    , placeholder = Nothing
    , size_ = Nothing
    , ctxType = Default
    , feedback = ""
    , error = Nothing
    , formErrors = Dict.empty
    , append = []
    , preAppend = []
    , mask = emptyMask
    , validators = []
    , typ = Text
    , autoc = False
    , required = False
    , readOnly = False
    }



---------------------------------------------------
--               OPTIONS                         --
---------------------------------------------------


type Option msg
    = Label (Html msg)
    | Name String
    | Value String
    | OnInput (String -> msg)
    | Placeholder String
    | Size Size
    | ContextType ContextType
    | Feedback String
    | Error (Maybe String)
    | FormErrors (Dict String (List String))
    | PreAppend (List (AppOption msg))
    | Append (List (AppOption msg))
    | Mask TextMask
    | Validators (List V.Validator)
    | Required Bool
    | Type Type
    | Autocomplete Bool
    | ReadOnly Bool
    | NoOpt


applyOption : Option msg -> TextConfig msg -> TextConfig msg
applyOption option config =
    case option of
        Label label ->
            { config | label = Just label }

        Name name ->
            { config | name = name }

        Value value ->
            { config | value = value }

        OnInput onInput ->
            { config | onInput = Just onInput }

        Placeholder placeholder ->
            { config | placeholder = Just placeholder }

        Size size_ ->
            { config | size_ = Just size_ }

        ContextType ctxType ->
            { config | ctxType = ctxType }

        Feedback feedback ->
            { config | feedback = feedback }

        Error error ->
            { config | error = error }

        FormErrors formErrors ->
            { config | formErrors = formErrors }

        Append [] ->
            config

        Append appendOpts ->
            { config | append = appendOpts ++ [ AppIsPre False ] }

        PreAppend [] ->
            config

        PreAppend appendOpts ->
            { config | preAppend = appendOpts ++ [ AppIsPre True ] }

        Mask mask ->
            { config | mask = mask }

        Validators valids ->
            { config | validators = config.validators ++ valids }

        Required required ->
            { config | required = required }

        Type typ ->
            { config | typ = typ }

        Autocomplete autoc ->
            { config | autoc = autoc }

        ReadOnly readOnly ->
            { config | readOnly = readOnly }

        NoOpt ->
            config


buildConfig : List (Option msg) -> TextConfig msg
buildConfig options =
    List.foldl applyOption default options


view : List (Option msg) -> Html msg
view options =
    let
        config =
            buildConfig options

        ctxTypeClass =
            "input-" ++ ctxType2Str config.ctxType

        sizeClass =
            case config.size_ of
                Just s ->
                    "input-" ++ size2Str s

                Nothing ->
                    ""

        onInput_ =
            case config.onInput of
                Just onInp ->
                    onInput (Tuple.second config.mask >> onInp)

                Nothing ->
                    class ""

        preAppend =
            viewApp config.preAppend

        append =
            viewApp config.append

        typeStr =
            typ2Str config.typ

        renderInput =
            input
                [ onInput_
                , readonly config.readOnly
                , class "form-control"
                , class sizeClass
                , class ctxTypeClass
                , type_ typeStr
                , autocomplete config.autoc
                , value (config.value |> Tuple.first config.mask)
                , config.placeholder
                    |> withDefault ""
                    |> placeholder
                ]
                []

        errorsFromForm =
            Dict.get config.name config.formErrors
                |> Maybe.withDefault []

        errors =
            V.getStrErrors config.validators config.value
                ++ ME.toList config.error
                ++ errorsFromForm

        opts =
            [ IW.Input renderInput
            , IW.PreAppend preAppend
            , IW.Append append
            , IW.Feedback config.feedback
            , IW.Required config.required
            ]
                |> H.appIfJust (Maybe.map IW.Label config.label)
                |> H.appIfJust (Maybe.map IW.Error (List.head errorsFromForm))
    in
    IW.view opts

module Components.Field.DatePicker exposing
    ( Model
    , Msg
    , Option(..)
    , initial
    , initialFromCalendarDate
    , view
    )

import Components.Field.DatePicker2 as DP
import Components.Field.InputWrapper as IW
import Components.Form.Validator as V
import Date as DT exposing (Date, numberToMonth)
import Helpers as H
import Html exposing (Html, div, text)
import Maybe.Extra as ME
import Time as T exposing (Month(..), Weekday(..))



---------------------------------------------------
--       FORMATTERS
---------------------------------------------------


dateFormatter : Date -> String
dateFormatter date =
    DT.format "dd/MM/yyyy" date


dayFormatter : Weekday -> String
dayFormatter wd =
    case wd of
        Mon ->
            "Seg"

        Tue ->
            "Ter"

        Wed ->
            "Qua"

        Thu ->
            "Qui"

        Fri ->
            "Sex"

        Sat ->
            "Sáb"

        Sun ->
            "Dom"


monthFormatter : Month -> String
monthFormatter month =
    case month of
        Jan ->
            "Janeiro"

        Feb ->
            "Fevereiro"

        Mar ->
            "Março"

        Apr ->
            "Abril"

        May ->
            "Maio"

        Jun ->
            "Junho"

        Jul ->
            "Julho"

        Aug ->
            "Agosto"

        Sep ->
            "Setembro"

        Oct ->
            "Outubro"

        Nov ->
            "Novembro"

        Dec ->
            "Dezembro"



---------------------------------------------------
--       MODEL
---------------------------------------------------


type alias Model =
    DP.DatePicker


type alias Msg =
    DP.Msg


initial : Model
initial =
    DP.initFromDate (DT.fromCalendarDate 2019 Jul 1)


initialFromCalendarDate : Int -> Int -> Int -> Model
initialFromCalendarDate year mon day =
    DP.initFromDate (DT.fromCalendarDate year (numberToMonth mon) day)



---------------------------------------------------
--       UPDATE
---------------------------------------------------


type Option msg
    = Label (Html msg)
    | Name String
    | Value (Maybe Date)
    | Feedback String
    | Validators (List V.Validator)
    | Required Bool
    | Updater (( Maybe Date, Model ) -> msg)
    | Model Model


type alias Config msg =
    { label : Maybe (Html msg)
    , name : String
    , value : Maybe Date
    , feedback : String
    , validators : List V.Validator
    , required : Bool
    , model : DP.DatePicker
    , updater : Maybe (( Maybe Date, Model ) -> msg)
    }


default : Config msg
default =
    { label = Nothing
    , name = ""
    , value = Nothing
    , feedback = ""
    , validators = []
    , required = False
    , model = initial
    , updater = Nothing
    }


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Label label ->
            { config | label = Just label }

        Name name ->
            { config | name = name }

        Value value ->
            { config | value = value }

        Feedback feedback ->
            { config | feedback = feedback }

        Validators valids ->
            { config | validators = config.validators ++ valids }

        Required required ->
            { config | required = required }

        Updater updater ->
            { config | updater = Just updater }

        Model model ->
            { config | model = model }


configToSetting : Config msg -> DP.Settings
configToSetting config =
    let
        defSett =
            DP.defaultSettings

        dateValOrNothing validator =
            case validator of
                V.Date func ->
                    Just func

                _ ->
                    Nothing

        isDisabled2 dt =
            config.validators
                |> List.map dateValOrNothing
                |> ME.values
                |> List.map (\f -> f dt)
                |> List.any ME.isJust
    in
    { defSett
        | inputName = Just config.name
        , isDisabled = isDisabled2
        , parser = myParser
        , placeholder = "Escolher..."
        , dateFormatter = dateFormatter
        , dayFormatter = dayFormatter
        , monthFormatter = monthFormatter
        , inputClassList =
            [ ( "form-control", True )
            , ( "input-medium", True )
            ]
    }


myParser : String -> Result String Date
myParser str =
    let
        ints =
            String.split "/" str
                |> List.map String.toInt
                |> ME.values

        mD =
            List.head ints

        mM =
            List.head (List.drop 1 ints)
                |> Maybe.map DT.numberToMonth

        mY =
            List.head (List.drop 2 ints)

        mDt =
            if String.length str == 10 then
                Maybe.map3
                    DT.fromCalendarDate
                    mY
                    mM
                    mD

            else
                Nothing
    in
    Result.fromMaybe
        "Wrong date format"
        mDt



---------------------------------------------------
--       VIEW
---------------------------------------------------


renderInput : Config msg -> (( Maybe Date, Model ) -> msg) -> Html msg
renderInput config updater =
    let
        settings =
            configToSetting config

        msgMapper msg =
            let
                ( newDatePicker, dEv ) =
                    DP.update
                        settings
                        msg
                        config.model

                newDate =
                    case dEv of
                        DP.Picked date ->
                            Just date

                        DP.Erased ->
                            Nothing

                        _ ->
                            config.value
            in
            updater ( newDate, newDatePicker )
    in
    DP.view
        config.value
        settings
        config.model
        |> Html.map msgMapper


view : List (Option msg) -> Html msg
view options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        input =
            case config.updater of
                Just updater ->
                    renderInput config updater

                Nothing ->
                    text ""

        errors =
            case config.value of
                Just date ->
                    V.getDateErrors config.validators date

                Nothing ->
                    V.getMDateErrors config.validators Nothing

        opts =
            [ IW.Input input
            , IW.Feedback config.feedback
            , IW.Required config.required
            ]
                |> H.appIfJust (Maybe.map IW.Label config.label)
                |> H.appIfJust (Maybe.map IW.Error (List.head errors))
    in
    IW.view opts

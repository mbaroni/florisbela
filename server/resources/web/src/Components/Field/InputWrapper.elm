module Components.Field.InputWrapper exposing
    ( Option(..)
    , view
    )

import Components.Types as T
import Helpers as H
import Html exposing (Attribute, Html, div, label, span, text)
import Html.Attributes exposing (class, for, property, style)


type alias Config msg =
    { label : Maybe (Html msg)
    , input : Html msg
    , ctxType : T.ContextType
    , name : String
    , horizontal : Bool
    , feedback : String
    , required : Bool
    , append : Html msg
    , preAppend : Html msg
    , error : String
    }


default : Config msg
default =
    { label = Nothing
    , input = text ""
    , ctxType = T.Default
    , name = ""
    , horizontal = True
    , feedback = ""
    , required = False
    , append = text ""
    , preAppend = text ""
    , error = ""
    }


type Option msg
    = Label (Html msg)
    | Input (Html msg)
    | ContextType T.ContextType
    | Name String
    | Horizontal
    | Vertical
    | Feedback String
    | Required Bool
    | PreAppend (Html msg)
    | Append (Html msg)
    | Error String


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Label label ->
            { config | label = Just label }

        Input input ->
            { config | input = input }

        ContextType ctxType ->
            { config | ctxType = ctxType }

        Name name ->
            { config | name = name }

        Horizontal ->
            { config | horizontal = True }

        Vertical ->
            { config | horizontal = False }

        Feedback feedback ->
            { config | feedback = feedback }

        Required required ->
            { config | required = required }

        Append append ->
            { config | append = append }

        PreAppend preAppend ->
            { config | preAppend = preAppend }

        Error error ->
            { config | error = error }


viewHorizontal : Config msg -> Html msg
viewHorizontal config =
    let
        inputGroup =
            div
                [ class "input-group" ]
                [ config.preAppend
                , config.input
                , config.append
                ]

        contextStr =
            if config.error == "" then
                T.ctxType2Str config.ctxType

            else
                T.ctxType2Str T.Danger

        feedbackText =
            if config.error == "" then
                config.feedback

            else
                config.error

        feedback =
            if config.feedback /= "" || config.error /= "" then
                div
                    [ class "input-feedback"
                    , class ("input-feedback-" ++ contextStr)
                    ]
                    [ text feedbackText ]

            else
                text ""

        requiredMark =
            if config.required then
                span
                    [ style "color" "#ff0000" ]
                    [ text "*" ]

            else
                text ""

        inputLabel =
            case config.label of
                Just l ->
                    label
                        [ class "col-sm-3 col-xs-12", for config.name ]
                        [ div
                            [ class "form-label" ]
                            [ l, requiredMark ]
                        ]

                Nothing ->
                    text ""

        iputColClass =
            case config.label of
                Just _ ->
                    "col-sm-9 col-xs-12"

                Nothing ->
                    "col-sm-12 col-xs-12"
    in
    div
        [ class "form-group row" ]
        [ inputLabel
        , div
            [ class iputColClass ]
            [ inputGroup
            , feedback
            ]
        ]


viewVertical : Config msg -> Html msg
viewVertical config =
    div
        [ class "form-group" ]
        [ label
            [ class "col-sm-3", for config.name ]
            [ div
                []
                [ Maybe.withDefault (text "") config.label ]
            ]
        , config.input
        ]


view : List (Option msg) -> Html msg
view options =
    let
        config =
            List.foldr applyOption default options
    in
    if config.horizontal then
        viewHorizontal config

    else
        viewVertical config

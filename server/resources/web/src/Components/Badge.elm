module Components.Badge exposing (view)

import Components.Types as T
import Html exposing (Html, span, text)
import Html.Attributes exposing (class, placeholder, type_, value)


view : T.ContextType -> String -> Bool -> Html msg
view ctxt txt notRounded =
    let
        typClass =
            class ("badge-" ++ T.ctxType2Str ctxt)

        formClass =
            if notRounded then
                class ""

            else
                class "badge-rounded"
    in
    span
        [ class "badge"
        , formClass
        , typClass
        ]
        [ text txt ]

module Components.Modal exposing
    ( Option(..)
    , alert
    , confirm
    , view
    )

import Components.Button as Btn
import Components.Types exposing (..)
import FontAwesome as FA
import Helpers as H
import Html exposing (Html, div, h2, h3, h5, span, text)
import Html.Attributes exposing (class, id, style, value)
import Html.Events exposing (on, onClick, targetValue)
import Json.Decode as JD
import Maybe.Extra as ME


type alias Config msg =
    { opened : Bool
    , onClose : Maybe msg
    , header : Maybe (Html msg)
    , body : Maybe (Html msg)
    , footer : Maybe (Html msg)
    , onOk : Maybe msg
    , onCancel : Maybe msg
    }


default : Config msg
default =
    { opened = False
    , onClose = Nothing
    , header = Nothing
    , body = Nothing
    , footer = Nothing
    , onOk = Nothing
    , onCancel = Nothing
    }


type Option msg
    = Opened Bool
    | OnClose msg
    | Header (Html msg)
    | Body (Html msg)
    | Footer (Html msg)
    | OnOk msg
    | OnCancel msg


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Opened opened ->
            { config | opened = opened }

        OnClose onClose ->
            { config | onClose = Just onClose }

        Header header ->
            { config | header = Just header }

        Body body ->
            { config | body = Just body }

        Footer footer ->
            { config | footer = Just footer }

        OnOk onOk ->
            { config | onOk = Just onOk }

        OnCancel onCancel ->
            { config | onCancel = Just onCancel }


{-| MODAL
-}
view : List (Option msg) -> Html msg
view options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        ( display, subClass ) =
            if config.opened then
                ( "flex", "modal-opened" )

            else
                ( "none", "modal-closed" )

        closeOpt =
            case config.onClose of
                Just onClose ->
                    onClick onClose

                Nothing ->
                    class ""

        customCloseOpt =
            case config.onClose of
                Just onClose ->
                    let
                        valueDecoder v =
                            if v == "modal-value" then
                                JD.succeed onClose

                            else
                                JD.fail "Wrong target."
                    in
                    on "click" (JD.andThen valueDecoder targetValue)

                Nothing ->
                    class ""

        renderHeader =
            config.header
                |> Maybe.map (\h -> div [ class "modal-header" ] [ h ])
                |> Maybe.withDefault (text "")

        renderBody =
            config.body
                |> Maybe.map (\b -> div [ class "modal-body" ] [ b ])
                |> Maybe.withDefault (text "")

        renderFooter =
            config.footer
                |> Maybe.map (\f -> div [ class "modal-footer" ] [ f ])
                |> Maybe.withDefault (text "")

        closeBtn =
            span
                [ class "modal-close", closeOpt ]
                [ FA.icon FA.times ]
    in
    div
        [ class "modal"
        , style "display" display
        , class subClass
        , value "modal-value"
        , customCloseOpt
        ]
        [ div
            [ class "modal-content" ]
            [ closeBtn
            , renderHeader
            , renderBody
            , renderFooter
            ]
        ]


{-| ALERT
-}
alert : List (Option msg) -> Html msg
alert options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        onCloseOpt =
            config.onClose
                |> ME.or config.onOk
                |> Maybe.map OnClose
                |> ME.toList

        headerOpt =
            config.header
                |> Maybe.map Header
                |> ME.toList

        bodyOpt =
            config.body
                |> Maybe.map Body
                |> ME.toList

        btnOnClickOpt =
            config.onClose
                |> ME.or config.onOk
                |> Maybe.map Btn.OnClick
                |> ME.toList

        footerOpt =
            let
                wtSpc =
                    "\u{00A0}\u{00A0}\u{00A0}\u{00A0}"
            in
            div
                []
                [ Btn.view
                    ([ Btn.Label (wtSpc ++ "Ok" ++ wtSpc |> text)
                     , Btn.Typ Primary
                     ]
                        ++ btnOnClickOpt
                    )
                ]
                |> Footer
                |> List.singleton

        openedOpt =
            [ Opened config.opened ]

        modalOpts =
            onCloseOpt
                ++ headerOpt
                ++ bodyOpt
                ++ footerOpt
                ++ openedOpt
    in
    view modalOpts


{-| ALERT
-}
confirm : List (Option msg) -> Html msg
confirm options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        onCloseOpt =
            config.onClose
                |> ME.or config.onCancel
                |> Maybe.map OnClose
                |> ME.toList

        headerOpt =
            config.header
                |> Maybe.map Header
                |> ME.toList

        bodyOpt =
            config.body
                |> Maybe.map Body
                |> ME.toList

        btnCancelOnClickOpt =
            config.onCancel
                |> ME.or config.onClose
                |> Maybe.map Btn.OnClick
                |> Maybe.withDefault Btn.NoOpt

        btnOkOnClickOpt =
            config.onOk
                |> Maybe.map Btn.OnClick
                |> Maybe.withDefault Btn.NoOpt

        footerOpt =
            let
                wtSpc =
                    "\u{00A0}\u{00A0}\u{00A0}\u{00A0}"
            in
            div
                []
                [ Btn.view
                    (btnOkOnClickOpt
                        :: [ Btn.Label (wtSpc ++ "Ok" ++ wtSpc |> text)
                           , Btn.Typ Success
                           , Btn.Icon FA.check
                           ]
                    )
                , Btn.view
                    (btnCancelOnClickOpt
                        :: [ Btn.Label (text "Cancelar")
                           , Btn.Typ Danger
                           , Btn.Icon FA.times
                           ]
                    )
                ]
                |> Footer
                |> List.singleton

        openedOpt =
            [ Opened config.opened ]

        modalOpts =
            onCloseOpt
                ++ headerOpt
                ++ bodyOpt
                ++ footerOpt
                ++ openedOpt
    in
    view modalOpts



--view modalOpts

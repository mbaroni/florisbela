module Components.Table exposing
    ( Alignment(..)
    , CellRenderer
    , Model
    , Msg
    , Option(..)
    , initial
    , raw
    , update
    , view
    )

import Api exposing (ParamsRequester)
import Components.Button as Btn
import Components.Field.Text as Txt
import Components.Synopsis as Syn
import Components.Types exposing (..)
import Dict exposing (Dict)
import FontAwesome as FA
import Helpers as H exposing (onEnter, send, sendAfter)
import Html exposing (Html, a, div, input, li, option, select, span, table, tbody, td, text, th, thead, tr, ul)
import Html.Attributes exposing (class, placeholder, style, value)
import Html.Events exposing (onClick, onInput)
import Http
import Json.Decode as JD
import Maybe.Extra as ME
import Result
import Tuple



---------------------------------------------------
--                 CONSTANTS                     --
---------------------------------------------------


perFetch : Int
perFetch =
    20


perPageOpts : List Int
perPageOpts =
    [ 5, 10 ]



---------------------------------------------------
--                   TYPES                       --
---------------------------------------------------


type Sort
    = Asc
    | Desc
    | None


type Alignment
    = Center
    | Right
    | Left


type alias RespData a =
    { elements : List a
    , count : Int
    }


type DataBlock a
    = Elements (List a)
    | BlockError


type alias FetchedData a =
    { elements : Dict Int (DataBlock a)
    , stoken : String
    , total : Int
    }


type alias Model a =
    { fetchedData : Maybe (FetchedData a)
    , token : String
    , fetching : Bool
    , page : Int
    , perPage : Int
    }


type alias CellRenderer a msg =
    { label : Html msg
    , content : a -> Html msg
    , aligment : Alignment
    }


type alias TableConfig a msg =
    { model : Model a
    , data : Maybe (List a)
    , rowRenderer : List (CellRenderer a msg)
    , synRenderer : Maybe (a -> List (Syn.Option msg))
    , searchFunc : String -> a -> Bool
    , perPage : Maybe Int
    , msgMapper : Maybe (Msg a -> msg)
    , onRowClick : Maybe (a -> msg)
    , decoder : JD.Decoder a
    , requester : Maybe ParamsRequester
    }


type Option a msg
    = TableModel (Model a)
    | Data (List a)
    | RowRenderer (List (CellRenderer a msg))
    | SynRenderer (a -> List (Syn.Option msg))
    | SearchFunc (String -> a -> Bool)
    | PerPage Int
    | MsgMapper (Msg a -> msg)
    | OnRowClick (a -> msg)
    | Decoder (JD.Decoder a)
    | Requester ParamsRequester


type Msg a
    = UpdateToken String
    | Search
    | ChangePage Int
    | ChangePerPage Int
    | Fetched ( String, Int ) (Result Http.Error (RespData a))



---------------------------------------------------
--                  HELPERS                      --
---------------------------------------------------


decodeRespData : JD.Decoder a -> JD.Decoder (RespData a)
decodeRespData decoder =
    JD.map2
        RespData
        (JD.field "rows" (JD.list decoder))
        (JD.field "count" JD.int)


default : TableConfig a msg
default =
    { model = Tuple.first (initial False)
    , data = Nothing
    , rowRenderer = []
    , synRenderer = Nothing
    , searchFunc = \s a -> True
    , perPage = Nothing
    , msgMapper = Nothing
    , onRowClick = Nothing
    , decoder = JD.fail "--"
    , requester = Nothing
    }


applyOption : Option a msg -> TableConfig a msg -> TableConfig a msg
applyOption option config =
    case option of
        TableModel model ->
            { config | model = model }

        Data data ->
            { config | data = Just data }

        RowRenderer rowRenderer ->
            { config | rowRenderer = rowRenderer }

        SynRenderer synRenderer ->
            { config | synRenderer = Just synRenderer }

        SearchFunc searchFunc ->
            { config | searchFunc = searchFunc }

        PerPage perPage ->
            { config | perPage = Just perPage }

        MsgMapper msgMapper ->
            { config | msgMapper = Just msgMapper }

        OnRowClick onRowClick ->
            { config | onRowClick = Just onRowClick }

        Decoder decoder ->
            { config | decoder = decoder }

        Requester requester ->
            { config | requester = Just requester }



---------------------------------------------------
--                   UPDATE                      --
---------------------------------------------------


initial : Bool -> ( Model a, Cmd (Msg a) )
initial fetch =
    let
        cmd =
            if fetch then
                send Search

            else
                Cmd.none
    in
    ( { fetchedData = Nothing
      , token = ""
      , fetching = False
      , page = 0
      , perPage = 10
      }
    , cmd
    )


requestBlock : TableConfig a msg -> Int -> Maybe (Cmd (Msg a))
requestBlock config block =
    case config.requester of
        Just requester ->
            let
                expect =
                    Http.expectJson
                        (Fetched ( config.model.token, block ))
                        (decodeRespData config.decoder)
            in
            requester
                [ ( "search", config.model.token )
                , ( "page", String.fromInt block )
                , ( "perPage", String.fromInt perFetch )
                ]
                |> Api.execRequest expect
                |> Just

        Nothing ->
            Nothing


cmdChangePage : Model a -> TableConfig a msg -> Int -> Cmd (Msg a)
cmdChangePage model config newPage =
    let
        blockIdx =
            (newPage * config.model.perPage) // perFetch

        fetchBlock =
            requestBlock config blockIdx
                |> Maybe.withDefault Cmd.none
    in
    case model.fetchedData of
        Just fData ->
            if ME.isJust (Dict.get blockIdx fData.elements) then
                Cmd.none

            else
                fetchBlock

        Nothing ->
            fetchBlock


updateFetchData : ( String, Int ) -> Result Http.Error (RespData a) -> Maybe (FetchedData a) -> Maybe (FetchedData a)
updateFetchData ( stoken, blockIdx ) response mFData =
    let
        newDataBlock =
            response
                |> Result.map (\r -> Elements r.elements)
                |> Result.withDefault BlockError

        newElems =
            mFData
                |> Maybe.map (\f -> f.elements)
                |> Maybe.withDefault Dict.empty
                |> Dict.insert blockIdx newDataBlock
    in
    case response of
        Ok respData ->
            Just
                { elements = newElems
                , stoken = stoken
                , total = respData.count
                }

        Err _ ->
            mFData


update : Msg a -> List (Option a msg) -> ( Model a, Cmd (Msg a) )
update msg options =
    let
        model =
            config.model

        config =
            List.foldl
                applyOption
                default
                options
    in
    case msg of
        UpdateToken token ->
            ( { model | token = token }, Cmd.none )

        ChangePage page ->
            ( { model | page = page }, cmdChangePage model config page )

        ChangePerPage perPage ->
            ( { model | perPage = perPage }, Cmd.none )

        Search ->
            let
                ( newFetchinhg, cmd ) =
                    case requestBlock config 0 of
                        Just fcmd ->
                            ( True, fcmd )

                        Nothing ->
                            ( model.fetching, Cmd.none )
            in
            ( { model | fetching = newFetchinhg, page = 0 }, cmd )

        Fetched ( ftoken, blockIdx ) response ->
            ( { model
                | fetching = False
                , fetchedData =
                    updateFetchData
                        ( ftoken, blockIdx )
                        response
                        model.fetchedData
              }
            , Cmd.none
            )



---------------------------------------------------------
--                        VIEW                         --
---------------------------------------------------------


{-| Per page select
-}
viewPerPageSelect : TableConfig a msg -> Html msg
viewPerPageSelect config =
    let
        renderOpt n =
            option
                [ value (String.fromInt n) ]
                [ text (String.fromInt n) ]

        opts =
            List.map
                renderOpt
                perPageOpts

        fromInt s =
            String.toInt s
                |> Maybe.withDefault 10

        onInputAttr =
            case config.msgMapper of
                Nothing ->
                    class ""

                Just msgMapper ->
                    onInput (fromInt >> ChangePerPage >> msgMapper)
    in
    div
        []
        [ text "Exibir "
        , select
            [ style "margin" "3px 0"
            , style "background-color" "#ffffff"
            , value (String.fromInt config.model.perPage)
            , onInputAttr
            ]
            opts
        , text " registros"
        ]


{-| Descrição de elementos exibidos
-}
viewExibition : TableConfig a msg -> Html msg
viewExibition config =
    let
        data2 =
            getDisplayedData config

        tot =
            case config.data of
                Just ds ->
                    List.length ds

                Nothing ->
                    case config.model.fetchedData of
                        Just fData ->
                            fData.total

                        Nothing ->
                            0
    in
    span
        []
        [ text "Exibindo "
        , span
            [ style "font-weight" "bold" ]
            [ String.fromInt (List.length data2) |> text ]
        , text " de "
        , span
            [ style "font-weight" "bold" ]
            [ String.fromInt tot |> text ]
        , text " registros"
        ]


{-| Cabeçalho (search input, etc)
-}
viewHeader : TableConfig a msg -> Html msg
viewHeader config =
    let
        onEnterAttr =
            case config.msgMapper of
                Nothing ->
                    class ""

                Just msgMapper ->
                    onEnter (msgMapper Search)

        onInputAttr =
            case config.msgMapper of
                Nothing ->
                    []

                Just msgMapper ->
                    [ Txt.OnInput (UpdateToken >> msgMapper) ]

        onClickAttr =
            case config.msgMapper of
                Nothing ->
                    []

                Just msgMapper ->
                    [ Txt.AppOnClick (msgMapper Search) ]

        tokenInput2 =
            [ Txt.Value config.model.token
            , [ Txt.AppIcon FA.search
              , Txt.AppTyp Info
              , Txt.AppLoading config.model.fetching
              ]
                ++ onClickAttr
                |> Txt.PreAppend
            ]
                ++ onInputAttr
                |> Txt.view

        tokenInput =
            div [ class "form-group" ]
                [ div
                    [ class "input-group"
                    , style "justify-content" "end"
                    ]
                    [ input
                        [ class "form-control"
                        , value config.model.token
                        ]
                        []
                    , div
                        [ class "input-group-append" ]
                        [ Btn.view
                            [ Btn.Icon FA.search
                            , Btn.Typ Info
                            , Btn.Style "margin" "0"
                            , Btn.Style "border-top-left-radius" "0"
                            , Btn.Style "border-bottom-left-radius" "0"
                            , Btn.Loading config.model.fetching
                            ]
                        ]
                    ]
                ]

        tokenInput3 =
            span
                [ class ""
                ]
                [ text "123124123" ]
    in
    div
        [ class "row"
        , class "tbl-header"
        , style "align-items" "baseline"
        ]
        [ div
            [ class "col-sm-12"
            , class "col-md-9"
            ]
            [ viewExibition config ]
        , div
            [ class "col-sm-12"
            , class "col-md-3"
            , onEnterAttr
            ]
            [ tokenInput
            ]
        ]


{-| Rodapé
-}
viewFooter : TableConfig a msg -> Html msg
viewFooter config =
    let
        perPage =
            Maybe.withDefault
                config.model.perPage
                config.perPage

        dataLen =
            config.data
                |> Maybe.map List.length
                |> Maybe.withDefault

        nPages =
            case config.model.fetchedData of
                Just fData ->
                    toFloat fData.total
                        / toFloat perPage
                        |> ceiling

                Nothing ->
                    case config.data of
                        Just data ->
                            toFloat (List.length data)
                                / toFloat perPage
                                |> ceiling

                        Nothing ->
                            0

        pagination =
            case config.msgMapper of
                Nothing ->
                    text ""

                Just msgMapper ->
                    viewPagination
                        nPages
                        config.model.page
                        (ChangePage >> msgMapper)
    in
    div
        [ class "row", class "tbl-footer" ]
        [ div
            [ class "col-sm-12"
            , class "col-md-5"
            ]
            [ viewPerPageSelect config ]
        , div
            [ class "col-sm-12"
            , class "col-md-7"
            ]
            [ pagination ]
        ]


{-| Elementos a serem exibidos
-}
getDisplayedData : TableConfig a msg -> List a
getDisplayedData config =
    case config.data of
        Just data ->
            data
                |> List.drop (config.model.perPage * config.model.page)
                |> List.take config.model.perPage

        Nothing ->
            case config.model.fetchedData of
                Just fData ->
                    let
                        cPage =
                            config.model.page

                        pPage =
                            config.model.perPage

                        dictIdx =
                            (cPage * pPage) // perFetch

                        dropOffset =
                            (cPage * pPage) - (dictIdx * perFetch)
                    in
                    case Dict.get dictIdx fData.elements of
                        Just respData ->
                            case respData of
                                Elements elems ->
                                    List.drop dropOffset elems
                                        |> List.take pPage

                                BlockError ->
                                    []

                        Nothing ->
                            []

                Nothing ->
                    []


{-| Corpo da tabela
-}
viewBody : TableConfig a msg -> Html msg
viewBody config =
    let
        data2 =
            getDisplayedData config

        renderRow d =
            List.map .content config.rowRenderer
                |> List.map (\c -> c d)

        head =
            List.map .label config.rowRenderer
                |> List.map List.singleton
                |> List.map (th [ class "tbl-th" ])
                |> tr [ class "tbl-tr-h" ]
                |> List.singleton
                |> thead []

        trAttrs x =
            case config.onRowClick of
                Nothing ->
                    [ class "tbl-tr-b" ]

                Just onCl ->
                    [ class "tbl-tr-b", onClick (onCl x), style "cursor" "pointer" ]

        viewBdRow x =
            renderRow x
                |> List.map List.singleton
                |> List.map (td [ class "tbl-td" ])
                |> tr (trAttrs x)

        body =
            data2
                |> List.map viewBdRow
                |> tbody []

        tableBody =
            table
                [ class "tbl" ]
                [ head
                , body
                ]

        synBody synRend =
            data2
                |> List.map synRend
                |> List.map Syn.view
                |> div [ style "margin-bottom" "1rem" ]

        dscBody =
            case config.synRenderer of
                Just synRend ->
                    synBody synRend

                Nothing ->
                    tableBody
    in
    div
        [ class "row" ]
        [ div
            [ class "col-sm-12"
            ]
            [ dscBody
            ]
        ]



---------------------------------------------------------
--                  VIEW PRINCIPAL                     --
---------------------------------------------------------


view : List (Option a msg) -> Html msg
view options =
    let
        config =
            List.foldl
                applyOption
                default
                options
    in
    div
        [ class "container-fluid"
        , style "padding" "0"
        , style "width" "100%"
        ]
        [ viewHeader config
        , viewBody config
        , viewFooter config
        ]



---------------------------------------------------------
--                     RAW TABLE                       --
---------------------------------------------------------


type alias RawConfig msg =
    { head : List (Html msg)
    , body : List (List (Html msg))
    }


raw : RawConfig msg -> Html msg
raw config =
    let
        head =
            config.head
                |> List.map List.singleton
                |> List.map (th [ class "tbl-th" ])
                |> tr [ class "tbl-tr-h" ]
                |> List.singleton
                |> thead []

        viewBdRow row =
            row
                |> List.map List.singleton
                |> List.map (td [ class "tbl-td" ])
                |> tr [ class "tbl-tr-b" ]

        body =
            config.body
                |> List.map viewBdRow
                |> tbody []
    in
    table
        [ class "tbl" ]
        [ head, body ]



---------------------------------------------------------
--                     PAGINATION                      --
---------------------------------------------------------


assemblePages : Int -> Int -> List (Maybe Int)
assemblePages np active =
    if np < 8 then
        List.range 0 (np - 1)
            |> List.map Just

    else
        let
            gap =
                np - active
        in
        if active < 3 then
            [ Just 0, Just 1, Just 2, Just 3, Nothing, Just (np - 2), Just (np - 1) ]

        else if active == 3 then
            [ Just 0, Just 1, Just 2, Just 3, Just 4, Nothing, Just (np - 1) ]

        else if gap < 3 then
            [ Just 0, Just 1, Nothing, Just (np - 4), Just (np - 2), Just (np - 1) ]

        else if gap == 3 then
            [ Just 0, Nothing, Just (np - 4), Just (np - 3), Just (np - 2), Just (np - 1) ]

        else
            [ Just 0, Nothing, Just (active - 1), Just active, Just (active + 1), Nothing, Just (np - 1) ]


viewPagination : Int -> Int -> (Int -> msg) -> Html msg
viewPagination nPages page onSelect =
    let
        viewItem mPag =
            case mPag of
                Just p ->
                    if p == page then
                        li
                            [ class "tbl-pagination-item"
                            , class "tbl-pagination-item-active"
                            ]
                            [ span
                                []
                                [ String.fromInt (p + 1) |> text ]
                            ]

                    else
                        li
                            [ onClick (onSelect p)
                            , class "tbl-pagination-item"
                            , class "tbl-pagination-item-default"
                            ]
                            [ span
                                []
                                [ String.fromInt (p + 1) |> text ]
                            ]

                Nothing ->
                    li
                        [ class "tbl-pagination-item"
                        , class "tbl-pagination-item-dots"
                        ]
                        [ span
                            []
                            [ text "..." ]
                        ]
    in
    assemblePages nPages page
        |> List.map viewItem
        |> ul [ class "tbl-pagination-ul" ]

module Components.Form.File exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.File as Fil
import Components.Form.Helpers as Help
import Components.Form.Types as T
import Components.Form.Validator as V
import File exposing (File)
import Helpers as H
import Html exposing (Html)
import Http
import Json.Encode as JE
import Maybe.Extra as ME


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    case Help.getFieldName (T.File opts) of
        Just name ->
            opts
                |> List.map (Help.getFilesValue value)
                |> ME.values
                |> List.head
                |> Maybe.map (List.map (Http.filePart (name ++ "[]")))
                |> Maybe.map ((++) parts)
                |> Maybe.withDefault parts

        Nothing ->
            parts


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    obj


type alias Config a =
    { label : Maybe (Html T.Msg)
    , name : String
    , feedback : String
    , getter : a -> Fil.FValue
    , setter : a -> Fil.FValue -> a
    , value : Fil.FValue
    , placeholder : Maybe String
    }


default : Config a
default =
    { label = Nothing
    , name = ""
    , feedback = ""
    , getter = always Fil.emptyFValue
    , setter = \v fs -> v
    , value = Fil.emptyFValue
    , placeholder = Nothing
    }


applyOption : T.FieldOption a -> Config a -> Config a
applyOption option config =
    case option of
        T.Label label ->
            { config | label = Just label }

        T.Name name ->
            { config | name = name }

        T.Feedback feedback ->
            { config | feedback = feedback }

        T.Placeholder placeholder ->
            { config | placeholder = Just placeholder }

        T.FilGetter getter ->
            { config | getter = getter }

        T.FilSetter setter ->
            { config | setter = setter }

        _ ->
            config


update : T.Config a -> String -> List File -> a
update config name newFiles =
    name
        |> Help.getFieldByName config.fields
        |> Maybe.map Help.getFieldOpts
        |> Maybe.map (List.foldl applyOption default)
        |> Maybe.map (\c -> c.setter config.value newFiles)
        |> Maybe.withDefault config.value


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view fconfig vals options =
    let
        config =
            List.foldl
                applyOption
                default
                options

        nameOpt =
            Fil.Name config.name

        feedOpt =
            Fil.Feedback config.feedback

        labelOpt =
            config.label
                |> Maybe.map Fil.Label
                |> Maybe.withDefault Fil.NoOpt

        pholderOpt =
            config.placeholder
                |> Maybe.map Fil.Placeholder
                |> Maybe.withDefault Fil.NoOpt

        valueOpt =
            Fil.Value (config.getter fconfig.value)

        filOpts =
            [ nameOpt
            , feedOpt
            , labelOpt
            , pholderOpt
            , valueOpt
            ]
    in
    Fil.view filOpts

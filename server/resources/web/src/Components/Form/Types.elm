module Components.Form.Types exposing
    ( CheckOp(..)
    , Config
    , Field(..)
    , FieldOption(..)
    , Model
    , Msg(..)
    )

import Api
import Components.Field.DatePicker as Dat
import Components.Field.Text as Txt
import Components.Field.Typeahead as Tah
import Components.Form.Validator as V
import Components.Types exposing (..)
import Date exposing (Date)
import Dict exposing (Dict)
import File as F
import Html exposing (Html)
import Json.Encode as JE



-----------------------------------------------------
---           FIELD OPTIONS                       ---
-----------------------------------------------------


type FieldOption a
    = Label (Html Msg)
    | Name String
    | Placeholder String
    | Feedback String
    | Size Size
    | ContextType ContextType
    | TxtAppend (List (Txt.AppOption Msg))
    | TxtPreAppend (List (Txt.AppOption Msg))
    | TxtGetter (a -> String)
    | TxtSetter (a -> String -> a)
    | TxtType Txt.Type
    | Mask Txt.TextMask
    | Validators (List V.Validator)
    | SelOptions (List ( String, String ))
    | SelSetter (a -> String -> a)
    | SelGetter (a -> String)
    | SelNoEmpty
    | ChkOptions (List (List (CheckOp a)))
    | RadGetter (a -> String)
    | RadSetter (a -> String -> a)
    | RadOptions (List ( String, Html Msg, Bool ))
    | DatGetter (a -> Maybe Date)
    | DatSetter (a -> Maybe Date -> a)
    | TahGetter (a -> Maybe JE.Value)
    | TahSetter (a -> Maybe JE.Value -> a)
    | TahFetcher (List ( String, String ) -> Api.ReqConfig)
    | TahViewer (JE.Value -> Tah.ItemViewer Msg)
    | TahPreFetch Bool
    | FilGetter (a -> List F.File)
    | FilSetter (a -> List F.File -> a)


type CheckOp a
    = ChkName String
    | ChkLabel (Html Msg)
    | ChkGetter (a -> Bool)
    | ChkSetter (a -> Bool -> a)
    | ChkEnabled Bool


type Field a
    = Text (List (FieldOption a))
    | Select (List (FieldOption a))
    | Checkbox (List (FieldOption a))
    | Radio (List (FieldOption a))
    | DatePicker (List (FieldOption a))
    | Typeahead (List (FieldOption a))


type alias Model =
    { datModels : Dict String Dat.Model
    , tahModels : Dict String (Tah.Model JE.Value)
    }


type Msg
    = TxtUpdate String String
    | SelUpdate String String
    | ChkUpdate String Bool
    | RadUpdate String String
    | DatUpdate String ( Maybe Date, Dat.Model )
    | TahUpdate String (Tah.Msg JE.Value)
    | NoOp


type alias Config a =
    { fields : List (Field a)
    , model : Model
    , value : a
    , showErrors : Bool
    }

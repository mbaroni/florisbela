module Components.Form.Text exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.Text as Txt
import Components.Form.Helpers as Help
import Components.Form.Types as T
import Components.Form.Validator as V
import Helpers as H
import Html exposing (Html)
import Http
import Json.Encode as JE
import Maybe.Extra as ME
import Tuple


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    case Help.getFieldName (T.Text opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> Maybe.map (Http.stringPart name)
                |> Maybe.map (\x -> x :: parts)
                |> Maybe.withDefault parts

        Nothing ->
            parts


mapToTxtOption : T.FieldOption a -> Maybe (Txt.Option T.Msg)
mapToTxtOption option =
    case option of
        T.Label lbl ->
            Txt.Label lbl |> Just

        T.Name name ->
            Txt.Name name |> Just

        T.Placeholder str ->
            Txt.Placeholder str |> Just

        T.Size sz ->
            Txt.Size sz |> Just

        T.ContextType cntx ->
            Txt.ContextType cntx |> Just

        T.Feedback str ->
            Txt.Feedback str |> Just

        T.TxtAppend opts ->
            Txt.Append opts |> Just

        T.TxtPreAppend opts ->
            Txt.PreAppend opts |> Just

        T.Mask mask ->
            Txt.Mask mask |> Just

        T.TxtType typ ->
            Txt.Type typ |> Just

        _ ->
            Nothing


type alias TxtConfig a =
    { options : List (Txt.Option T.Msg)
    , getter : a -> String
    , setter : a -> String -> a
    }


txtDefault : TxtConfig a
txtDefault =
    { options = []
    , getter = always ""
    , setter = always
    }


applyTxtOption : T.FieldOption a -> TxtConfig a -> TxtConfig a
applyTxtOption fieldOpt txtConfig =
    let
        newOptions =
            case mapToTxtOption fieldOpt of
                Just opt ->
                    [ opt ]

                Nothing ->
                    []

        newSetter =
            case fieldOpt of
                T.TxtSetter setter ->
                    setter

                _ ->
                    txtConfig.setter

        newGetter =
            case fieldOpt of
                T.TxtGetter getter ->
                    getter

                _ ->
                    txtConfig.getter
    in
    { options = txtConfig.options ++ newOptions
    , setter = newSetter
    , getter = newGetter
    }


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    case Help.getFieldName (T.Text opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> H.nuller JE.string
                |> Tuple.pair name
                |> (\x -> x :: obj)

        Nothing ->
            obj


update : T.Config a -> String -> String -> a
update config name newText =
    name
        |> Help.getFieldByName config.fields
        |> Maybe.map Help.getFieldOpts
        |> Maybe.map (List.foldl applyTxtOption txtDefault)
        |> Maybe.map (\c -> c.setter config.value newText)
        |> Maybe.withDefault config.value


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view config validators options =
    let
        txtValue =
            txtConfig.getter config.value

        validOpt =
            if config.showErrors then
                Txt.Validators validators

            else
                Txt.Validators []

        required =
            V.getStrErrors validators ""
                |> List.isEmpty
                |> not

        txtConfig =
            List.foldl
                applyTxtOption
                txtDefault
                options

        valueOpt =
            Txt.Value txtValue

        onInputOpt =
            case Help.getFieldName (T.Text options) of
                Just name ->
                    Txt.OnInput (T.TxtUpdate name)

                Nothing ->
                    Txt.OnInput (\s -> T.NoOp)

        -- onInputOpt2 =
        --     Txt.OnInput (txtConfig.setter config.value >> config.onChange config.model)
        txtOpts =
            txtConfig.options
                ++ [ valueOpt ]
                ++ [ onInputOpt ]
                ++ [ validOpt ]
                ++ [ Txt.Required required ]
    in
    Txt.view txtOpts

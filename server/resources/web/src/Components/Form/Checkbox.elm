module Components.Form.Checkbox exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.Checkbox as Chk
import Components.Form.Helpers as Help
import Components.Form.Types as T
import Components.Form.Validator as V
import Helpers as H
import Html exposing (Html, text)
import Http
import Json.Encode as JE
import Maybe.Extra as ME


type alias SubchkConfig a =
    { name : String
    , label : Html T.Msg
    , getter : a -> Bool
    , setter : a -> Bool -> a
    , enabled : Bool
    }


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    List.foldl
        (appendChkValue value)
        obj
        opts


bool2Str : Bool -> String
bool2Str val =
    if val then
        "true"

    else
        "false"


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    List.foldl
        (subAppendChkPart value)
        parts
        opts


subAppendChkPart : a -> T.FieldOption a -> List Http.Part -> List Http.Part
subAppendChkPart value option parts =
    let
        subSubAppendChkPart copts os =
            case getChkName copts of
                Just name ->
                    case getChkGetter copts of
                        Just getter ->
                            (getter value |> bool2Str |> Http.stringPart name) :: os

                        Nothing ->
                            os

                Nothing ->
                    os
    in
    case option of
        T.ChkOptions copts ->
            List.foldr
                subSubAppendChkPart
                parts
                copts

        _ ->
            parts


getChkGetter : List (T.CheckOp a) -> Maybe (a -> Bool)
getChkGetter options =
    let
        subGetChkGetter opt =
            case opt of
                T.ChkGetter getter ->
                    Just getter

                _ ->
                    Nothing
    in
    options
        |> List.map subGetChkGetter
        |> ME.values
        |> List.head


getChkName : List (T.CheckOp a) -> Maybe String
getChkName options =
    let
        subGetChkName opt =
            case opt of
                T.ChkName name ->
                    Just name

                _ ->
                    Nothing
    in
    options
        |> List.map subGetChkName
        |> ME.values
        |> List.head


appendChkValue : a -> T.FieldOption a -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendChkValue value option obj =
    let
        subAppendChkValue copts os =
            case getChkName copts of
                Just name ->
                    case getChkGetter copts of
                        Just getter ->
                            ( name, getter value |> JE.bool ) :: os

                        Nothing ->
                            os

                Nothing ->
                    os
    in
    case option of
        T.ChkOptions copts ->
            List.foldr
                subAppendChkValue
                obj
                copts

        _ ->
            obj


defaultSubChkConfig : SubchkConfig a
defaultSubChkConfig =
    { name = ""
    , label = text ""
    , getter = always True
    , setter = \a b -> a
    , enabled = True
    }


getSetterByName : String -> List (List (T.CheckOp a)) -> Maybe (a -> Bool -> a)
getSetterByName name optss =
    let
        buildSubChkConfig =
            List.foldl
                applySubChkOption
                defaultSubChkConfig
    in
    optss
        |> List.map buildSubChkConfig
        |> List.filter (\c -> c.name == name)
        |> List.head
        |> Maybe.andThen (\c -> Just c.setter)


update : T.Config a -> String -> Bool -> a
update config name newChk =
    let
        extractChkOpts opt =
            case opt of
                T.ChkOptions opts ->
                    Just opts

                _ ->
                    Nothing
    in
    config.fields
        |> List.map Help.getFieldOpts
        |> List.concat
        |> List.map extractChkOpts
        |> ME.values
        |> List.concat
        |> getSetterByName name
        |> Maybe.andThen (\s -> s config.value newChk |> Just)
        |> Maybe.withDefault config.value


subConfToNativeOpt : T.Config a -> SubchkConfig a -> List (Chk.CheckOption T.Msg)
subConfToNativeOpt config subConfig =
    let
        onCheckOp =
            if subConfig.name /= "" then
                [ Chk.OnCheck (T.ChkUpdate subConfig.name) ]

            else
                []
    in
    [ Chk.Name subConfig.name
    , Chk.Value (subConfig.getter config.value)
    , Chk.OpLabel subConfig.label
    , Chk.Enabled subConfig.enabled
    ]
        ++ onCheckOp


applySubChkOption : T.CheckOp a -> SubchkConfig a -> SubchkConfig a
applySubChkOption option subConfig =
    case option of
        T.ChkName name ->
            { subConfig | name = name }

        T.ChkLabel label ->
            { subConfig | label = label }

        T.ChkGetter getter ->
            { subConfig | getter = getter }

        T.ChkSetter setter ->
            { subConfig | setter = setter }

        T.ChkEnabled enabled ->
            { subConfig | enabled = enabled }


mapToChkOption : T.Config a -> T.FieldOption a -> Maybe (Chk.Option T.Msg)
mapToChkOption config option =
    case option of
        T.Label lbl ->
            Chk.Label lbl |> Just

        T.ChkOptions ops ->
            let
                subOpt2NativeOpt subOpts =
                    List.foldl
                        applySubChkOption
                        defaultSubChkConfig
                        subOpts

                nativeOpts =
                    ops
                        |> List.map subOpt2NativeOpt
                        |> List.map (subConfToNativeOpt config)
            in
            Chk.Options nativeOpts |> Just

        _ ->
            Nothing


view : T.Config a -> List (T.FieldOption a) -> Html T.Msg
view config options =
    options
        |> List.map (mapToChkOption config)
        |> ME.values
        |> Chk.view

module Components.Form.Select exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.Select as Sel
import Components.Form.Helpers as Help
import Components.Form.Types as T
import Components.Form.Validator as V
import Helpers as H
import Html exposing (Html)
import Http
import Json.Encode as JE
import Maybe.Extra as ME


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    case Help.getFieldName (T.Select opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> Maybe.map (Http.stringPart name)
                |> Maybe.map (\x -> x :: parts)
                |> Maybe.withDefault parts

        Nothing ->
            parts


mapToSelOption : T.FieldOption a -> Maybe (Sel.Option T.Msg)
mapToSelOption option =
    case option of
        T.Label lbl ->
            Sel.Label lbl |> Just

        T.Name name ->
            Sel.Name name |> Just

        T.Feedback str ->
            Sel.Feedback str |> Just

        T.SelOptions opts ->
            Sel.Options opts |> Just

        T.SelNoEmpty ->
            Sel.NoEmpty |> Just

        _ ->
            Nothing


type alias SelConfig a =
    { options : List (Sel.Option T.Msg)
    , getter : a -> String
    , setter : a -> String -> a
    }


selDefault : SelConfig a
selDefault =
    { options = []
    , getter = always ""
    , setter = always
    }


applySelOption : T.FieldOption a -> SelConfig a -> SelConfig a
applySelOption fieldOpt selConfig =
    let
        options =
            case mapToSelOption fieldOpt of
                Just opt ->
                    selConfig.options ++ [ opt ]

                Nothing ->
                    selConfig.options

        setter =
            case fieldOpt of
                T.SelSetter sett ->
                    sett

                _ ->
                    selConfig.setter

        getter =
            case fieldOpt of
                T.SelGetter gett ->
                    gett

                _ ->
                    selConfig.getter
    in
    { options = options
    , setter = setter
    , getter = getter
    }


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    case Help.getFieldName (T.Select opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> H.nuller JE.string
                |> Tuple.pair name
                |> (\x -> x :: obj)

        Nothing ->
            obj


update : T.Config a -> String -> String -> a
update config name newSelValue =
    name
        |> Help.getFieldByName config.fields
        |> Maybe.map Help.getFieldOpts
        |> Maybe.map (List.foldl applySelOption selDefault)
        |> Maybe.map (\c -> c.setter config.value newSelValue)
        |> Maybe.withDefault config.value


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view config validators options =
    let
        selConfig =
            List.foldl
                applySelOption
                selDefault
                options

        valueOpt =
            [ Sel.Value (selConfig.getter config.value) ]

        onInputOpt =
            case Help.getFieldName (T.Select options) of
                Just name ->
                    [ Sel.OnInput (T.SelUpdate name) ]

                Nothing ->
                    []

        validOpt =
            if config.showErrors then
                [ Sel.Validators validators ]

            else
                []

        required =
            V.getStrErrors validators ""
                |> List.isEmpty
                |> not

        selOpts =
            selConfig.options
                ++ valueOpt
                ++ onInputOpt
                ++ validOpt
                ++ [ Sel.Required required ]
    in
    Sel.view selOpts

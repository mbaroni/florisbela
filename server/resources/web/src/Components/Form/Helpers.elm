module Components.Form.Helpers exposing
    ( buildValidatorDict
    , extractValidator
    , getDatValue
    , getFieldByName
    , getFieldErrors
    , getFieldName
    , getFieldOpts
    , getFilesValue
    , getStrValue
    , getValidatorList
    )

import Components.Form.Types exposing (..)
import Components.Form.Validator as V
import Date exposing (Date)
import Dict exposing (Dict)
import File as F
import Maybe.Extra as ME


getFieldName : Field a -> Maybe String
getFieldName field =
    let
        getName opt =
            case opt of
                Name name ->
                    Just name

                _ ->
                    Nothing
    in
    field
        |> getFieldOpts
        |> List.map getName
        |> ME.values
        |> List.head


getFieldOpts : Field a -> List (FieldOption a)
getFieldOpts field =
    case field of
        Text opts ->
            opts

        Select opts ->
            opts

        Checkbox opts ->
            opts

        Radio opts ->
            opts

        DatePicker opts ->
            opts

        Typeahead opts ->
            opts


extractValidator : FieldOption a -> Maybe (List V.Validator)
extractValidator opt =
    case opt of
        Validators vals ->
            Just vals

        _ ->
            Nothing


getValidatorList : Field a -> List V.Validator
getValidatorList field =
    getFieldOpts field
        |> List.map extractValidator
        |> ME.values
        |> List.concat


getStrValue : a -> FieldOption a -> Maybe String
getStrValue value option =
    case option of
        TxtGetter getter ->
            getter value |> Just

        SelGetter getter ->
            getter value |> Just

        RadGetter getter ->
            getter value |> Just

        _ ->
            Nothing


getDatValue : a -> FieldOption a -> Maybe Date
getDatValue value option =
    case option of
        DatGetter getter ->
            getter value

        _ ->
            Nothing


getFilesValue : a -> FieldOption a -> Maybe (List F.File)
getFilesValue value option =
    case option of
        FilGetter getter ->
            getter value |> Just

        _ ->
            Nothing


getStrErrors : a -> List V.Validator -> List (FieldOption a) -> List String
getStrErrors value validators options =
    let
        strValue =
            options
                |> List.map (getStrValue value)
                |> ME.values
                |> List.head
    in
    case strValue of
        Just val ->
            V.getStrErrors validators val

        Nothing ->
            []


getFieldErrors : a -> Field a -> List String
getFieldErrors value field =
    case getFieldName field of
        Just name ->
            case field of
                Text opts ->
                    getStrErrors
                        value
                        (getValidatorList field)
                        opts

                Select opts ->
                    getStrErrors
                        value
                        (getValidatorList field)
                        opts

                Radio opts ->
                    getStrErrors
                        value
                        (getValidatorList field)
                        opts

                DatePicker opts ->
                    []

                Checkbox opts ->
                    []

                Typeahead opts ->
                    []

        Nothing ->
            []



-- getFieldName : Field a msg -> Maybe String


getFieldByName : List (Field a) -> String -> Maybe (Field a)
getFieldByName fields name =
    let
        subGetFieldByName field =
            case getFieldName field of
                Just name_ ->
                    if name == name_ then
                        Just field

                    else
                        Nothing

                Nothing ->
                    Nothing
    in
    fields
        |> List.map subGetFieldByName
        |> ME.values
        |> List.head


buildValidatorDict : List (Field a) -> Dict String (List V.Validator)
buildValidatorDict fields =
    let
        fieldValidatorPair field =
            Maybe.andThen
                (\name -> Just ( name, getValidatorList field ))
                (getFieldName field)
    in
    fields
        |> List.map fieldValidatorPair
        |> ME.values
        |> Dict.fromList

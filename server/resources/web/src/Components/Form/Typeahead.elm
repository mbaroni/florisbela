module Components.Form.Typeahead exposing
    ( appendJsonValue
    , update
    , view
    )

import Api
import Components.Field.Typeahead as Tah
import Components.Form.Helpers as Help exposing (..)
import Components.Form.Types as T
import Components.Form.Validator as V
import Dict exposing (Dict)
import Helpers as H
import Html exposing (Html, text)
import Http
import Json.Decode as JD
import Json.Encode as JE
import Maybe.Extra as ME


type alias TahConfig a =
    { getter : a -> Maybe JE.Value
    , setter : a -> Maybe JE.Value -> a
    , viewer : JE.Value -> Tah.ItemViewer T.Msg
    , fetcher : Maybe (List ( String, String ) -> Api.ReqConfig)
    , preFetch : Bool
    , options : List (Tah.Option JE.Value T.Msg)
    }


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    let
        tahConfig =
            List.foldl
                applyTahOption
                tahDefault
                opts

        newField =
            case Help.getFieldName (T.Typeahead opts) of
                Just name ->
                    [ ( name, Maybe.withDefault JE.null (tahConfig.getter value) ) ]

                Nothing ->
                    []
    in
    obj ++ newField


tahDefault : TahConfig a
tahDefault =
    { getter = always Nothing
    , setter = \a v -> a
    , viewer = always Tah.emptyItemView
    , fetcher = Nothing
    , preFetch = False
    , options = []
    }


applyTahOption : T.FieldOption a -> TahConfig a -> TahConfig a
applyTahOption fieldOpt tahConfig =
    case fieldOpt of
        T.Label label ->
            { tahConfig | options = Tah.Label label :: tahConfig.options }

        T.Name name ->
            { tahConfig | options = Tah.Name name :: tahConfig.options }

        T.Feedback feedback ->
            { tahConfig | options = Tah.Feedback feedback :: tahConfig.options }

        T.TahPreFetch preFetch ->
            { tahConfig | options = Tah.PreFetch preFetch :: tahConfig.options }

        T.TahGetter getter ->
            { tahConfig | getter = getter }

        T.TahSetter setter ->
            { tahConfig | setter = setter }

        T.TahViewer viewer ->
            { tahConfig | viewer = viewer }

        T.TahFetcher fetcher ->
            { tahConfig | fetcher = Just fetcher }

        _ ->
            tahConfig


update : T.Config a -> String -> Tah.Msg JE.Value -> ( Tah.Model JE.Value, a, Cmd (Tah.Msg JE.Value) )
update fConfig name msg =
    let
        mOpts =
            name
                |> Help.getFieldByName fConfig.fields
                |> Maybe.map Help.getFieldOpts

        tahConfig =
            mOpts
                |> Maybe.map (List.foldl applyTahOption tahDefault)
                |> Maybe.withDefault tahDefault

        extractFetcher opt =
            case opt of
                T.TahFetcher fetcher ->
                    Just fetcher

                _ ->
                    Nothing

        mResGetter =
            mOpts
                |> Maybe.map (List.map extractFetcher)
                |> Maybe.map ME.values
                |> Maybe.map List.head
                |> ME.join

        preFetch =
            True

        ( tahModel, cmdInit ) =
            Dict.get name fConfig.model.tahModels
                |> Maybe.map (\tM -> ( tM, Cmd.none ))
                |> Maybe.withDefault Tah.initial

        updateConfig resGetter =
            { model = tahModel
            , decoder = JD.value
            , preFetch = preFetch
            , resGetter = resGetter
            }

        ( newModel, mNewVal, cmdUpdate ) =
            mResGetter
                |> Maybe.map updateConfig
                |> Maybe.map (\uc -> Tah.update uc msg)
                |> Maybe.withDefault ( tahModel, Nothing, Cmd.none )

        newValue =
            mNewVal
                |> Maybe.withDefault (tahConfig.getter fConfig.value)
                |> (\v -> tahConfig.setter fConfig.value v)
    in
    ( newModel, newValue, Cmd.batch [ cmdInit, cmdUpdate ] )


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view config validators options =
    let
        tahConfig =
            List.foldl
                applyTahOption
                tahDefault
                options

        fname =
            getFieldName (T.Typeahead options)

        tahModel =
            case fname of
                Just name ->
                    Dict.get name config.model.tahModels
                        |> Maybe.withDefault (Tuple.first Tah.initial)

                Nothing ->
                    Tuple.first Tah.initial

        value =
            tahConfig.getter config.value

        valueOpt =
            Tah.Value value

        viewerOpt =
            Tah.Viewer tahConfig.viewer

        decoderOpt =
            Tah.Decoder JD.value

        tahOpts =
            tahConfig.options
                ++ [ viewerOpt ]
                ++ [ valueOpt ]
                ++ [ decoderOpt ]
    in
    case fname of
        Just name ->
            let
                msgMapper =
                    T.TahUpdate name
            in
            Tah.view
                tahModel
                msgMapper
                tahOpts

        Nothing ->
            text "--"

module Components.Form.Validator exposing
    ( ValValid
    , Validator(..)
    , getDateErrors
    , getErrors
    , getFloatErrors
    , getIntErrors
    , getMDateErrors
    , getStrErrors
    , isAfter
    , isBefore
    , isDateRequired
    , isEmail
    , isUsefulDay
    , maxFloat
    , maxInt
    , maxLength
    , minFloat
    , minInt
    , minLength
    , notEmpty
    , validCPF
    )

import Date as D exposing (Date)
import Helpers as H
import Maybe.Extra as ME
import Regex
import String as Str
import Time as T


type alias ValValid a =
    a -> Maybe String


type Validator
    = String (ValValid String)
    | Int (ValValid Int)
    | Float (ValValid Float)
    | Date (ValValid Date)
    | MDate (ValValid (Maybe Date))


assertOrError : Bool -> String -> Maybe String
assertOrError check error =
    if check then
        Nothing

    else
        Just error



-----------------------------------------------------
--                    STRING                       --
-----------------------------------------------------


maxLength : Int -> Validator
maxLength nmax =
    let
        func value =
            assertOrError
                (String.length value < nmax)
                ("Deve ter no máximo "
                    ++ String.fromInt nmax
                    ++ " caracteres"
                )
    in
    String func


minLength : Int -> Validator
minLength nmin =
    let
        func value =
            assertOrError
                (String.length value >= nmin)
                ("Deve ter no mínimo "
                    ++ String.fromInt nmin
                    ++ " caracteres"
                )
    in
    String func


isEmail : Validator
isEmail =
    (\str ->
        if H.isValidEmail str then
            Just "Formato inválido de e-mail"

        else
            Nothing
    )
        |> String


validCPF : Validator
validCPF =
    String
        (\cpf ->
            let
                nums =
                    cpf
                        |> String.toList
                        |> List.filter Char.isDigit
                        |> List.map String.fromChar
                        |> List.map String.toInt
                        |> ME.values

                dv1 =
                    List.reverse nums
                        |> List.drop 1
                        |> List.head
                        |> Maybe.withDefault 10

                dv2 =
                    List.reverse nums
                        |> List.head
                        |> Maybe.withDefault 10

                nums9 =
                    List.reverse nums
                        |> List.drop 2
                        |> List.reverse

                f ( index, digit ) =
                    digit * (9 - remainderBy 10 index)

                cpfDv ns =
                    List.reverse ns
                        |> zip (List.range 0 <| List.length ns)
                        |> List.map f
                        |> List.sum
                        |> remainderBy 11
                        |> remainderBy 10

                dv1_ =
                    cpfDv nums9

                dv2_ =
                    List.singleton dv1_
                        |> List.append nums9
                        |> cpfDv

                zip =
                    List.map2 (\a b -> ( a, b ))

                isValidCpf =
                    dv1 == dv1_ && dv2 == dv2_ && List.length nums9 == 9
            in
            if isValidCpf then
                Nothing

            else
                Just "CPF inválido"
        )


notEmpty : Validator
notEmpty =
    String
        (\val ->
            assertOrError
                (val /= "")
                "Campo obrigatório"
        )



-----------------------------------------------------
--                    INTEGER                      --
-----------------------------------------------------


maxInt : Int -> Validator
maxInt imax =
    let
        func val =
            assertOrError
                (val > imax)
                ("Deve ser no máximo " ++ String.fromInt imax)
    in
    Int func


minInt : Int -> Validator
minInt imin =
    let
        func val =
            assertOrError
                (val < imin)
                ("Deve ser no mínimo " ++ String.fromInt imin)
    in
    Int func



-----------------------------------------------------
--                     FLOAT                       --
-----------------------------------------------------


maxFloat : Float -> Validator
maxFloat fmax =
    let
        func val =
            assertOrError
                (val > fmax)
                ("Deve ser no máximo " ++ String.fromFloat fmax)
    in
    Float func


minFloat : Float -> Validator
minFloat fmin =
    let
        func val =
            assertOrError
                (val < fmin)
                ("Deve ser no mínimo " ++ String.fromFloat fmin)
    in
    Float func



-----------------------------------------------------
--                      DATE                       --
-----------------------------------------------------


isBefore : Date -> Validator
isBefore dat =
    let
        func val =
            assertOrError
                (D.compare val dat /= LT)
                ("Deve ser antes de " ++ D.format "dd/MM/y" dat)
    in
    Date func


isAfter : Date -> Validator
isAfter dat =
    let
        func val =
            assertOrError
                (D.compare val dat /= GT)
                ("Deve ser depois de " ++ D.format "dd/MM/y" dat)
    in
    Date func


isUsefulDay : Validator
isUsefulDay =
    let
        func val =
            assertOrError
                ([ T.Sat, T.Sun ]
                    |> List.member (D.weekday val)
                    |> not
                )
                "Deve ser um dia útil."
    in
    Date func


isDateRequired : Validator
isDateRequired =
    let
        func val =
            assertOrError
                (ME.isJust val)
                "Campo obrigatório."
    in
    MDate func



-----------------------------------------------------
--                 GETTERS BY TYPE                 --
-----------------------------------------------------


strValOrNothing : Validator -> Maybe (ValValid String)
strValOrNothing validator =
    case validator of
        String func ->
            Just func

        _ ->
            Nothing


intValOrNothing : Validator -> Maybe (ValValid Int)
intValOrNothing validator =
    case validator of
        Int func ->
            Just func

        _ ->
            Nothing


floatValOrNothing : Validator -> Maybe (ValValid Float)
floatValOrNothing validator =
    case validator of
        Float func ->
            Just func

        _ ->
            Nothing


dateValOrNothing : Validator -> Maybe (ValValid Date)
dateValOrNothing validator =
    case validator of
        Date func ->
            Just func

        _ ->
            Nothing


mDateValOrNothing : Validator -> Maybe (ValValid (Maybe Date))
mDateValOrNothing validator =
    case validator of
        MDate func ->
            Just func

        _ ->
            Nothing


filterIntValidator : Validator -> Maybe (ValValid Int)
filterIntValidator validator =
    case validator of
        Int func ->
            Just func

        _ ->
            Nothing



-----------------------
--  ERRORS FILTERS
-----------------------


getErrors : a -> List (ValValid a) -> List String
getErrors x vals =
    vals
        |> List.map (\v -> v x)
        |> ME.values


getStrErrors : List Validator -> String -> List String
getStrErrors validators value =
    getErrors value (List.map strValOrNothing validators |> ME.values)


getIntErrors : List Validator -> Int -> List String
getIntErrors validators value =
    getErrors value (List.map intValOrNothing validators |> ME.values)


getFloatErrors : List Validator -> Float -> List String
getFloatErrors validators value =
    getErrors value (List.map floatValOrNothing validators |> ME.values)


getDateErrors : List Validator -> Date -> List String
getDateErrors validators value =
    getErrors value (List.map dateValOrNothing validators |> ME.values)


getMDateErrors : List Validator -> Maybe Date -> List String
getMDateErrors validators value =
    getErrors value (List.map mDateValOrNothing validators |> ME.values)

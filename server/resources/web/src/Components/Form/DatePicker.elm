module Components.Form.DatePicker exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.DatePicker as Dat
import Components.Form.Helpers as Help exposing (..)
import Components.Form.Types as T
import Components.Form.Validator as V
import Date exposing (Date, toIsoString)
import Dict exposing (Dict)
import Helpers as H
import Html exposing (Html)
import Http
import Json.Encode as JE
import Maybe.Extra as ME
import Tuple


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    case getFieldName (T.DatePicker opts) of
        Just name ->
            opts
                |> List.map (getDatValue value)
                |> ME.values
                |> List.head
                |> Maybe.map toIsoString
                |> Maybe.map (Http.stringPart name)
                |> Maybe.map (\x -> x :: parts)
                |> Maybe.withDefault parts

        Nothing ->
            parts


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    case getFieldName (T.DatePicker opts) of
        Just name ->
            opts
                |> List.map (getDatValue value)
                |> ME.values
                |> List.head
                |> H.mDateEncoder
                |> Tuple.pair name
                |> (\x -> x :: obj)

        Nothing ->
            obj


mapToDatOption : T.FieldOption a -> Maybe (Dat.Option T.Msg)
mapToDatOption option =
    case option of
        T.Label lbl ->
            Dat.Label lbl |> Just

        T.Name name ->
            Dat.Name name |> Just

        T.Feedback str ->
            Dat.Feedback str |> Just

        _ ->
            Nothing


update : T.Config a -> String -> Maybe Date -> a
update config name newDate =
    let
        extractDatSetter opt =
            case opt of
                T.DatSetter setter ->
                    Just setter

                _ ->
                    Nothing
    in
    name
        |> Help.getFieldByName config.fields
        |> Maybe.map Help.getFieldOpts
        |> Maybe.map (List.map extractDatSetter)
        |> Maybe.map ME.values
        |> Maybe.map List.head
        |> ME.join
        |> Maybe.withDefault (\a v -> a)
        |> (\f -> f config.value newDate)


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view config validators options =
    let
        stdOpts =
            options
                |> List.map mapToDatOption
                |> ME.values

        fname =
            getFieldName (T.DatePicker options)

        getDatGetter opt =
            case opt of
                T.DatGetter getter ->
                    Just getter

                _ ->
                    Nothing

        datGetter =
            options
                |> List.map getDatGetter
                |> ME.values
                |> List.head

        getDatSetter opt =
            case opt of
                T.DatSetter setter ->
                    Just setter

                _ ->
                    Nothing

        datSetter =
            options
                |> List.map getDatSetter
                |> ME.values
                |> List.head

        valOpt =
            case datGetter of
                Just getter ->
                    Dat.Value (getter config.value)

                Nothing ->
                    Dat.Value Nothing

        updaterOpt =
            case fname of
                Just name ->
                    [ Dat.Updater (T.DatUpdate name) ]

                Nothing ->
                    []

        modelOpt =
            case fname of
                Just name ->
                    case Dict.get name config.model.datModels of
                        Just datMod ->
                            Dat.Model datMod

                        Nothing ->
                            Dat.Model Dat.initial

                Nothing ->
                    Dat.Model Dat.initial

        validsOpt =
            if config.showErrors then
                Dat.Validators validators

            else
                Dat.Validators []

        reqOpt =
            V.getMDateErrors validators Nothing
                |> List.isEmpty
                |> not
                |> Dat.Required

        opts =
            stdOpts
                ++ [ valOpt ]
                ++ updaterOpt
                ++ [ modelOpt ]
                ++ [ validsOpt ]
                ++ [ reqOpt ]
    in
    Dat.view opts

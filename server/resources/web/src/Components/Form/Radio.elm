module Components.Form.Radio exposing
    ( appendJsonValue
    , appendPart
    , update
    , view
    )

import Components.Field.Radio as Rad
import Components.Form.Helpers as Help
import Components.Form.Types as T
import Components.Form.Validator as V
import Helpers as H
import Html exposing (Html, text)
import Http
import Json.Encode as JE
import Maybe
import Maybe.Extra as ME


appendPart : a -> List (T.FieldOption a) -> List Http.Part -> List Http.Part
appendPart value opts parts =
    case Help.getFieldName (T.Radio opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> Maybe.map (Http.stringPart name)
                |> Maybe.map (\x -> x :: parts)
                |> Maybe.withDefault parts

        Nothing ->
            parts


appendJsonValue : a -> List (T.FieldOption a) -> List ( String, JE.Value ) -> List ( String, JE.Value )
appendJsonValue value opts obj =
    case Help.getFieldName (T.Radio opts) of
        Just name ->
            opts
                |> List.map (Help.getStrValue value)
                |> ME.values
                |> List.head
                |> H.nuller JE.string
                |> Tuple.pair name
                |> (\x -> x :: obj)

        Nothing ->
            obj


update : T.Config a -> String -> String -> a
update config name newVal =
    let
        extractSetter opt =
            case opt of
                T.RadSetter setter ->
                    Just setter

                _ ->
                    Nothing
    in
    name
        |> Help.getFieldByName config.fields
        |> Maybe.map Help.getFieldOpts
        |> Maybe.map (List.map extractSetter)
        |> Maybe.map ME.values
        |> Maybe.map List.head
        |> ME.join
        |> Maybe.withDefault (\a v -> a)
        |> (\f -> f config.value newVal)



-- let
--
-- in
--   name
--       |> Help.getFieldByName config.fields
--       |> Maybe.andThen (Help.getFieldOpts >> Just)
--       |> Maybe.andThen (List.foldl applyTxtOption txtDefault >> Just)
--       |> Maybe.andThen (\c -> Just (c.setter config.value newVal))
--       |> Maybe.withDefault config.value


view : T.Config a -> List V.Validator -> List (T.FieldOption a) -> Html T.Msg
view config validators options =
    let
        mapToNativeOpt opt =
            case opt of
                T.Label lbl ->
                    Rad.Label lbl |> Just

                T.Name name ->
                    Rad.Name name |> Just

                T.Feedback feedback ->
                    Rad.Feedback feedback |> Just

                T.RadGetter getter ->
                    getter config.value |> Rad.Value |> Just

                T.RadSetter setter ->
                    T.Radio options
                        |> Help.getFieldName
                        |> Maybe.map (T.RadUpdate >> Rad.OnInput)

                T.RadOptions opts ->
                    Rad.Options opts |> Just

                _ ->
                    Nothing

        validOpt =
            if config.showErrors then
                [ Rad.Validators validators ]

            else
                []

        required =
            V.getStrErrors validators ""
                |> List.isEmpty
                |> not

        radOpts =
            options
                |> List.map mapToNativeOpt
                |> ME.values
                |> (++) validOpt
                |> (++) [ Rad.Required required ]
    in
    Rad.view radOpts

module Components.Button exposing (Attr(..), view)

import Components.Types exposing (ContextType(..), Size(..), ctxType2Str, size2Str)
import FontAwesome exposing (Animation(..), Icon, Option(..), Style(..), circleNotch, iconWithOptions, spinner)
import Helpers as H
import Html exposing (Html, a, button, div, text)
import Html.Attributes exposing (class, disabled, href, style)
import Html.Events as E
import Json.Decode as J
import List
import Maybe.Extra exposing (isNothing)
import String


type alias Config msg =
    { label : Html msg
    , icon : Maybe Icon
    , iconStyle : Style
    , onClick : Maybe msg
    , typ : ContextType
    , loading : Bool
    , enabled : Bool
    , size : Size
    , light : Bool
    , iconLeft : Bool
    , clas : String
    , outline : Bool
    , href : Maybe String
    , styles : List ( String, String )
    }


defaultConf : Config msg
defaultConf =
    { label = text " "
    , icon = Nothing
    , iconStyle = Solid
    , iconLeft = True
    , onClick = Nothing
    , typ = Primary
    , loading = False
    , enabled = True
    , size = Medium
    , light = False
    , clas = ""
    , outline = False
    , href = Nothing
    , styles = []
    }


type Attr msg
    = Label (Html msg)
    | Icon Icon
    | IconStyle Style
    | IconRight
    | OnClick msg
    | Typ ContextType
    | Loading Bool
    | Enabled Bool
    | Size Size
    | Light Bool
    | Class String
    | Outline Bool
    | Href String
    | Style String String
    | NoOpt


applyAttr : Attr msg -> Config msg -> Config msg
applyAttr attr conf =
    case attr of
        Label lbl ->
            { conf | label = lbl }

        Icon icon ->
            { conf | icon = Just icon }

        IconStyle stl ->
            { conf | iconStyle = stl }

        IconRight ->
            { conf | iconLeft = False }

        OnClick msg ->
            { conf | onClick = Just msg }

        Typ typ ->
            { conf | typ = typ }

        Loading loading ->
            { conf | loading = loading }

        Enabled enabled ->
            { conf | enabled = enabled }

        Size sz ->
            { conf | size = sz }

        Light light ->
            { conf | light = light }

        Class clas ->
            { conf | clas = conf.clas ++ " " ++ clas }

        Outline outline ->
            { conf | outline = outline }

        Href href ->
            { conf | href = Just href }

        Style st1 st2 ->
            { conf | styles = ( st1, st2 ) :: conf.styles }

        NoOpt ->
            conf


spinningIcon : Html msg
spinningIcon =
    iconWithOptions
        circleNotch
        Solid
        [ Animation Spin ]
        []


pulsingIcon : Html msg
pulsingIcon =
    iconWithOptions
        spinner
        Solid
        [ Animation Pulse ]
        []


view : List (Attr msg) -> Html msg
view attrList =
    let
        conf =
            List.foldr
                applyAttr
                defaultConf
                attrList

        sizeClass =
            "btn-" ++ size2Str conf.size

        outliniePiece =
            if conf.outline then
                "outline-"

            else
                ""

        typClass =
            "btn-" ++ outliniePiece ++ ctxType2Str conf.typ

        isDisabled =
            conf.loading || not conf.enabled

        --|| isNothing conf.onClick
        disClass =
            if conf.loading || not conf.enabled then
                "btn-disabled"

            else
                ""

        label =
            conf.label

        loadingIc =
            if conf.loading then
                pulsingIcon

            else
                text ""

        ic =
            if conf.loading then
                text ""

            else
                case conf.icon of
                    Just icon ->
                        iconWithOptions
                            icon
                            conf.iconStyle
                            []
                            []

                    Nothing ->
                        text ""

        ( iconL, spsL ) =
            if conf.iconLeft then
                ( ic, text " " )

            else
                ( text "", text "" )

        ( iconR, spsR ) =
            if conf.iconLeft then
                ( text "", text "" )

            else
                ( ic, text " " )

        onInps =
            case conf.onClick of
                Just msg ->
                    [ E.preventDefaultOn
                        "click"
                        (J.map
                            (\x -> ( x, True ))
                            (J.succeed msg)
                        )
                    ]

                Nothing ->
                    []

        hrefAttr =
            case conf.href of
                Just href_ ->
                    [ href href_ ]

                Nothing ->
                    []

        customStyles =
            List.map
                (\( s1, s2 ) -> style s1 s2)
                conf.styles

        attrs =
            [ class "btn"
            , class sizeClass
            , class typClass
            , class disClass
            , class conf.clas
            , disabled isDisabled
            ]
                ++ customStyles
                ++ onInps
                ++ hrefAttr

        nod =
            case conf.href of
                Just _ ->
                    a

                Nothing ->
                    button
    in
    nod
        attrs
        [ loadingIc
        , iconL
        , spsL
        , label
        , spsR
        , iconR
        ]

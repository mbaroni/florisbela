module Components.ListView.Types exposing
    ( Config
    , ListRequest
    , ListResult
    , Model
    , Msg(..)
    , Option(..)
    , defaultConf
    , init
    , initialModel
    )

import Dict
import Html exposing (Html)
import Http
import Json.Decode as JD
import Components.Button as Btn


type alias Config a msg =
    { url : Maybe String
    , renderer : Maybe (a -> Html msg)
    , data : Maybe (List a)
    , decoder : Maybe (JD.Decoder a)
    , loading : Bool
    , actions : List (List (Btn.Attr msg))
    , ifilter : Maybe (String -> a -> Bool)
    }


defaultConf : Config a msg
defaultConf =
    { url = Nothing
    , renderer = Nothing
    , data = Nothing
    , decoder = Nothing
    , loading = False
    , actions = []
    , ifilter = Nothing
    }


type Option a msg
    = Url String
    | Renderer (a -> Html msg)
    | Data (List a)
    | Decoder (JD.Decoder a)
    | Loading Bool
    | Action (List (Btn.Attr msg))
    | Filter (String -> a -> Bool)


type alias Model a =
    { token : String
    , perPage : Int
    , page : Int
    , loading : Bool
    , data : Dict.Dict Int (List a)
    , totalData : Int
    }


type alias ListRequest a =
    { url : String
    , decoder : JD.Decoder a
    }


init : ( Model a, Cmd (Msg a) )
init =
    ( initialModel, Cmd.none )


initialModel : Model a
initialModel =
    { token = ""
    , perPage = 10
    , page = 0
    , loading = False
    , data = Dict.empty
    , totalData = 0
    }


type alias ListResult a =
    { total : Int
    , initial : Int
    , data : List a
    }


type Msg a
    = NoOp
    | ChangeToken String
    | Search (ListRequest a)
    | ChangePerPage Int
    | ChangePage Int
    | KeyHit Int
    | GotData (Result Http.Error (ListResult a))

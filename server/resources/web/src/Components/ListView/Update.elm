module Components.ListView.Update exposing (update)

import Components.ListView.Helpers exposing (..)
import Components.ListView.Types exposing (..)
import Dict
import Helpers exposing (log, onEnter)
import Http
import Maybe.Extra as ME


update : Msg a -> Model a -> ( Model a, Cmd (Msg a) )
update msg model =
    case msg of
        ChangeToken newToken ->
            ( { model | token = newToken }, Cmd.none )

        Search lreq ->
            let
                req =
                    Http.get
                        { url = lreq.url
                        , expect =
                            Http.expectJson
                                GotData
                                (listResultDecoder lreq.decoder)
                        }
            in
            ( { model | loading = True }, req )

        ChangePerPage perPage ->
            ( { model | perPage = perPage }, Cmd.none )

        ChangePage page ->
            let
                ( pblock, nDrops ) =
                    getBlockPageNDrops
                        page
                        model.perPage
                        blockSize

                hasBlock =
                    Dict.get pblock model.data
                        |> ME.isJust

                cmd =
                    if hasBlock then
                        Cmd.none

                    else
                        Cmd.none
            in
            ( { model
                | page = page
                , loading = not hasBlock
              }
            , cmd
            )

        KeyHit key ->
            let
                x =
                    key
            in
            ( { model | loading = x == 2 }, Cmd.none )

        GotData resp ->
            case resp of
                Ok res ->
                    let
                        x =
                            2
                    in
                    ( { model
                        | loading = False
                        , data =
                            Dict.insert
                                (res.initial // blockSize)
                                res.data
                                model.data
                        , totalData = res.total
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        NoOp ->
            ( model, Cmd.none )

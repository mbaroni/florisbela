module Components.ListView.View exposing (view)

import Components.Button as Btn
import Components.Field.Text as T
import Components.ListView.Helpers exposing (..)
import Components.ListView.Types exposing (..)
import Components.Types as Typ
import Dict
import FontAwesome as FA
import Helpers exposing (log, onEnter)
import Html exposing (Attribute, Html, button, div, h4, input, li, option, select, span, text, ul)
import Html.Attributes exposing (class, placeholder, style, type_, value)
import Html.Events exposing (on, onClick, onInput)
import Html.Events.Extra exposing (targetValueIntParse)
import Http
import Json.Decode as JD
import List as L
import Maybe.Extra as ME
import Url.Builder as UB



--fetchBlockPage bPage =
----------
--   VIEW
----------


header : Model a -> (Msg a -> msg) -> Config a msg -> Html msg
header model msgMapper config =
    let
        btnsSection =
            List.map Btn.view config.actions
                |> div
                    [ class "col-sm-12"
                    , class "col-md-6"
                    ]

        searchMsg =
            case config.url of
                Just url ->
                    case config.decoder of
                        Just decoder ->
                            let
                                lreq =
                                    buildListRequest
                                        url
                                        0
                                        model.token
                                        decoder
                            in
                            Search lreq
                                |> msgMapper

                        Nothing ->
                            msgMapper NoOp

                Nothing ->
                    msgMapper NoOp

        inputPiece =
            input
                [ class "form-control input-xlarge"
                , onInput (msgMapper << ChangeToken)
                , type_ "text"
                , value model.token
                , placeholder "Buscar..."
                , onEnter searchMsg
                ]
                []

        searchBtn =
            Btn.view
                [ Btn.Icon FA.search
                , Btn.Typ Typ.Info
                , Btn.OnClick searchMsg
                , Btn.Style "border-top-left-radius" "0"
                , Btn.Style "border-bottom-left-radius" "0"
                , Btn.Style "margin" "0"
                , Btn.Enabled (not model.loading)
                ]

        inputSection =
            div
                [ class "col-sm-12"
                , class "col-md-6"
                , class "d-flex"
                , class "justify-content-end"
                ]
                [ inputPiece
                , searchBtn
                ]
    in
    div
        [ class "row"
        , class "listview__header"
        ]
        [ btnsSection
        , inputSection
        ]


emptyHolder : Html msg
emptyHolder =
    div
        [ class "listview__emptyholder" ]
        [ text "Nenhum registro encontrado."
        ]


loadingHolder : Html msg
loadingHolder =
    div
        [ class "listview__emptyholder"
        , class "listview__emptyholder__loading"
        ]
        [ FA.iconWithOptions
            FA.spinner
            FA.Solid
            [ FA.Animation FA.Pulse ]
            []
        ]


exposedData : Model a -> Config a msg -> List a
exposedData model config =
    case config.data of
        Just ds ->
            ds
                |> List.drop (model.perPage * model.page)
                |> List.take model.perPage

        Nothing ->
            let
                ( bPage, nDrops ) =
                    getBlockPageNDrops
                        model.page
                        model.perPage
                        blockSize
            in
            case Dict.get bPage model.data of
                Just ds2 ->
                    ds2
                        |> List.drop nDrops
                        |> List.take model.perPage

                Nothing ->
                    []


content : Model a -> (Msg a -> msg) -> Config a msg -> Html msg
content model msgMapper config =
    let
        data =
            exposedData model config
    in
    if model.loading then
        loadingHolder

    else if List.length data == 0 then
        emptyHolder

    else
        case config.renderer of
            Just rend ->
                data
                    |> List.map rend
                    |> div []

            Nothing ->
                emptyHolder


buildPagerList : Int -> Int -> List Int
buildPagerList npages cp =
    if npages < 8 then
        List.range 1 npages

    else if cp > npages - 2 then
        [ 1, 2, 3, 0, npages - 2, npages - 1, npages ]

    else if cp == npages - 2 then
        [ 1, 2, 0, npages - 3, npages - 2, npages - 1, npages ]

    else if cp == npages - 3 then
        [ 1, 0, npages - 4, npages - 3, npages - 2, npages - 1, npages ]

    else
        case cp of
            1 ->
                [ 1, 2, 3, 0, npages - 2, npages - 1, npages ]

            2 ->
                [ 1, 2, 3, 0, npages - 2, npages - 1, npages ]

            3 ->
                [ 1, 2, 3, 4, 0, npages - 1, npages ]

            4 ->
                [ 1, 2, 3, 4, 5, 0, npages ]

            _ ->
                [ 1, 0, cp - 1, cp, cp + 1, 0, npages ]


totPages : Model a -> Config a msg -> Int
totPages model config =
    case config.data of
        Just ds ->
            ds
                |> List.length
                |> toFloat
                |> (\x ->
                        (x / toFloat model.perPage)
                            |> ceiling
                   )

        Nothing ->
            model.totalData // model.perPage


pagination : Model a -> (Msg a -> msg) -> Config a msg -> Html msg
pagination model msgMapper config =
    let
        totPgs =
            totPages model config

        pageList =
            buildPagerList totPgs (model.page + 1)

        activeClass p =
            if p == model.page + 1 then
                "listview__pagination-item__active"

            else
                ""

        itemOnClick p =
            if p - 1 == model.page then
                class ""

            else
                onClick (msgMapper (ChangePage (p - 1)))

        renderLiEtc =
            li
                [ class "listview__pagination-item"
                ]
                [ text "..." ]

        renderLiPage p =
            li
                [ class "listview__pagination-item"
                , class (activeClass p)
                , itemOnClick p
                ]
                [ text (String.fromInt p) ]

        renderLi p =
            if p == 0 then
                renderLiEtc

            else
                renderLiPage p

        lis =
            List.map renderLi pageList
    in
    div
        [ class "col-sm-12"
        , class "col-md-6"
        ]
        [ div
            [ class "listview__pagination-wrapper" ]
            [ ul
                [ class "listview__pagination" ]
                lis
            ]
        ]


controller : Model a -> (Msg a -> msg) -> Config a msg -> Html msg
controller model msgMapper config =
    let
        nexp =
            exposedData model config
                |> List.length

        ntot =
            case config.data of
                Just ds ->
                    List.length ds

                Nothing ->
                    model.totalData
    in
    div
        [ class "col-sm-12"
        , class "col-md-6"
        , class "listview__counter"
        ]
        [ div
            []
            [ span
                []
                [ text "Exibindo " ]
            , span
                [ style "font-weight" "bold" ]
                [ text (" " ++ String.fromInt nexp ++ " ") ]
            , span
                []
                [ text " de " ]
            , span
                [ style "font-weight" "bold" ]
                [ text (" " ++ String.fromInt ntot ++ " ") ]
            , span
                []
                [ text " registros." ]
            ]
        ]


footer : Model a -> (Msg a -> msg) -> Config a msg -> Html msg
footer model msgMapper config =
    div
        [ class "row"
        , class "listview__footer"
        , style "margin-top" "0.5rem"
        ]
        [ controller model msgMapper config
        , pagination model msgMapper config
        ]


perPageChanger : (Msg a -> msg) -> Html msg
perPageChanger msgMapper =
    let
        perPagesOpts =
            allowedPerPages
                |> List.map String.fromInt
                |> List.map
                    (\pp -> option [ value pp ] [ text pp ])

        perPageSelect =
            select
                [ on "change" (JD.map (msgMapper << ChangePerPage) targetValueIntParse) ]
                perPagesOpts
    in
    div
        [ class "col-sm-12"
        , class "col-md-6"
        , class "listview__counter"
        ]
        [ div
            []
            [ span
                []
                [ text "Exibir " ]
            , perPageSelect
            , span
                []
                [ text " registros." ]
            ]
        ]


subfooter : Model a -> (Msg a -> msg) -> Html msg
subfooter model msgMapper =
    div
        [ class "row"
        , class "listview__footer"
        ]
        [ perPageChanger msgMapper ]


view : Model a -> (Msg a -> msg) -> List (Option a msg) -> Html msg
view model msgMapper options =
    let
        config =
            L.foldr applyOption defaultConf options
    in
    div
        [ class "listview__wrapper" ]
        [ header model msgMapper config
        , content model msgMapper config
        , footer model msgMapper config
        , subfooter model msgMapper
        ]

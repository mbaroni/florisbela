module Components.ListView.Helpers exposing
    ( allowedPerPages
    , applyOption
    , blockSize
    , buildListRequest
    , getBlockPageNDrops
    , listResultDecoder
    )

import Components.ListView.Types exposing (..)
import Json.Decode as JD
import Url.Builder as UB


blockSize : Int
blockSize =
    50


allowedPerPages : List Int
allowedPerPages =
    [ 5, 10, 25, 50, 100 ]


applyOption : Option a msg -> Config a msg -> Config a msg
applyOption option config =
    case option of
        Url url ->
            { config | url = Just url }

        Renderer renderer ->
            { config | renderer = Just renderer }

        Data data ->
            { config | data = Just data }

        Decoder decoder ->
            { config | decoder = Just decoder }

        Loading loading ->
            { config | loading = loading }

        Action btnattrs ->
            { config | actions = btnattrs :: config.actions }

        Filter ifilter ->
            { config | ifilter = Just ifilter }


getBlockPageNDrops : Int -> Int -> Int -> ( Int, Int )
getBlockPageNDrops page perPage perBlock =
    let
        pBlock =
            (page * perPage) // perBlock

        nDrop =
            (page * perPage) - (perBlock * pBlock)
    in
    ( pBlock, nDrop )


buildListRequest : String -> Int -> String -> JD.Decoder a -> ListRequest a
buildListRequest url blockPage token decoder =
    { url =
        UB.relative
            [ url ]
            [ UB.string "search" token
            , UB.int "page" blockPage
            , UB.int "perpage" blockSize
            , UB.string "token" "V_547thHARWeNjcN674WnQ"
            ]
    , decoder = decoder
    }


listResultDecoder : JD.Decoder a -> JD.Decoder (ListResult a)
listResultDecoder adec =
    JD.map3
        ListResult
        (JD.field "total" JD.int)
        (JD.field "initial" JD.int)
        (JD.field "data" (JD.list adec))

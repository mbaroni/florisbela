module Components.Synopsis exposing
    ( Option(..)
    , view
    )

import Components.Button as Btn
import Components.Types exposing (ContextType(..), Size(..))
import Helpers exposing (log)
import Html exposing (Html, a, div, h6, img, input, span, text)
import Html.Attributes exposing (class, src, style, width)
import Html.Events exposing (onClick)
import Maybe.Extra exposing (isJust)


type alias Config msg =
    { title : Maybe (Html msg)
    , content : Maybe (Html msg)
    , actions : List (List (Btn.Attr msg))
    , corner : Maybe (Html msg)
    , href : Maybe String
    , onClick : Maybe msg
    , thumb : Maybe (Html msg)
    }


defaultConf : Config msg
defaultConf =
    { title = Nothing
    , content = Nothing
    , actions = []
    , corner = Nothing
    , href = Nothing
    , onClick = Nothing
    , thumb = Nothing
    }


type Option msg
    = Title (Html msg)
    | Content (Html msg)
    | Action (List (Btn.Attr msg))
    | Corner (Html msg)
    | Href String
    | OnClick msg
    | Thumb (Html msg)


applyOption : Option msg -> Config msg -> Config msg
applyOption option config =
    case option of
        Title title ->
            { config | title = Just title }

        Content content ->
            { config | content = Just content }

        Action action ->
            { config | actions = config.actions ++ [ action ] }

        Corner corner ->
            { config | corner = Just corner }

        Href href ->
            { config | href = Just href }

        OnClick onClk ->
            { config | onClick = Just onClk }

        Thumb thumb ->
            { config | thumb = Just thumb }


buildConf : List (Option msg) -> Config msg
buildConf options =
    List.foldl applyOption defaultConf options


view : List (Option msg) -> Html msg
view options =
    let
        config =
            buildConf options

        thumb =
            case config.thumb of
                Nothing ->
                    text ""

                Just tmb ->
                    div
                        [ class "p-2" ]
                        [ tmb ]

        title =
            case config.title of
                Just tlt ->
                    h6
                        [ class "syn__title" ]
                        [ tlt ]

                Nothing ->
                    text ""

        content =
            case config.content of
                Just ctnt ->
                    span
                        [ class "syn__subtitle"
                        ]
                        [ ctnt ]

                Nothing ->
                    text ""

        btns =
            List.map
                (\attrs -> Btn.view (attrs ++ [ Btn.Size Mini ]))
                config.actions

        actions =
            div
                [ class "comment-footer" ]
                btns

        footer =
            actions

        clickHandler =
            case config.onClick of
                Nothing ->
                    class ""

                Just msg ->
                    onClick msg

        cursorStyle =
            if isJust config.onClick || isJust config.href then
                style "cursor" "pointer"

            else
                style "" ""

        nod =
            if isJust config.href then
                a

            else
                div
    in
    nod
        [ class "syn__wrapper"
        , clickHandler
        , cursorStyle
        ]
        [ thumb
        , div
            [ class "comment-text"
            , class "w-100"
            ]
            [ title
            , content
            , footer
            ]
        ]

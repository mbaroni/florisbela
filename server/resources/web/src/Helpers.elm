module Helpers exposing
    ( Option(..)
    , abrevFileName
    , appIfJust
    , baseInputView
    , baseInputView2
    , buildErrorDict
    , buildStyle
    , dateDecoder
    , dateEncoder
    , humanizeSize
    , isValidEmail
    , log
    , mDateEncoder
    , maybeNull
    , nuller
    , onEnter
    , posixDayFormat
    , posixDecode
    , posixTimeFormat
    , send
    , sendAfter
    , strHash
    )

import Bitwise exposing (shiftLeftBy)
import Char exposing (toCode)
import Components.Types exposing (..)
import Date exposing (Date, toIsoString)
import Debug as D
import Dict exposing (Dict)
import Html exposing (Attribute, Html, div, label, text)
import Html.Attributes exposing (class, for, property)
import Html.Events exposing (keyCode, on)
import ISO8601
import Json.Decode as JD
import Json.Encode as Encode exposing (Value, null, string)
import List exposing (map)
import Maybe as M
import Process
import Regex
import String as Str exposing (concat)
import Task
import Time as Time exposing (Posix)


maybeNull : JD.Decoder a -> JD.Decoder (Maybe a)
maybeNull decoder =
    JD.oneOf [ JD.null Nothing, JD.map Just decoder ]


log s x =
    if True then
        D.log s x

    else
        x


{-| Set 'null' JSON value for Maybe.Nothing.
-}
nuller : (a -> Value) -> Maybe a -> Value
nuller encoder x =
    x
        |> M.map encoder
        |> M.withDefault null


{-| Decode Date type from JSON Value
-}
dateDecoder : JD.Decoder Date.Date
dateDecoder =
    JD.string
        |> JD.andThen
            (\str ->
                case Date.fromIsoString str of
                    Ok date ->
                        JD.succeed date

                    Err err ->
                        JD.fail err
            )


posixDecode : JD.Decoder Posix
posixDecode =
    JD.string
        |> JD.andThen
            (\str ->
                case ISO8601.fromString str of
                    Ok time ->
                        JD.succeed (ISO8601.toPosix time)

                    Err err ->
                        JD.fail err
            )


monthToInt : Time.Month -> Int
monthToInt mon =
    case mon of
        Time.Jan ->
            1

        Time.Feb ->
            2

        Time.Mar ->
            3

        Time.Apr ->
            4

        Time.May ->
            5

        Time.Jun ->
            6

        Time.Jul ->
            7

        Time.Aug ->
            8

        Time.Sep ->
            9

        Time.Oct ->
            10

        Time.Nov ->
            11

        Time.Dec ->
            12


zPad : Int -> String -> String
zPad mi num =
    "0000000"
        ++ num
        |> String.right mi


posixDayFormat : Posix -> String
posixDayFormat posix =
    let
        zone =
            Time.utc

        day =
            Time.toDay zone posix |> String.fromInt |> zPad 2

        mon =
            Time.toMonth zone posix |> monthToInt |> String.fromInt |> zPad 2

        yea =
            Time.toYear zone posix |> String.fromInt |> zPad 4
    in
    day ++ "/" ++ mon ++ "/" ++ yea


posixTimeFormat : Posix -> String
posixTimeFormat posix =
    let
        zone =
            Time.utc

        hou =
            Time.toHour zone posix |> String.fromInt |> zPad 2

        minu =
            Time.toMinute zone posix |> String.fromInt |> zPad 2

        sec =
            Time.toSecond zone posix |> String.fromInt |> zPad 2

        time =
            hou ++ ":" ++ minu ++ ":" ++ sec

        day =
            posixDayFormat posix
    in
    day ++ " " ++ time


{-| Encode Date type to JSON Value
-}
dateEncoder : Date -> Value
dateEncoder date =
    date
        |> toIsoString
        |> string


mDateEncoder : Maybe Date -> Value
mDateEncoder mdate =
    case mdate of
        Just date ->
            date
                |> toIsoString
                |> string

        Nothing ->
            null


{-| Css Style builder
-}
buildStyle : List ( String, String ) -> Attribute msg
buildStyle stls =
    let
        wrpSty ( s1, s2 ) =
            s1 ++ ":" ++ s2 ++ ";"
    in
    property "style" (Encode.string (concat (map wrpSty stls)))


{-| Send a msg throught Cmd
-}
send : msg -> Cmd msg
send msg =
    Task.succeed msg
        |> Task.perform identity


{-| Send a msg throught Cmd after "milli" milliseconds
-}
sendAfter : Float -> msg -> Cmd msg
sendAfter milli msg =
    Task.perform
        (always msg)
        (Process.sleep milli)


{-| On keyboard Enter
-}
onEnter : msg -> Attribute msg
onEnter m =
    let
        enterDec key =
            if key == 13 then
                JD.succeed m

            else
                JD.fail "wrong key"

        rdecoder =
            JD.andThen enterDec keyCode
    in
    on "keydown" rdecoder


type alias BaseInputConfig msg =
    { label : Html msg
    , input : Html msg
    , ctxType : ContextType
    , name : String
    , horizontal : Bool
    , feedback : Maybe String
    , required : Bool
    }


defaultConfig : BaseInputConfig msg
defaultConfig =
    { label = text ""
    , input = text ""
    , ctxType = Default
    , name = ""
    , horizontal = True
    , feedback = Nothing
    , required = False
    }


type Option msg
    = Label (Html msg)
    | Input (Html msg)
    | ContextType ContextType
    | Name String
    | Horizontal
    | Vertical
    | Feedback String
    | Required
    | NoOpt


applyOption : Option msg -> BaseInputConfig msg -> BaseInputConfig msg
applyOption option config =
    case option of
        Label label ->
            { config | label = label }

        Input input ->
            { config | input = input }

        ContextType ctxType ->
            { config | ctxType = ctxType }

        Name name ->
            { config | name = name }

        Horizontal ->
            { config | horizontal = True }

        Vertical ->
            { config | horizontal = False }

        Feedback feedback ->
            { config | feedback = Just feedback }

        Required ->
            { config | required = True }

        NoOpt ->
            config


build : List (Option msg) -> BaseInputConfig msg
build opts =
    List.foldl applyOption defaultConfig opts


baseInputView2 : List (Option msg) -> Html msg
baseInputView2 options =
    let
        config =
            build options
    in
    if config.horizontal then
        baseInputHorizontal config

    else
        baseInputVertical config


{-| Input Render
-}
baseInputView : BaseInputConfig msg -> Html msg
baseInputView config =
    if config.horizontal then
        baseInputHorizontal config

    else
        baseInputVertical config


baseInputHorizontal : BaseInputConfig msg -> Html msg
baseInputHorizontal config =
    let
        contextStr =
            ctxType2Str config.ctxType

        feedback =
            case config.feedback of
                Just feedbackText ->
                    div
                        [ class "input-feedback", class ("input-feedback-" ++ contextStr) ]
                        [ text feedbackText ]

                Nothing ->
                    text ""
    in
    div
        [ class "form-group row" ]
        [ label
            [ class "col-sm-3", for config.name ]
            [ div
                [ class "box form-label text-right" ]
                [ config.label ]
            ]
        , div
            [ class "col-sm-9" ]
            [ div
                [ class "box" ]
                [ config.input
                , feedback
                ]
            ]
        ]


baseInputVertical : BaseInputConfig msg -> Html msg
baseInputVertical config =
    div
        [ class "form-group" ]
        [ label
            [ class "col-sm-3", for config.name ]
            [ div
                []
                [ config.label ]
            ]
        , config.input
        ]


appIfJust : Maybe a -> List a -> List a
appIfJust mb ls =
    case mb of
        Just x ->
            ls ++ [ x ]

        Nothing ->
            ls


strHash : String -> Int
strHash str =
    let
        updateHash c h =
            shiftLeftBy h 5 + h + toCode c
    in
    List.foldl updateHash 5381 (Str.toList str)


isValidEmail : String -> Bool
isValidEmail email =
    let
        validEmail =
            "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
                |> Regex.fromStringWith { caseInsensitive = True, multiline = False }
                |> Maybe.withDefault Regex.never
    in
    Regex.contains validEmail email


buildErrorDict : List ( String, List ( Bool, String ) ) -> Dict String (List String)
buildErrorDict confs =
    let
        filterErrs errs =
            errs
                |> List.filter (Tuple.first >> not)
                |> List.map Tuple.second

        filterActiveErrs field =
            ( Tuple.first field, filterErrs (Tuple.second field) )

        errorIsNotEmpty o =
            Tuple.second o
                |> List.isEmpty
                |> not
    in
    confs
        |> List.map filterActiveErrs
        |> List.filter errorIsNotEmpty
        |> Dict.fromList


abrevFileName : Int -> String -> String
abrevFileName maxLen fname =
    if String.length fname > maxLen then
        let
            len =
                String.length fname

            pre =
                String.dropRight
                    (len - maxLen + 10)
                    fname

            pos =
                String.dropLeft
                    (len - 7)
                    fname
        in
        pre ++ "..." ++ pos

    else
        fname


humanizeSize : Int -> String
humanizeSize size =
    String.fromInt size ++ "b"

port module Api exposing
    ( GetConfig
    , LoginResp(..)
    , ParamsRequester
    , PostConfig
    , ReqConfig
    , Resource
    , authedResource
    , execRequest
    , getJwt
    , gotJwt
    , myExecRequest
    , nonAuthedResource
    , postLogin
    , saveJwt
    )

import Either exposing (Either)
import Either.Decode as ED
import Helpers exposing (log)
import Http exposing (Body, Expect, Header)
import Json.Decode as JD
import Json.Encode as JE exposing (Value)
import Maybe exposing (withDefault)
import Session as Session exposing (JWT)
import Url.Builder as UB exposing (QueryParameter)


port saveJwt : String -> Cmd msg


port getJwt : () -> Cmd msg


port gotJwt : (String -> msg) -> Sub msg


type alias ParamsRequester =
    List ( String, String ) -> ReqConfig


type alias ReqConfig2 a msg =
    { url : List String
    , qParams : List QueryParameter
    , jwt : Maybe String
    , decoder : JD.Decoder a
    , body : Maybe Value
    , toMsg : Result Http.Error a -> msg
    }


url paths queryParams =
    UB.absolute ([ "api" ] ++ paths) queryParams


post : ReqConfig2 a msg -> Cmd msg
post config =
    let
        authHeader =
            case config.jwt of
                Just jwt ->
                    [ Http.header "Authorization" ("Bearer " ++ jwt) ]

                Nothing ->
                    []

        body =
            Maybe.map Http.jsonBody config.body
                |> withDefault Http.emptyBody

        expect =
            Http.expectJson config.toMsg config.decoder
    in
    Http.request
        { method = "POST"
        , headers = authHeader
        , url = url config.url config.qParams
        , body = body
        , expect = expect
        , timeout = Nothing
        , tracker = Nothing
        }


type alias LoginOkResp =
    { email : String, token : String }


type alias LoginErrResp =
    { slug : String, msg : String }


type LoginResp
    = LoginOk LoginOkResp
    | LoginErr LoginErrResp


loginOkDecoder : JD.Decoder LoginResp
loginOkDecoder =
    JD.map2 LoginOkResp
        (JD.at [ "email" ] JD.string)
        (JD.at [ "token" ] JD.string)
        |> JD.map LoginOk


loginErrDecoder : JD.Decoder LoginResp
loginErrDecoder =
    JD.map2 LoginErrResp
        (JD.at [ "slug" ] JD.string)
        (JD.at [ "msg" ] JD.string)
        |> JD.map LoginErr


postLogin : String -> String -> (Result Http.Error LoginResp -> msg) -> Cmd msg
postLogin email password toMsg =
    post
        { url = [ "auth", "login" ]
        , qParams = []
        , jwt = Nothing
        , decoder = JD.oneOf [ loginOkDecoder, loginErrDecoder ]
        , toMsg = toMsg
        , body =
            JE.object
                [ ( "email", JE.string email )
                , ( "password", JE.string password )
                ]
                |> Just
        }


buildUrl : List String -> List ( String, String ) -> String
buildUrl utokens params =
    UB.absolute utokens (List.map (\( x, y ) -> UB.string x y) params)



--------------------------------------------------------
--         AUTHENTICATED METHODS (JWT)                --
--------------------------------------------------------


execRequest : Expect msg -> ReqConfig -> Cmd msg
execRequest expect reqConf =
    let
        reqConf_ =
            { method = reqConf.method
            , headers = reqConf.headers
            , url = reqConf.url
            , body = reqConf.body
            , expect = expect
            , timeout = Just 3000
            , tracker = reqConf.tracker
            }
    in
    Http.request reqConf_


myExecRequest : (Result (Maybe String) a -> msg) -> JD.Decoder a -> ReqConfig -> Cmd msg
myExecRequest toMsg decoder reqConf =
    let
        decodeErrMsg =
            JD.field "msg" JD.string

        resulter resp =
            case resp of
                Http.BadUrl_ str ->
                    Err Nothing

                Http.Timeout_ ->
                    Err Nothing

                Http.NetworkError_ ->
                    Err Nothing

                Http.BadStatus_ metadata body ->
                    case JD.decodeString decodeErrMsg body of
                        Ok msg ->
                            Err (Just msg)

                        Err _ ->
                            Err Nothing

                Http.GoodStatus_ metadata body ->
                    case JD.decodeString decoder body of
                        Ok value ->
                            Ok value

                        Err err ->
                            Err Nothing

        expect =
            Http.expectStringResponse
                toMsg
                resulter

        reqConf_ =
            { method = reqConf.method
            , headers = reqConf.headers
            , url = reqConf.url
            , body = reqConf.body
            , expect = expect
            , timeout = Just 3000
            , tracker = reqConf.tracker
            }
    in
    Http.request reqConf_


type alias ReqConfig =
    { method : String
    , headers : List Header
    , url : String
    , body : Body
    , timeout : Maybe Float
    , tracker : Maybe String
    }


authedReqConfig : Maybe JWT -> (ReqConfig -> ReqConfig)
authedReqConfig jwt =
    \reqConfig ->
        let
            authHeaders =
                case jwt of
                    Just token ->
                        [ Http.header "Authorization" ("Bearer " ++ token) ]

                    Nothing ->
                        []
        in
        { method = reqConfig.method
        , headers = authHeaders ++ reqConfig.headers
        , url = reqConfig.url
        , body = reqConfig.body
        , timeout = reqConfig.timeout
        , tracker = reqConfig.tracker
        }


type alias GetConfig =
    { utokens : List String
    , params : List ( String, String )
    }


authedGetConfig : Maybe JWT -> (List String -> List ( String, String ) -> ReqConfig)
authedGetConfig jwt =
    \utokens params ->
        authedReqConfig jwt
            { method = "GET"
            , headers = []
            , url = buildUrl utokens params
            , body = Http.emptyBody
            , timeout = Nothing
            , tracker = Nothing
            }


type alias PostConfig =
    { utokens : List String
    , params : List ( String, String )
    , body : Body
    }


authedPostConfig : Maybe JWT -> List String -> List ( String, String ) -> Body -> ReqConfig
authedPostConfig jwt utokens params body =
    authedReqConfig jwt
        { method = "POST"
        , headers = []
        , url = buildUrl utokens params
        , body = body
        , timeout = Nothing
        , tracker = Nothing
        }


type alias Resource =
    { request : ReqConfig -> ReqConfig
    , get : List String -> List ( String, String ) -> ReqConfig
    , post : List String -> List ( String, String ) -> Body -> ReqConfig
    }


getAuthedResource : JWT -> Resource
getAuthedResource jwt =
    { request = authedReqConfig (Just jwt)
    , get = authedGetConfig (Just jwt)
    , post = authedPostConfig (Just jwt)
    }



--------------------------------------------------------
--           UNAUTHENTICATED METHODS                  --
--------------------------------------------------------


nonAuthedResource : Resource
nonAuthedResource =
    { request = authedReqConfig Nothing
    , get = authedGetConfig Nothing
    , post = authedPostConfig Nothing
    }


authedResource : Session.Model -> Resource
authedResource session =
    case session of
        Session.Logged user ->
            getAuthedResource user.jwt

        --nonAuthedResource
        Session.Guest ->
            nonAuthedResource

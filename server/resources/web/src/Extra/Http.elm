module Extra.Http exposing (..)

import Date exposing (Date, toIsoString)
import Http
import Maybe.Extra as ME


mStringPart : String -> Maybe String -> Http.Part
mStringPart field mstr =
    Http.stringPart field (Maybe.withDefault "" mstr)


mDatePart : String -> Maybe Date -> Http.Part
mDatePart field mDate =
    mDate
        |> Maybe.map toIsoString
        |> Maybe.withDefault ""
        |> Http.stringPart field

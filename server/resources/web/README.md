# Florisbela Admin Project

Project for managing Florisbela's business (work in progress).

Author: Marcos Daniel Baroni (marcosdaniel.baroni@gmail.com) Jan 2019 - Present

Repository link: [https://bitbucket.org/mbaroni/florisbela](https://bitbucket.org/mbaroni/florisbela/src/master/server/resources/web/)


#### Technologies:
Elm, Express.js, PostgreSQL, Docker/Docker Compose, MinIO, Redis, Webpack.

#### Project directories (server/):

Short description of main project directories.

 * **resources/web/src:** FrontEnd application project (Elm source)
    * **Components:** Shared components
    * **Entries:** Web app entries (roles: Admin, Public)
         * **Entries/\*/Main.js:** Main SPA entry
         * **Entries/\*/App:** Application setup
         * **Entries/\*/Pages:** Application pages
    * **Shared:** Shared layouts
 * **src:**  BackEnd (Express.js)
 * **containers:**  Docker containers (DB, redis, MinIO, etc)
 * **models:**  Sequelize.js Models

#### Some Elm Features:

Example of some Elm features used in Frontend application (path: server/resources/web/src/).

##### 1. Form submission** (w/ file upload):
 - Components/Field/File.elm
 - Entries/Admin/Pages/Profile/Edit.elm:159
 - Entries/Admin/Pages/Profile/Model.elm:141


##### 2. JWT authenticated requests:
 - Api.elm:119
 - Entries/Admin/App/Model.elm:30
 - Entries/Admin/App/Update.elm:220


##### 3. LocalStorage access (Elm ports):
 - Entries/Admin/index.js
 - Api.elm:29


##### 4. Url parsing/routing:
   - Entries/Admin/App/Route.elm:35
   - Entries/Admin/App/View.elm:58
   - Entries/Admin/App/Update.elm:41


##### 5. Json Encoding/Decoding:
  - Entries/Admin/Pages/Profile/Model.elm: 119
  - Components/ListView/Helpers.elm:76
  - Components/Field/Typeahead.elm:162

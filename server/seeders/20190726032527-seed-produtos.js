'use strict';
var faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
    faker.seed(1);
    var produtos = [];
    for( var i = 0 ; i < 100 ; i++ ){
      produtos.push({
        nome: faker.commerce.productName(),
        valor: faker.commerce.price(1, 100000)/100.0,
        marcaId: faker.random.number({min: 1, max: 10}),
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
    return queryInterface.bulkInsert('produtos', produtos, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('produtos', null, {});
  }
};

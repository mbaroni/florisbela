'use strict';
var faker = require('faker');
var bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    faker.seed(1);
    var users = [
      { email: 'marcosdaniel.baroni@gmail.com'
      , password: bcrypt.hashSync('12341234', 10)
      , isAdmin: true
      , createdAt: new Date()
      , updatedAt: new Date()
      }
    ];
    for( var i = 0 ; i < 10 ; i++ ){
      //var defaultPassword = faker.internet.password();
      var defaultPassword = '12341234';
      
      users.push(
        { email: faker.internet.email().toLowerCase()
        , password: bcrypt.hashSync(defaultPassword, 10)
        , nome: `${faker.name.firstName()} ${faker.name.lastName()}`
        , sexo: ['F', 'M'][faker.random.number({min: 0, max: 1})]
        , isAdmin: false
        , ufId: faker.random.number({min: 1, max: 27})
        , datNascimento: faker.date.past()
        , fileProfile: null
        , favoriteProdutoId: null
        , createdAt: new Date()
        , updatedAt: new Date()
        }
      );
    }
    return queryInterface.bulkInsert('users', users, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};

'use strict';
var faker = require('faker');

module.exports = {
  up: (queryInterface, Sequelize) => {
    faker.seed(1);
    var marcas = [];
    for( var i = 0 ; i < 10 ; i++ ){
      marcas.push({
        id: i+1,
        nome: faker.company.companyName(),
        countryCode: faker.address.countryCode(),
        createdAt: new Date(),
        updatedAt: new Date(),
      });
    }
    return queryInterface.bulkInsert('marcas', marcas, {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('marcas', null, {});
  }
};

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    var fileSourcesDef = {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      description: {
        type: Sequelize.STRING,
        allowNull: true
      },
      dscReference: {
        type: Sequelize.STRING,
        allowNull: true
      },
      bucket: {
        type: Sequelize.STRING,
        allowNull: true
      }
    };

    var filesDef = {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      original: {
        type: Sequelize.STRING,
        allowNull: false
      },
      size: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      mimetype: {
        type: Sequelize.STRING,
        allowNull: true
      },
      hashname: {
        type: Sequelize.STRING,
        allowNull: true
      },
      sourceId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      referenceId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    };

    return Promise.all([
      queryInterface.createTable('file_sources', fileSourcesDef),
      queryInterface.createTable('files', filesDef),
    ]);
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction( (t) => {
      return Promise.all([
        queryInterface.dropTable('file_sources'),
        queryInterface.dropTable('files'),
      ])
    });
  }
};

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /* USER TABLE */
    var userDef = {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    }

    var addEmailConstraint = () => {
      queryInterface.addConstraint('users', ['email'], {
        type: 'unique',
        name: 'users_unique_email'
      });
    };

    return queryInterface.createTable('users', userDef)
    .then( addEmailConstraint );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('users');
  }
};

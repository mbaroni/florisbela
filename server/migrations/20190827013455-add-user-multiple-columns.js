'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction( t => {
      return Promise.all([
        queryInterface.addColumn('users', 'sexo', {
          type: Sequelize.STRING(1),
          defaultValue: 'M'
        }, { transaction: t }),
        queryInterface.addColumn('users', 'isAdmin', {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
          allowNull: false
        }, { transaction: t }),
        queryInterface.addColumn('users', 'ufId', {
          type: Sequelize.INTEGER,
        }, { transaction: t }),
        queryInterface.addColumn('users', 'datNascimento', {
          type: Sequelize.DATEONLY,
        }, { transaction: t }),
        queryInterface.addColumn('users', 'fileProfile', {
          type: Sequelize.STRING,
        }, { transaction: t }),
        queryInterface.addColumn('users', 'favoriteProdutoId', {
          type: Sequelize.INTEGER,
        }, { transaction: t })
      ])
    });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction( (t) => {
      return Promise.all([
        queryInterface.removeColumn('users', 'sexo', { transaction: t }),
        queryInterface.removeColumn('users', 'isAdmin', { transaction: t }),
        queryInterface.removeColumn('users', 'ufId', { transaction: t }),
        queryInterface.removeColumn('users', 'datNascimento', { transaction: t }),
        queryInterface.removeColumn('users', 'fileProfile', { transaction: t }),
        queryInterface.removeColumn('users', 'favoriteProdutoId', { transaction: t })
      ]);
    })
  }
};

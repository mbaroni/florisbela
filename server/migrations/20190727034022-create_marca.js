'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const tableDef = {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false
      },
      countryCode: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'BR'
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    };
    return queryInterface.createTable('marcas', tableDef);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('marcas');
  }
};

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    var command = 'CREATE EXTENSION IF NOT EXISTS unaccent;';
    return queryInterface.sequelize.query(command);
  },

  down: (queryInterface, Sequelize) => {
    return Promise.all([]);
  }
};

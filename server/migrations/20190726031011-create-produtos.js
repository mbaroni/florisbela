'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    const prodDef = {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false
      },
      valor: {
        type: Sequelize.FLOAT,
        allowNull: false,
      },
      marcaId: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: false,
      }
    };
    return queryInterface.createTable('produtos', prodDef);
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('produtos');
  }
};

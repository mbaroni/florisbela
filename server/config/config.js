module.exports = {
  /* Development */
  development: {
    dialect: "postgres",
    username: 'florisbela_dev',
    password: 'florisbela_dev',
    database: 'florisbela_dev',
    host: 'localhost',
    port: '6543',
    logging: str => {console.log(' * ' + str)},
    web: {
      host: process.env.HOSTNAME || 'localhost',
      port: process.env.PORT || '8080'
    },
    redis: {
      host: 'localhost',
      port: '6379'
    },
    minio: {
      endPoint: '192.168.1.100',
      port: 9000,
      accessKey: '8NQLFQm2ajtPLm3Yh1RX',
      secretKey: 'mUKfLIBC5HT9x3VKwWjRRO3EJVyd9s1JD7QWvk4p',
      useSSL: false
    },
    upload_dest: process.env.UPLOAD_DEST || '/tmp'
  },
  test: {
    dialect: "postgres",
    username: 'florisbela_test',
    password: '12345678',
    database: 'florisbela_test',
    host: 'localhost'
  },
  production: {
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOSTNAME,
    dialect: 'mysql',
    use_env_variable: 'DATABASE_URL'
  }
};

var MinioClient = require('minio');
var env = process.env.NODE_ENV || 'development';
var config = require('./config.js')[env];

const Minio = new MinioClient(config.minio)

export default Minio;

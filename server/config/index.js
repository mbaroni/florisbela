const multer = require('multer')
var Minio = require('minio');
var env = process.env.NODE_ENV || 'development';
var config = require('./config.js')[env];

module.exports = {
  upload: multer({dest: config.upload_dest }),
  minio: new Minio.Client(config.minio)
};

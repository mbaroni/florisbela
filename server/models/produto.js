'use strict';

module.exports = (sequelize, DataTypes) => {
  var Produto = sequelize.define('Produto', {
    nome: DataTypes.STRING,
    valor: DataTypes.FLOAT,
  }, { tableName: 'produtos' });
  return Produto;
};

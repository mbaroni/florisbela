'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/config.js')[env];
var db        = {};

if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));

    /* Grid Response */
    model.gridResponce = (req, res, query) => {
      var { matchFields, where } = query;
      var { page, perPage, search } = req.query;
      var offset = !!perPage && !!page
        ? page*perPage
        : 0 ;
      var limit = perPage || 10000000;

      if( matchFields ){
        matchFields = matchFields.filter( x => !!x);
        var statments = [];
        statments = matchFields.map( fname => (
          Sequelize.where(
            Sequelize.fn('unaccent', Sequelize.col(fname)),
            {[Sequelize.Op.iLike]: `%${search || ''}%`}
          )
        ));
        if( !!query.where ){
          statments.push(Object.assign({}, query.where));
        }
        query.where = {
          [Sequelize.Op.and]: statments
        };
        delete query.matchFields;
      }

      model.findAndCountAll({
        ...query, offset, limit
      })
      .then( elems => {
        res.json(elems)
      });
    };
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

/* Associations */
db.Marca.hasMany(db.Produto, {foreignKey: 'marcaId', as: 'marca'});
db.Produto.belongsTo(db.Marca, {foreignKey: 'marcaId', as: 'marca'});

module.exports = db;

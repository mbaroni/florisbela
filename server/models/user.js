'use strict';
module.exports = (sequelize, DataTypes) => {
  var User = sequelize.define('User', {
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    nome: DataTypes.STRING,
    sexo: DataTypes.STRING(1),
    datNascimento: DataTypes.DATE,
    fileProfile: DataTypes.STRING
  }, {tableName: 'users'});
  
  return User;
};

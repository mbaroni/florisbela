'use strict';
const config = require('../config')
const faker = require('faker')


const bucketName = 'bucket0';

module.exports = (sequelize, DataTypes) => {
  var File = sequelize.define('File', {
    original: DataTypes.STRING,
    size: DataTypes.INTEGER,
    mimetype: DataTypes.STRING,
    hashname: DataTypes.STRING,
    sourceId: DataTypes.INTEGER,
    referenceId: DataTypes.INTEGER,
  }, {
    tableName: 'files',
    updatedAt: false,
  });


  /* Convert to archive */
  File.prototype.toArchive = function(){
    return {
      id: this.id,
      mimetype: this.mimetype,
      size: this.size,
      filename: this.original,
      url: ""
    };
  }


  /* Exclui um ou mais arquivos */
  File.deleteById = (ids, sourceId, referenceId ) => {
    var subDeleteById = (id, sourceId, referenceId ) => {
      File.findOne({
        where: { id, sourceId, referenceId }
      })
      .then( file => {
        console.log('file.hashname', file.hashname);
        config.minio.removeObject(bucketName, file.hashname, (err) => {
          if( !err ){
            file.destroy()
            .then( resp => {
              console.log('destroy resp:', resp);
            });
          }else{
            return false;
          }
        });
      });
    }

    if( !ids )
      return;
    if( !Array.isArray(ids) ){
      return subDeleteById(ids, sourceId, referenceId);
    }else{
      return ids.every( id => {
        return subDeleteById(id, sourceId, referenceId);
      });
    }
  }


  /* Cria e salva arquivos vindos de upload */
  File.createFromUpload = ( upload, sourceId, referenceId ) => {
    var subCreateFromUpload = ( upload, sourceId, referenceId ) => {
      var hashname = faker.random.alphaNumeric(20);
      var metaData = {
        'Content-Type': upload.mimetype,
      };
      config.minio.fPutObject(
        bucketName,
        hashname,
        upload.path,
        metaData
      )
      .then( resp => {
        return File.create({
          original: upload.originalname,
          size: upload.size,
          mimetype: upload.mimetype,
          hashname: hashname,
          sourceId,
          referenceId
        });
      });
    }

    if( !upload )
      return;
    if( !Array.isArray(upload) ){
      return subCreateFromUpload(upload, sourceId, referenceId);
    }else{
      return Promise.all(
        upload.map( upl =>
          subCreateFromUpload(upl, sourceId, referenceId)
        )
      );
    }
  }


  /* Serve file download */
  File.prototype.serveDownload = function( res ){
    config.minio.presignedGetObject(
      bucketName,
      this.hashname,
      (err, url) => {
        if( err ) console.log(err);
        res.redirect(url);
        //res.send(url);
      }
    );
  }


  /* get by referencie */
  File.findByReference = ( sourceId, referenceId ) => {
    return File.findAll({
      where: { sourceId, referenceId }
    });
  }


  return File;
};

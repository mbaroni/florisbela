'use strict';

module.exports = (sequelize, DataTypes) => {
  var Marca = sequelize.define('Marca', {
    nome: DataTypes.STRING,
    countryCode: DataTypes.STRING,
  }, {tableName: 'marcas'});
  return Marca;
};

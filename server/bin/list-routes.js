const listEndpoints = require('express-list-endpoints')
const express = require('express')
const routes = require('../src/routes')
const app = express()

app.use(routes);

console.log(listEndpoints(app));
